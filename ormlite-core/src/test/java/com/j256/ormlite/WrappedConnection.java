package com.j256.ormlite;

import com.j256.ormlite.support.DatabaseConnection;

import java.lang.reflect.InvocationHandler;

public interface WrappedConnection extends InvocationHandler {

	public DatabaseConnection getDatabaseConnectionProxy();

	public boolean isOkay();

	public void close();
}
