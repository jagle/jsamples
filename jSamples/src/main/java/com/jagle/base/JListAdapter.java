package com.jagle.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

// simple list adapter
public class JListAdapter<T> extends BaseAdapter {

    protected List<T> mObjects;
    protected ItemBuilder<T> mItemBuilder;

    // get item by custom builder
    public JListAdapter(Context context, ItemBuilder<T> builder, List<T> objects) {
        super();
        mObjects = objects;
        mItemBuilder = builder;
    }

    public JListAdapter(Context context, ItemBuilder<T> builder) {
        this(context, builder, new ArrayList<T>());
    }

    // get item by view class
    public JListAdapter(Context context, Class<? extends View> cls, List<T> objects) {
        this(context, new ViewItemBuilder<T>(context, cls), objects);
    }

    public JListAdapter(Context context, Class<? extends View> cls) {
        this(context, cls, new ArrayList<T>());
    }

    // get item by resource
    public JListAdapter(Context context, int resource, List<T> objects) {
        this(context, new ResourceItemBuilder<T>(context, resource), objects);
    }

    public JListAdapter(Context context, int resource) {
        this(context, resource, new ArrayList<T>());
    }

    public List<T> getDatas() {
        return mObjects;
    }

    public void clearData() {
        mObjects = new ArrayList<T>();
        notifyDataSetChanged();
    }

    public void addAll(List<T> data) {
        mObjects.addAll(data);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mObjects.remove(position);
        notifyDataSetChanged();
    }

    // set data
    public void setDatas(List<T> objects) {
        mObjects = objects;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (mObjects == null) {
            return 0;
        }
        return mObjects.size();
    }

    @Override
    public T getItem(int position) {
        return mObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View createView(int position) {
        return mItemBuilder.buildItem(position, getItem(position));
    }

    public void updateView(View view, int position) {
        updateView(view, getItem(position));
    }

    public boolean isRightView(View view, int position) {
        return isRightView(view, getItem(position));
    }

    // is the right view type
    public boolean isRightView(View view, T data) {
        return true;
    }

    // update view info
    public void updateView(View view, T data) {
    }

    ;

    public static interface ItemBuilder<T> {
        public View buildItem(int postion, T data);
    }

    public static class ResourceItemBuilder<T> implements ItemBuilder<T> {
        private int mResource = -1;
        private LayoutInflater mInflater;

        public ResourceItemBuilder(Context context, int resource) {
            mResource = resource;
            mInflater = LayoutInflater.from(context);
        }

        public View buildItem(int position, T data) {
            return mInflater.inflate(mResource, null);
        }
    }

    public static class ViewItemBuilder<T> implements ItemBuilder<T> {
        private final Class<?> mViewCls;
        private final WeakReference<Context> mContext;

        public ViewItemBuilder(Context context, Class<?> cls) {
            mViewCls = cls;
            mContext = new WeakReference<Context>(context);
        }

        @Override
        public View buildItem(int position, T data) {
            try {
                View v = (View) mViewCls.getConstructor(Context.class).newInstance(mContext.get());
                v.setLayoutParams(new AbsListView.LayoutParams(-1, -2));
                return v;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = createView(position);
            onItemCreated(convertView);
        }

        updateView(convertView, position);
        return convertView;
    }

    protected void onItemCreated(View convertView) {

    }

    public static class JViewHolder<T> {
        protected final View mRootView;

        public JViewHolder(View view) {
            mRootView = view;
        }

        public void updateView(int position, T data) {
        }
    }
}
