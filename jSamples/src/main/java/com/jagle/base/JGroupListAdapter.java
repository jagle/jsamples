package com.jagle.base;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jagle.base.JListAdapter.ItemBuilder;
import com.jagle.base.JListAdapter.ResourceItemBuilder;
import com.jagle.base.JListAdapter.ViewItemBuilder;

public abstract class JGroupListAdapter<G extends Object, I extends Object> extends BaseAdapter {
	
	public static final int GROUP_ITEM_TYPE = 0;
	public static final int LIST_ITEM_TYPE = 1;
	
	protected ItemBuilder<G> mGroupItemBuilder;
	protected ItemBuilder<I> mListItemBuilder;
	
	private int mAllCount = 0;
	
	public JGroupListAdapter(Context context, ItemBuilder<G> groupItemBuilder, ItemBuilder<I> listItemBuilder) {
		mGroupItemBuilder = groupItemBuilder;
		mListItemBuilder = listItemBuilder;
	}
	
	public JGroupListAdapter(Context context, int groupItemResId, int listItemResId) {
		mGroupItemBuilder = new ResourceItemBuilder<G>(context, groupItemResId);
		mListItemBuilder = new ResourceItemBuilder<I>(context, listItemResId);
	}
	
	public JGroupListAdapter(Context context, int groupItemResId, Class<? extends View> listItemCls) {
		mGroupItemBuilder = new ResourceItemBuilder<G>(context, groupItemResId);
		mListItemBuilder = new ViewItemBuilder<I>(context, listItemCls);
	}
	
	public JGroupListAdapter(Context context, Class<? extends View> groupItemCls, int listItemResId) {
		mGroupItemBuilder = new ViewItemBuilder<G>(context, groupItemCls);
		mListItemBuilder = new ResourceItemBuilder<I>(context, listItemResId);
	}
	
	public JGroupListAdapter(Context context, Class<? extends View> groupItemCls, Class<? extends View> listItemCls) {
		mGroupItemBuilder = new ViewItemBuilder<G>(context, groupItemCls);
		mListItemBuilder = new ViewItemBuilder<I>(context, listItemCls);
	}
	
	abstract public int getGroupCount();
	
	abstract public G getGroupItem(int groupId);
	
	abstract public int getListCount(int groupId);
	
	abstract public I getListItem(int groupId, int position);
	
	@Override
	public int getCount() {
		return mAllCount;
	}

	@Override
	public Object getItem(int position) {
		if (position <0 || position >= mAllCount ){ 
			return null;
		}
		
		ItemIndex index = getItemIndex(position);
		if(index == null){
			return null;
		}
		
		if(index.itemType == GROUP_ITEM_TYPE){
			return getGroupItem(index.groudId);
		} else {
			return getListItem(index.groudId, index.position);
		}
	}
	
	@Override
	public int getItemViewType(int position) {
		ItemIndex index = getItemIndex(position);
		if(index == null){
			return LIST_ITEM_TYPE;
		}
		
		return index.itemType;
	}
	
	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ItemIndex index = getItemIndex(position);
		if(index == null){
			return null;
		}
		
		if (convertView == null){
			if (index.itemType == GROUP_ITEM_TYPE){
				convertView = mGroupItemBuilder.buildItem(index.groudId, getGroupItem(index.groudId));
				onCreateGroupView(convertView, index.groudId);
			} else {
				convertView = mListItemBuilder.buildItem(index.position, getListItem(index.groudId, index.position));
				onCreateListItemView(convertView, index.groudId, index.position);
			}
		}
		
		if (index.itemType == GROUP_ITEM_TYPE){
			updateGroupView(convertView, index.groudId);
		} else {
			updateListeItemView(convertView, index.groudId, index.position);
		}
		
		return convertView;
	}
	
	protected void onCreateGroupView(View view, int groupId){
		
	}
	
	protected void onCreateListItemView(View view, int groupId, int position){
		
	}
	
	
	protected void updateGroupView(View view, int groupId){
		updateGroupView(view, getGroupItem(groupId));
	}
	
	protected void updateListeItemView(View view, int groupId, int position){
		updateListItemView(view, getListItem(groupId, position));
	}
	
	protected void updateGroupView(View view, G data){
	}

	protected void updateListItemView(View view, I data){
	}
	
	public void notifyItemCountChanged(){
		refreshAllCount();
	}
	
	@Override
	public void notifyDataSetChanged() {
		notifyItemCountChanged();
		super.notifyDataSetChanged();
	}
	
	@Override
	public void notifyDataSetInvalidated() {
		notifyItemCountChanged();
		super.notifyDataSetInvalidated();
	}
	
	private void refreshAllCount(){
		int count = 0;
		for(int i = 0; i < getGroupCount(); i++ ){
			count += getListCount(i) + 1;
		}
		mAllCount = count;
	}
	
	private ItemIndex getItemIndex(int position){
		int pos = position;
		if (pos < 0){
			return null;
		}
		
		if (pos == 0){
			return ItemIndex.createGroupItemIndex(0);
		}
		for(int i = 0 ; i < getGroupCount(); i++ ){
			pos--;
			if (pos < getListCount(i)){
				return ItemIndex.createListItemIndex(i, pos);
			} else {
				pos -= getListCount(i);
			}
			if (pos == 0){
				return ItemIndex.createGroupItemIndex(i + 1);
			} 
		}
		return null;
	}
	
	public static class ItemIndex{
		int groudId = 0;
		int position = 0;
		int itemType = LIST_ITEM_TYPE;
		
		public static ItemIndex createListItemIndex(int groudId, int position){
			ItemIndex index = new ItemIndex();
			index.groudId = groudId;
			index.position = position;
			index.itemType = LIST_ITEM_TYPE;
			return index;
		}
		
		public static ItemIndex createGroupItemIndex(int groupId){
			ItemIndex index = new ItemIndex();
			index.groudId = groupId;
			index.itemType = GROUP_ITEM_TYPE;
			return index;
		}
	}

}
