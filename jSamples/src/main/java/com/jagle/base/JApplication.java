package com.jagle.base;

import android.app.Application;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;

import com.jagle.common.JUncaughtExceptionHandler;
import com.jagle.samples.BuildConfig;
import com.jagle.samples.R;

import java.lang.Thread.UncaughtExceptionHandler;

public class JApplication extends Application {
	private static JApplication mSelf;
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		setUncaughtExceptionHandler();
		
		if (BuildConfig.DEBUG){
			StrictMode.enableDefaults();
		}
		
		mSelf = this;
	}

	public static JApplication getApp(){
		return mSelf;
	}
	
	public static int getAppIconId(){
		return R.drawable.icon_app;
	}
	
	public static int getAppNameId(){
		return R.string.app_name;
	}
	
	public static Drawable getAppIcon(){
		return mSelf.getResources().getDrawable(getAppIconId());
	}
	
	public static String getAppName(){
		return mSelf.getResources().getString(getAppNameId());
	}
	
	private void setUncaughtExceptionHandler() {
        UncaughtExceptionHandler dueh = Thread.getDefaultUncaughtExceptionHandler();
        JUncaughtExceptionHandler uEH = new JUncaughtExceptionHandler(dueh);
        Thread.setDefaultUncaughtExceptionHandler(uEH);
    }
}
