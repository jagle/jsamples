package com.jagle.base;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;

public abstract class JUiActivity extends Activity {
	
	public static final String DATA_ACTIVITY_TITLE = "data.activity.title";
	
	private SharedPreferences mPref;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		String title = getIntent().getStringExtra(DATA_ACTIVITY_TITLE);
		
		if (title != null){
			setTitle(title);
		}
		
		if (!isRootActivity()){
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
		setContentView(getContentViewId());
		(new Handler()).post(new Runnable() {
			
			@Override
			public void run() {
				onCreateContentView();
				
				holderViews();
			}
		});
		
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(DATA_ACTIVITY_TITLE, getTitle().toString());
		
		super.onSaveInstanceState(outState);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		String title = savedInstanceState.getString(DATA_ACTIVITY_TITLE);
		if (title != null){
			setTitle(title);
		}
		
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
	}
	
	public boolean isRootActivity(){
		return false;
	}
	
	protected void onCreateContentView() {
	}

	protected void holderViews() {
	}

	abstract public int getContentViewId();
	
	public static void setOnClickedListener(View.OnClickListener listener, View... views){
		for (View view : views){
			view.setOnClickListener(listener);
		}
	}
	
	public void setOnClickedListener(View.OnClickListener lintener, int... ids){
		for (int id : ids){
			View view = findViewById(id);
			view.setOnClickListener(lintener);
		}
	}
	
	public Context getContext(){
		return this;
	}
	
	protected SharedPreferences getPref() {
		if (mPref == null) {
			mPref = getPreferences(MODE_PRIVATE);
		}
		
		return mPref;
	}
}
