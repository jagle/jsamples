package com.jagle.base;

import android.content.Context;
import android.view.View;
import android.widget.Filter;
import android.widget.Filterable;

import com.jagle.utils.T9SearchEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


public class JListAdapterEx<T> extends JListAdapter<T> implements Filterable{
	private List<T> mOriginalValues;
	
	private String mKey;
	
	private boolean mPinyinSearch = true;
	
	private AtomicBoolean mT9DataInvalidate;
	
	private final byte[] mLock = new byte[0];

	private GListFilter mFilter;
	
	private T9SearchEngine mT9Search;

	public JListAdapterEx(Context context, int resource) {
		super(context, resource);
		mOriginalValues = new ArrayList<T>();
	}

	public JListAdapterEx(Context context, int resource, List<T> objects) {
		super(context, resource, objects);
		mOriginalValues = objects;
	}

	public JListAdapterEx(Context context, ItemBuilder<T> builder) {
		super(context, builder);
		mOriginalValues = new ArrayList<T>();
	}
	
	public JListAdapterEx(Context context, Class<? extends View> cls,
			List<T> objects) {
		super(context, cls, objects);
		mOriginalValues = objects;
	}

	public JListAdapterEx(Context context, Class<? extends View> cls) {
		super(context, cls);
		mOriginalValues = new ArrayList<T>();
	}

	public JListAdapterEx(Context context,
			ItemBuilder<T> builder,
			List<T> objects) {
		super(context, builder, objects);
		mOriginalValues = objects; 
	}

	@Override
	public void setDatas(List<T> objects) {
		synchronized (mLock) {
			mOriginalValues = objects;
		}
		if (mT9DataInvalidate != null){
			mT9DataInvalidate.compareAndSet(false, true);
		}
		getFilter().filter(mKey);
	}
	
	public void filter(String key){
		mKey = key;
		getFilter().filter(mKey);
	}
	
	public void setPinYinSearchMode(boolean open){
		mPinyinSearch = open;
	}
	
	public String getDataString(T data){
		return data.toString().toLowerCase();
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		if (mT9DataInvalidate != null){
			mT9DataInvalidate.compareAndSet(false, true);
		}
	}
	
	public T9SearchEngine getPinyinSearchEngine(){
		if (mT9Search == null){
			mT9DataInvalidate = new AtomicBoolean(true);
			mT9Search = new T9SearchEngine();
			mT9Search.create();
		}
		return mT9Search;
	}
	
	public long getT9SearchId(T data){
		return -1;
	}

	@Override
	public Filter getFilter() {
		if (mFilter == null) {
            mFilter = new GListFilter();
        }
        return mFilter;
	}
	
	/**
     * <p>An array filter constrains the content of the array adapter with
     * a prefix. Each item that does not contain the keywords 
     * is removed from the list.</p>
     */
    private class GListFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence key) {
            FilterResults results = new FilterResults();

            if (key == null || key.length() == 0) {
                ArrayList<T> list;
                synchronized (mLock) {
                    list = new ArrayList<T>(mOriginalValues);
                }
                results.values = list;
                results.count = list.size();
            } else if (!mPinyinSearch){
                String keyString = key.toString().toLowerCase();

                List<T> values = mOriginalValues;

                final int count = values.size();
                final ArrayList<T> newValues = new ArrayList<T>();

                for (int i = 0; i < count; i++) {
                    final T value = values.get(i);
                    final String valueText = getDataString(value).toLowerCase();

                    // First match against the whole, non-splitted value
                    if (valueText.contains(keyString)) {
                        newValues.add(value);
                    } else {
                        final String[] words = keyString.split(" ");
                        final int wordCount = words.length;

                        // Start at index 0, in case valueText starts with space(s)
                        int k = 0;
                        for (; k < wordCount; k++) {
                            if (!valueText.contains(words[k])) {
                                break;
                            }
                        }
                        if (k == wordCount){
                        	newValues.add(value);
                        }
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            } else {
            	String keyString = key.toString().toLowerCase();
            	List<T> values = mOriginalValues;
            	T9SearchEngine t9Search = getPinyinSearchEngine();
            	if(mT9DataInvalidate.compareAndSet(true, false)){
	            	t9Search.removeAllSentences();
	            	
	                for (int i = 0; i < values.size(); ++i) {
	                	t9Search.addSentence(getDataString(values.get(i)), 0x400, i);
	                	long id = getT9SearchId(values.get(i));
	                	if(id != -1){
	                		t9Search.addSentence(String.valueOf(id), 0x600, i);
	                	}
	                }
            	}

            	int[] foundT9Infos = t9Search.search(keyString, 0x03);
            	final ArrayList<T> newValues = new ArrayList<T>();
                if (foundT9Infos != null) {
                	for (int i: foundT9Infos){
                		newValues.add(values.get(i));
                	}
                }
                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @SuppressWarnings("unchecked")
		@Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mObjects = (List<T>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }

}
