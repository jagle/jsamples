package com.jagle.utils;

import android.graphics.Rect;

public class YuvUtil {

    public static final int ROTATE_0 = 0;
    public static final int ROTATE_90 = 90;
    public static final int ROTATE_180 = 180;
    public static final int ROTATE_270 = 270;

    int memoryBuffer = 0;
    int memoryBufferSize = 0;

    private native int convertNV21(byte[] srcFrame, byte[] dstFrame, int width, int height,
                                   int cropX, int cropY, int cropWidth, int cropHeight,
                                   int dstWidth, int dstHeight, int rotateMode);

    private native void getMemoryBuffer(int cropWidth, int cropHeight, int dstWidth, int dstHeight, int rotateMode);

    private native void releaseMemoryBuffer();

    public static class YuvFrameOp {
        Rect mCropRect = null;
        int mDstWidth = 0;
        int mDstHeight = 0;
        int mRotateMode = ROTATE_90;

        private YuvUtil mYuvUtil;

        public YuvFrameOp() {
            mYuvUtil = new YuvUtil();
        }

        public static YuvFrameOp getOp() {
            return new YuvFrameOp();
        }

        public YuvFrameOp crop(int x, int y, int width, int height) {
            mCropRect = new Rect(x, y, x + width, y + height);
            return this;
        }

        public YuvFrameOp crop(Rect rect) {
            mCropRect = rect;
            return this;
        }

        public YuvFrameOp rotate(int rotateMode) {
            mRotateMode = rotateMode;
            return this;
        }

        public YuvFrameOp scale(int width, int height) {
            mDstWidth = width;
            mDstHeight = height;
            return this;
        }

        public void release() {
            mYuvUtil.releaseMemoryBuffer();
        }

        public byte[] op(byte[] frame, int width, int height) {
            int cropX = 0, cropY = 0, cropWidth = width, cropHeight = height;

            if (mCropRect != null) {
                if (mCropRect.right > width || mCropRect.bottom > height) {
                    throw new IllegalArgumentException("crop rect is outside of source image");
                }

                cropX = mCropRect.left;
                cropY = mCropRect.top;
                cropWidth = mCropRect.width();
                cropHeight = mCropRect.height();
            }
            int dstWidth = cropWidth, dstHeight = cropHeight;
            if (mRotateMode == ROTATE_90 || mRotateMode == ROTATE_270) {
                dstWidth = cropHeight;
                dstHeight = cropWidth;
            }

            if (mDstWidth != 0) {
                dstHeight = mDstWidth;
            }

            if (mDstHeight != 0) {
                dstWidth = mDstHeight;
            }

            byte[] dstFrame = new byte[dstWidth * dstHeight * 3 / 2];
            mYuvUtil.getMemoryBuffer(cropWidth, cropHeight, dstWidth, dstHeight, mRotateMode);
            mYuvUtil.convertNV21(frame, dstFrame, width, height, cropX, cropY, cropWidth, cropHeight, dstWidth, dstHeight, mRotateMode);
            return dstFrame;
        }
    }

}
