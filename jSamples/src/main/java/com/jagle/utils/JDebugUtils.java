package com.jagle.utils;

import android.content.Context;
import android.widget.Toast;

import com.jagle.base.JApplication;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.samples.BuildConfig;

import java.util.HashMap;

public class JDebugUtils {

    private static Toast mDebugToast;

    private static HashMap<String, Long> mTimes = new HashMap<String, Long>();

    public static void traceTimeBegin(String key) {
        if (!BuildConfig.DEBUG) {
            return;
        }

        if (mTimes.containsKey(key)) {
            JLog.w(Developer.Jagle, "%s has been used", key);
            return;
        }
        Long time = System.currentTimeMillis();
        mTimes.put(key, time);
    }

    public static long traceTimeEnd(String key) {
        if (!BuildConfig.DEBUG) {
            return -1;
        }

        if (!mTimes.containsKey(key)) {
            JLog.w(Developer.Jagle, "none %s has been started ", key);
            return -1;
        }

        Long now = System.currentTimeMillis();
        Long begin = mTimes.remove(key);
        JLog.w(Developer.Jagle, "%1$s exec time:%2$d", key, now - begin);
        return now - begin;

    }

    public static void traceMethodTimeBegin() {
        if (!BuildConfig.DEBUG) {
            return;
        }

        String key = getCallerMethodName() + "@" + getCallerFilename();

        if (mTimes.containsKey(key)) {
            JLog.w(Developer.Jagle, "%s has been used", key);
            return;
        }
        Long time = System.currentTimeMillis();
        mTimes.put(key, time);
    }

    public static long traceMethodTimeEnd() {
        if (!BuildConfig.DEBUG) {
            return -1;
        }

        String key = getCallerMethodName() + "@" + getCallerFilename();

        if (!mTimes.containsKey(key)) {
            JLog.w(Developer.Jagle, "none %s has been started ", key);
            return -1;
        }

        Long now = System.currentTimeMillis();
        Long begin = mTimes.remove(key);
        JLog.w(Developer.Jagle, "%1$s exec time:%2$d", key, now - begin);
        return now - begin;

    }

    private static String getCallerFilename() {
        return Thread.currentThread().getStackTrace()[4].getFileName();
    }

    private static String getCallerMethodName() {
        return Thread.currentThread().getStackTrace()[4].getMethodName();
    }

    public static boolean assertArgs(boolean checkor, String msg) throws IllegalArgumentException {
        if (BuildConfig.DEBUG && !checkor) {
            throw new IllegalArgumentException(msg);
        } else {
            JLog.e(Developer.Jagle, msg);
        }
        return checkor;
    }

    public static boolean assertArgs(boolean checkor, String format, Object... args) throws IllegalArgumentException {
        if (BuildConfig.DEBUG && !checkor) {
            throw new IllegalArgumentException(String.format(format, args));
        } else {
            JLog.e(Developer.Jagle, String.format(format, args));
        }
        return checkor;
    }

    public static void toast(final Context context, final String text) {
        if (BuildConfig.DEBUG) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    if(mDebugToast == null) {
                        mDebugToast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
                    }else {
                        mDebugToast.setText(text);
                    }
                    mDebugToast.show();
                }
            });

        }
    }

    public static void toast(final Context context, final int resId) {
        if (BuildConfig.DEBUG) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    if(mDebugToast == null) {
                        mDebugToast =  Toast.makeText(context, resId, Toast.LENGTH_SHORT);
                    }else {
                        mDebugToast.setText(resId);
                    }
                    mDebugToast.show();
                }
            });
        }
    }

    public static void toast( final int resId) {
        if (BuildConfig.DEBUG) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    if (mDebugToast == null) {
                        mDebugToast = Toast.makeText(JApplication.getApp(), resId, Toast.LENGTH_SHORT);
                    } else {
                        mDebugToast.setText(resId);
                    }
                    mDebugToast.show();
                }
            });


        }
    }

    public static void toast(final String text) {
        if (BuildConfig.DEBUG) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    if(mDebugToast == null) {
                        mDebugToast =   Toast.makeText(JApplication.getApp(), text, Toast.LENGTH_SHORT);
                    }else {
                        mDebugToast.setText(text);
                    }
                    mDebugToast.show();
                }
            });
        }
    }
}
