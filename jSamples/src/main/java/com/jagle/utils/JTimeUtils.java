package com.jagle.utils;

import android.content.Context;

import com.jagle.samples.R;

import java.util.Calendar;

public class JTimeUtils {
    public static final String PATTERN_YMD_HMS = "(\\d+)-(\\d+)-(\\d+) (\\d+):(\\d+):(\\d+)";

    public static final String FORMAT_HMS = "%1$02d:%2$02d:%3$02d";

    public static long getCurrentTimeMills() {
        return System.currentTimeMillis();
    }

    public static long getCurrentTimeSeconds() {
        return System.currentTimeMillis() / 1000;
    }

    public static long getDelayTimeMillis(long delayMills) {
        return System.currentTimeMillis() + delayMills;
    }

    public static String getMillisHMSFormat(long millis, String format) {
        long value = millis / 1000;

        int second = (int) (value % 60);
        value /= 60;

        int minute = (int) (value % 60);
        value /= 60;

        int hour = (int) value;
        return String.format(format, hour, minute, second);
    }

    public static String getMillisHMSFormat(long millis) {
        return getMillisHMSFormat(millis, FORMAT_HMS);
    }

    public static String getTimeDescription(Context context, long time) {
        long now = System.currentTimeMillis();

        if ((now - time) / 1000 == 0) {
            return context.getString(R.string.jtu_now);
        }

        Calendar nowDate = Calendar.getInstance();
        Calendar timeDate = Calendar.getInstance();
        timeDate.setTimeInMillis(time);

        String ss1;
        if (timeDate.before(nowDate)) {
            ss1 = context.getString(R.string.jtu_before_time);
        } else {
            ss1 = context.getString(R.string.jtu_after_time);
            Calendar tmp = nowDate;
            nowDate = timeDate;
            timeDate = tmp;
        }

        String ss2;
        if (nowDate.get(Calendar.YEAR) != timeDate.get(Calendar.YEAR)) {
            ss2 = String.format(context.getString(R.string.jtu_year_number),
                    nowDate.get(Calendar.YEAR) - timeDate.get(Calendar.YEAR));
        } else if (nowDate.get(Calendar.MONTH) != timeDate.get(Calendar.MONTH)) {
            ss2 = String.format(context.getString(R.string.jtu_mouth_number),
                    nowDate.get(Calendar.MONTH) - timeDate.get(Calendar.MONTH));
        } else if (nowDate.get(Calendar.DAY_OF_MONTH) != timeDate.get(Calendar.DAY_OF_MONTH)) {
            ss2 = String.format(context.getString(R.string.jtu_day_number),
                    nowDate.get(Calendar.DAY_OF_MONTH) - timeDate.get(Calendar.DAY_OF_MONTH));
        } else if (nowDate.get(Calendar.HOUR_OF_DAY) != timeDate.get(Calendar.HOUR_OF_DAY)) {
            ss2 = String.format(context.getString(R.string.jtu_hour_number),
                    nowDate.get(Calendar.HOUR_OF_DAY) - timeDate.get(Calendar.HOUR_OF_DAY));
        } else if (nowDate.get(Calendar.MINUTE) != timeDate.get(Calendar.MINUTE)) {
            ss2 = String.format(context.getString(R.string.jtu_minute_number),
                    nowDate.get(Calendar.MINUTE) - timeDate.get(Calendar.MINUTE));
        } else {
            ss2 = String.format(context.getString(R.string.jtu_second_number),
                    nowDate.get(Calendar.SECOND) - timeDate.get(Calendar.SECOND));
        }

        return String.format(ss1, ss2);
    }
}
