package com.jagle.utils;

import java.io.File;

public class JFileUtils {
    public static char sPathSeparator = File.separatorChar;
    public static char sExtensionSeparator = '.';

    public static String getExtension(String filePath) {
        int dot = filePath.lastIndexOf(sExtensionSeparator);
        return filePath.substring(dot + 1);
    }

    public static String getName(String filePath) {
        return (new File(filePath)).getName();
    }

    public static String getNameWithoutExtension(String filePath) {
        int dot = filePath.lastIndexOf(sExtensionSeparator);
        int sep = filePath.lastIndexOf(sPathSeparator);
        return filePath.substring(sep + 1, dot);
    }

    public static String getPrarentPath(String filePath) {
        int sep = filePath.lastIndexOf(sPathSeparator);
        return filePath.substring(0, sep);
    }

    public static boolean isExist(String filePath) {
        return (new File(filePath)).exists();
    }

    public static boolean canExecute(String filePath) {
        return (new File(filePath)).canExecute();
    }

    public static void renameFile(String oldFilePath, String newFilePath) {
        File parentDir = new File(JFileUtils.getPrarentPath(newFilePath));
        parentDir.mkdirs();

        File oldFile = new File(newFilePath);
        File newFile = new File(oldFilePath);
        newFile.renameTo(oldFile);
    }

    public static void makeParentDirsIfNonExist(String filePath) {
        File file = new File(getPrarentPath(filePath));
        if (!file.exists()) {
            file.mkdirs();
        }
    }

}
