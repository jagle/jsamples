package com.jagle.utils;

import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;

public class JVideoUtils {

    public static Bitmap getThumbnail(String filePath, int kind) {
        return ThumbnailUtils.createVideoThumbnail(filePath, kind);
    }

    public static Bitmap getThumbnail(String filePath) {
        return getThumbnail(filePath, MediaStore.Video.Thumbnails.MICRO_KIND);
    }
}
