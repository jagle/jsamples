package com.jagle.samples;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jagle.base.JLinearLayout;
import com.jagle.common.ViewRef;

/**
 * Created by liujie on 14-10-8.
 */
public class KillServiceListItem extends JLinearLayout {

    ViewRef<TextView> mColumns0 = new ViewRef<TextView>(this, R.id.column_0);
    ViewRef<TextView> mColumns1 = new ViewRef<TextView>(this, R.id.column_1);
    ViewRef<TextView> mColumns2 = new ViewRef<TextView>(this, R.id.column_2);


    public KillServiceListItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KillServiceListItem(Context context) {
        super(context);
        setOrientation(HORIZONTAL);
    }

    @Override
    public int getContentViewId() {
        return R.layout.ksd_list_item;
    }

    public void setDatas(String ... datas) {
        if (datas == null) {
            return;
        }

        int length = datas.length > 3 ? 3 : datas.length;
        switch (length) {
            case 3:
                mColumns2.get().setText(datas[2]);
            case 2:
                mColumns1.get().setText(datas[1]);
            case 1:
                mColumns0.get().setText(datas[0]);
        }

    }
}
