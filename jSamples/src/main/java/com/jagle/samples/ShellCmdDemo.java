package com.jagle.samples;

import android.support.v17.leanback.widget.AbstractDetailsDescriptionPresenter;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.jagle.base.JUiActivity;
import com.jagle.common.ViewRef;
import com.jagle.utils.JDebugUtils;
import com.jagle.utils.ShellCmd;

import java.util.ArrayList;

/**
 * Created by ljn1781 on 2015/5/26.
 */
public class ShellCmdDemo extends JUiActivity implements Spinner.OnItemSelectedListener {

    ViewRef<TextView> mOutput = new ViewRef<TextView>(this, R.id.sc_output);
    ViewRef<Spinner> mCmd = new ViewRef<Spinner>(this, R.id.sc_cmd);

    ArrayList<CmdInfo> mCmds ;

    String mCurrentCmd;

    ArrayAdapter<CmdInfo> mAdapter;

    @Override
    public int getContentViewId() {
        return R.layout.activity_shell_cmd;
    }

    @Override
    protected void onCreateContentView() {
        mCmds = new ArrayList<CmdInfo>();
        mAdapter = new ArrayAdapter<CmdInfo>(this, R.layout.cmds_list_item, mCmds);

        mCmds.add(new CmdInfo("list input method", "ime list -a"));
        mCmds.add(new CmdInfo("set leanback ime", "ime set com.google.leanback.ime/.LeanbackImeService"));
        mCmds.add(new CmdInfo("set my ime", "com.jagle.samples/.Keyboard.SoftKeyboard"));
        mCmd.get().setAdapter(mAdapter);
        mCmd.get().setOnItemSelectedListener(this);

        mCmd.get().requestFocus();
    }

    public void onExecute(View view){
        if (TextUtils.isEmpty(mCurrentCmd)){
            JDebugUtils.toast("empty cmd");
            return;
        }
        mOutput.get().setText("");
        ArrayList<String> output = ShellCmd.docmd(mCurrentCmd);

        if(output != null){
            for(String line: output){
                mOutput.get().append(line);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mCurrentCmd = mCmds.get(position).mCmd;
        ((TextView)view).setText(mCmds.get(position).mTitle);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    static class CmdInfo{
        public String mTitle;
        public String mCmd;

        CmdInfo(String title, String cmd){
            mTitle = title;
            mCmd = cmd;
        }

        @Override
        public String toString() {
            return mTitle;
        }
    }
}
