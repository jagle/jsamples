package com.jagle.samples;


import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.AudioFormat;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jagle.base.JApplication;
import com.jagle.base.JUiActivity;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JDebugUtils;
import com.jagle.utils.JTimeUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

public class ScreenRecordActivity2 extends JUiActivity implements View.OnClickListener {
    private static final int PERMISSION_CODE = 1;
    private static final int BIT_RATE = 4000000;

    private int mScreenDensity;
    private MediaProjectionManager mProjectionManager;

    private int mVideoWidth = 720;
    private int mVideoHeight = 1280;
    private boolean mScreenRecording;

    private int mSampleRateInHz = 44100;
    private int mChannelCount = 2;

    private long mLastStartTime = 0;

    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private Surface mSurface;

    private MediaEncoderGroup mMeidaEncoderGroup;
    private Mp4Writer mMp4Writer;

    private File mOutputFile;

    private TextView mTextView;

    private H mHandler;

    private MediaEncoderGroup.MediaEncoder mVideoEncoder;
    private MediaEncoderGroup.MediaEncoder mAudioEncoder;

    private MediaEncoderGroup.EncoderDequeueThread mEncoderDequeueThread;

    private AudioRecorderUtil mAudioRecorderUtil;
    private AudioRecorderUtil.AudioRecorderThread mAudioRecorderThread;

    private MediaEncoderGroup.EncoderCallback mVideoCallback = new MediaEncoderGroup.EncoderCallback() {
        @Override
        public void onFormatChanged(MediaFormat format) {
            mMp4Writer.addVideoTrace(format);
        }

        @Override
        public void onData(ByteBuffer data, MediaCodec.BufferInfo bufferInfo, boolean endOfStream) {
            mMp4Writer.writeVideoData(data, bufferInfo);
        }

        @Override
        public void onDataEnd() {
            mMp4Writer.onVideoStopped();
            if (mAudioEncoder.mStopped){
                releaseEncoder();
            }
        }
    };

    private MediaEncoderGroup.EncoderCallback mAudioCallback = new MediaEncoderGroup.EncoderCallback() {
        @Override
        public void onFormatChanged(MediaFormat format) {
            mMp4Writer.addAudioTrace(format);
        }

        @Override
        public void onData(ByteBuffer data, MediaCodec.BufferInfo bufferInfo, boolean endOfStream) {
            mMp4Writer.writeAudioData(data, bufferInfo);
        }

        @Override
        public void onDataEnd() {
            mMp4Writer.onAudioStopped();
            if (mVideoEncoder.mStopped){
                releaseEncoder();
            }
        }
    };

    private AudioRecorderUtil.RecorderCallback mAudioRecorderCallbck = new AudioRecorderUtil.RecorderCallback() {
        @Override
        public void onStart(long timeStampUs) {
        }

        @Override
        public void onData(byte[] data, int length, long timeStampUs) {
            sendAudioFrame(data, length, timeStampUs, false);
        }

        @Override
        public void onStop(byte[] data, int length, long timeStampUs) {
            JLog.i(Developer.Jagle, "on audio recorder stop");
            sendAudioFrame(data, length, timeStampUs, true);

            releaseAudioRecorder();
        }
    };

    public void releaseAudioRecorder(){
        if (mAudioRecorderThread != null){
            mAudioRecorderThread.quit();
            mAudioRecorderUtil.release();
            mAudioRecorderThread = null;
            mAudioRecorderUtil = null;
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_screen_recorder;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenDensity = metrics.densityDpi;
        mProjectionManager = (MediaProjectionManager) JApplication.getApp().getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        mHandler = new H(Looper.myLooper(), this);
    }

    public void createEncoders(){
        mMeidaEncoderGroup = new MediaEncoderGroup();
        try {
            mVideoEncoder = mMeidaEncoderGroup.addVideoEncoder(mVideoWidth, mVideoHeight,
                    BIT_RATE, mVideoCallback);
            mSurface = mVideoEncoder.mCodec.createInputSurface();
            mVideoEncoder.mInputSurface = mSurface;
            mVideoEncoder.mCodec.start();

            mAudioEncoder = mMeidaEncoderGroup.addAudioEncoder(mSampleRateInHz, mChannelCount, mAudioCallback);
            mAudioEncoder.mCodec.start();

            mMp4Writer = new Mp4Writer(Mp4Writer.K_VIDEO_AUDIO, createOutputFile());

            mEncoderDequeueThread = new MediaEncoderGroup.EncoderDequeueThread(mMeidaEncoderGroup);
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    public void releaseEncoder(){
        if (mEncoderDequeueThread != null) {
            mEncoderDequeueThread.quit();
            mEncoderDequeueThread = null;
        }
        if (mMeidaEncoderGroup!= null){
            mMeidaEncoderGroup.release();
            mMeidaEncoderGroup = null;
        }

        if (mSurface != null){
            mSurface.release();
            mSurface = null;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseEncoder();
        releaseAudioRecorder();
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();

        setOnClickedListener(this, R.id.asr_start_record, R.id.asr_stop_record, R.id.asr_play_record);
        mTextView = (TextView) findViewById(R.id.asr_record_time);
    }

    private VirtualDisplay createVirtualDisplay() {
        mVirtualDisplay = mMediaProjection.createVirtualDisplay("ScreenSharingDemo",
                mVideoWidth, mVideoHeight, mScreenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mSurface, null /*Callbacks*/, null /*Handler*/);
        onCreateVirtualDisplay();
        return  mVirtualDisplay;
    }

    private File createOutputFile() {
        File file = new File(JApplication.getApp().getExternalFilesDir(Environment.DIRECTORY_MOVIES), String.valueOf(System.currentTimeMillis()) + ".mp4");
        mOutputFile = file;
        return file;
    }

    private void startRecord() {
        createEncoders();
        mScreenRecording = true;
        if (mSurface == null) {
            return;
        }

        mEncoderDequeueThread.startDequeue(10);

        if (mMediaProjection == null) {
            startActivityForResult(mProjectionManager.createScreenCaptureIntent(),
                    PERMISSION_CODE);
            return;
        }
        mVirtualDisplay = createVirtualDisplay();
    }

    private void startAudioRecord(){
        mAudioRecorderUtil = new AudioRecorderUtil();
        mAudioRecorderUtil.setRecorderCallback(mAudioRecorderCallbck);
        mAudioRecorderUtil.init();

        mAudioRecorderThread = new AudioRecorderUtil.AudioRecorderThread(mAudioRecorderUtil);
        mAudioRecorderThread.start();
    }

    private void stopAudioRecorder(){
        JLog.i(Developer.Jagle, "stopAudioRecorder");
        if (mAudioRecorderThread != null){
            mAudioRecorderThread.stop();
        }
    }

    private void stopRecord() {
        mScreenRecording = false;
        mEncoderDequeueThread.stopDequeue(100);

        stopAudioRecorder();

        if (mVirtualDisplay == null) {
            return;
        }
        mVirtualDisplay.release();
        mVirtualDisplay = null;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != PERMISSION_CODE) {
            JLog.e(Developer.Jagle, "Unknown request code: " + requestCode);
            return;
        }
        if (resultCode != RESULT_OK) {
            Toast.makeText(this,
                    "User denied screen sharing permission", Toast.LENGTH_SHORT).show();
            return;
        }
        mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
        createVirtualDisplay();
    }

    public void onCreateVirtualDisplay(){

        startAudioRecord();

        mTextView.post(new Runnable() {
            @Override
            public void run() {
                mLastStartTime = System.currentTimeMillis();
                mTimerRunable.run();
            }
        });
    }

    private void updateTime() {
        Long diff = System.currentTimeMillis() - mLastStartTime;
        mTextView.setText(JTimeUtils.getMillisHMSFormat(diff));
    }

    private Runnable mTimerRunable = new Runnable() {
        @Override
        public void run() {
            updateTime();
            mTextView.postDelayed(mTimerRunable, 1000);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.asr_start_record:
                if(!mScreenRecording){
                    mHandler.sendEmptyMessage(H.MSG_STRAR_RECORD);
                }
                break;

            case R.id.asr_stop_record:
                if (mScreenRecording) {
                    mHandler.sendEmptyMessage(H.MSG_STOP_RECORD);
                    updateTime();
                    mTextView.removeCallbacks(mTimerRunable);
                }
                break;

            case R.id.asr_play_record:
                if(!mScreenRecording){
                    showVideo();
                } else {
                    JDebugUtils.toast("show record first");
                }
                break;
            default:

                break;
        }
    }

    public void showVideo(){
        if (mOutputFile == null){
            JDebugUtils.toast("record first");
            return;
        }
        Uri uri = Uri.fromFile(new File(mOutputFile.getAbsolutePath()));
        String type = "video/mp4";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, type);
        try
        {
            startActivity(intent);
        }
        catch(Exception e)
        {
            JLog.e(Developer.Default, "Activity Not Found..", e);
            Toast.makeText(this,"Activity Not Found..", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendAudioFrame(byte[] data, int length, long timeStampUs, boolean endOfStream){

        if (endOfStream){
            JLog.i(Developer.Jagle, "send Audio Frame endOfStream");
        }

        mMeidaEncoderGroup.sendDataToEncoder(mAudioEncoder, data, 0, length, timeStampUs, endOfStream);
    }


    private static class H extends Handler{

        public static final int MSG_STRAR_RECORD = 0;
        public static final int MSG_STOP_RECORD = 1;
        public static final int MSG_SHOW_VIDEO = 2;

        private final WeakReference<ScreenRecordActivity2> mActRef;

        H(Looper looper, ScreenRecordActivity2 activity){
            super(looper);
            mActRef = new WeakReference<ScreenRecordActivity2>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MSG_STRAR_RECORD:{
                    mActRef.get().startRecord();
                    break;
                }
                case MSG_STOP_RECORD:{
                    mActRef.get().stopRecord();
                    break;
                }
                case MSG_SHOW_VIDEO:{
                    mActRef.get().showVideo();
                    break;
                }

            }
            super.handleMessage(msg);
        }
    }

}
