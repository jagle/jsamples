package com.jagle.samples;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class JService extends Service {

    public static final String COMMAND = "command";

    public JService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int cmd = intent.getIntExtra(COMMAND, -1);
        if (cmd != -1){
            onCommand(cmd);
        }
        return START_NOT_STICKY;
    }

    protected void onCommand(int cmd){

    }


    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }
}
