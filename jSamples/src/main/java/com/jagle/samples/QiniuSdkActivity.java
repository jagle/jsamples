package com.jagle.samples;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jagle.base.JUiActivity;
import com.jagle.constants.MineType;
import com.jagle.sdks.JQiniuSdk;
import com.jagle.utils.JActivityUtils;
import com.qiniu.auth.JSONObjectRet;

import org.json.JSONObject;

public class QiniuSdkActivity extends JUiActivity implements
		View.OnClickListener {
	
	private static final int RC_CHOOSE_FILE = 1;
	private TextView mFileUri;
	private ProgressBar mProgressBar;
	private Uri mUri = null;

	@Override
	public int getContentViewId() {
		return R.layout.activity_qiniu_sdk;
	}

	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();

		setOnClickedListener(this, R.id.aqs_choose_file_btn,
				R.id.aqs_form_upload_btn, 
				R.id.aqs_resumable_upload_btn);
		
		mFileUri = (TextView) findViewById(R.id.aqs_file_uri);
		mProgressBar = (ProgressBar) findViewById(R.id.aqs_upload_progress);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.aqs_choose_file_btn: {
			JActivityUtils.chooseFile(QiniuSdkActivity.this, MineType.ALL.getContentType(), RC_CHOOSE_FILE);
			break;
		}
		case R.id.aqs_form_upload_btn: {
			if (mUri == null){
				Toast.makeText(this, "choose a file", Toast.LENGTH_SHORT).show();
				break;
			}
			
			JQiniuSdk.uploadFile(QiniuSdkActivity.this, "test/" + Long.toHexString(System.currentTimeMillis()), mUri);
			break;
		}
		case R.id.aqs_resumable_upload_btn: {
			if (mUri == null){
				Toast.makeText(this, "choose a file", Toast.LENGTH_SHORT).show();
				break;
			}
			
			JQiniuSdk.resumeUploadFile(QiniuSdkActivity.this, "test/" + Long.toHexString(System.currentTimeMillis()), mUri, new JSONObjectRet() {
				
				@Override
				public void onFailure(Exception ex) {
					Toast.makeText(QiniuSdkActivity.this, "on failure", Toast.LENGTH_LONG).show();
				}
				
				@Override
				public void onSuccess(JSONObject obj) {
					Toast.makeText(QiniuSdkActivity.this, "on Success", Toast.LENGTH_LONG).show();
				}
				
				@Override
				public void onProcess(long current, long total) {
					int percent = (int) (current * 100 / total);
					mProgressBar.setProgress(percent);
				}
			});
			break;
		}
		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK) {
			return;
		}
		
		switch (requestCode) {
		case RC_CHOOSE_FILE:{
			mUri = data.getData();
			mFileUri.setText(mUri.toString());
			break;
		}
		default:
			break;
		}
	}

}
