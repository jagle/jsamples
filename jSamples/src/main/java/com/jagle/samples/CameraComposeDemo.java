package com.jagle.samples;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import com.jagle.base.JUiActivity;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JBitmapUtils;
import com.jagle.utils.JDebugUtils;
import com.jagle.utils.YuvUtil;
import com.jagle.utils.YuvUtil.YuvFrameOp;

import java.lang.ref.WeakReference;
import java.util.List;

public class CameraComposeDemo extends JUiActivity implements Callback,
		View.OnClickListener, PreviewCallback {
	
	private SurfaceView mCameraPreview;
	private SurfaceView mComposePreview;
	
	private TextView mTimeTrace;

	private Camera mCamera;

	private SurfaceHolder mComposedSurfaceHolder;
	
	private HandlerThread mDrawComposedThread;
	
	private ComposedHandler mHandler;

	@Override
	public int getContentViewId() {
		return R.layout.activity_camera_compose;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		BitmapFactory.Options options = new BitmapFactory.Options();  
        // Make sure it is 24 bit color as our image processing algorithm   
        // expects this format  
	    options.inPreferredConfig = Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test_image1, options);
		setBaseBitmap(bitmap);
		
	}

	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();

		mCameraPreview = (SurfaceView) findViewById(R.id.acc_camera_preview);
		mComposePreview = (SurfaceView) findViewById(R.id.acc_compose_preview);
		mTimeTrace = (TextView) findViewById(R.id.acc_time_trace);

		setOnClickedListener(this, R.id.acc_open_camera, R.id.acc_close_camera,
				R.id.acc_start_compose);

		mCameraPreview.getHolder().addCallback(this);
		mComposedSurfaceHolder = mComposePreview.getHolder();
		
		mDrawComposedThread = new HandlerThread("composed draw thread");
		mDrawComposedThread.start();
		mHandler = new ComposedHandler(mDrawComposedThread, mComposedSurfaceHolder);
		
		mTimeTrace.setMovementMethod(new ScrollingMovementMethod());
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		native_release();
		mDrawComposedThread.quit();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (mCamera != null) {
			try {
				mCamera.setPreviewDisplay(mCameraPreview.getHolder());
			} catch (Exception e) {
				JLog.e(Developer.Jagle, "set Preview failed", e);
				mCamera.release();
				mCamera = null;
			}
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		if (mCamera != null) {
			Camera.Parameters parameters = mCamera.getParameters();

			parameters.setPreviewSize(width, height);

			mCamera.setParameters(parameters);
			mCamera.startPreview();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.acc_open_camera:
			openCamera();
			break;

		case R.id.acc_close_camera:
			closeCamera();
			break;
			
		case R.id.acc_start_compose:
			
			break;

		default:
			break;
		}
	}

	private void openCamera() {
		mCamera = Camera.open();
		mCamera.setDisplayOrientation(90);
		mCamera.setPreviewCallback(this);
		Camera.Parameters parameters = mCamera.getParameters();
		List<Integer> formats = parameters.getSupportedPreviewFormats();
		JLog.i(Developer.Jagle, "support format::%s", formats.toString());
		parameters.setRotation(90);
		mCamera.setParameters(parameters);
		if (mCamera == null) {
			JLog.e(Developer.Jagle, "open camera failed");
		}

		try {
			mCamera.setPreviewDisplay(mCameraPreview.getHolder());
		} catch (Exception e) {
			JLog.e(Developer.Jagle, "set Preview failed", e);
			mCamera.release();
			mCamera = null;
		}
		mCamera.startPreview();
	}

	private void closeCamera() {
		if (mCamera == null){
			return;
		}
		
		mCamera.stopPreview();
		mCamera.setPreviewCallback(null);
		mCamera.release();
		mCamera = null;
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		JDebugUtils.traceMethodTimeBegin();
		
		Camera.Parameters p = mCamera.getParameters();
		Size size = p.getPreviewSize();
		JDebugUtils.traceTimeBegin("YuvFrameOp");
		data = YuvFrameOp.getOp().rotate(YuvUtil.ROTATE_90).op(data, size.width, size.height);
		JDebugUtils.traceTimeEnd("YuvFrameOp");
		composeFrame(data, p.getPreviewFormat(), 0, 0, size.height, size.width);
		long time = JDebugUtils.traceMethodTimeEnd();
		mTimeTrace.append(String.format("Compose Frame:%d \n" , time));
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public native void setBaseBitmap(Bitmap bitmap);

	public native void composeFrame(byte[] data, int format, int x, int y,
			int width, int height);
	
	public native void native_release();

	private static int count = 1;
	public void postComposedFrame(int[] data, int format,int width, int height){
		JLog.d(Developer.Jagle, "postComposedFrame");
		
		Message msg = mHandler.obtainMessage(ComposedHandler.MSG_POST_BUFFER);
		Bundle b = new Bundle();
		b.putIntArray(ComposedHandler.KEY_BUFFER_DATA, data);
		b.putInt(ComposedHandler.KEY_BUFFER_FORMAT, format);
		b.putInt(ComposedHandler.KEY_BUFFER_WIDTH, width);
		b.putInt(ComposedHandler.KEY_BUFFER_HEIGHT, height);
		msg.setData(b);
		msg.sendToTarget();
		
		/*Canvas c = mComposedSurfaceHolder.lockCanvas();
		if (format != JBitmapUtils.ARGB_8888){
			throw new IllegalArgumentException("format must be ARGB_8888");
		}
		
		Bitmap bitmap = Bitmap.createBitmap(data, width, height, Bitmap.Config.ARGB_8888);
		//JBitmapUtils.saveToFile(bitmap, JDeviceUtil.getExternalPictureFile(this, "tmp" + count++ +".png").getAbsolutePath());
		
		c.drawBitmap(bitmap, new Rect(0, 0, width, height), new Rect(0, 0 , 480, 640), null);
		mComposedSurfaceHolder.unlockCanvasAndPost(c);*/
	}
	
	static class ComposedHandler extends Handler{
		public static final String KEY_BUFFER_FORMAT = "key.buffer.format";
		public static final String KEY_BUFFER_WIDTH = "key.buffer.width";
		public static final String KEY_BUFFER_HEIGHT = "key.buffer.height";
		public static final String KEY_BUFFER_DATA = "key.buffer.data";
		
		public static final int MSG_POST_BUFFER = 0;
		private WeakReference<SurfaceHolder> mHolder;
		public ComposedHandler(HandlerThread thread, SurfaceHolder holder) {
			super(thread.getLooper());
			mHolder = new WeakReference<SurfaceHolder>(holder);
		}
		
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MSG_POST_BUFFER:
				JDebugUtils.traceTimeBegin("MSG_POST_BUFFER");
				int format = msg.getData().getInt(KEY_BUFFER_FORMAT);
				int width = msg.getData().getInt(KEY_BUFFER_WIDTH);
				int height = msg.getData().getInt(KEY_BUFFER_HEIGHT);
				int[] data = msg.getData().getIntArray(KEY_BUFFER_DATA);
				
				JDebugUtils.traceTimeBegin("lockCanvas");
				Canvas c = mHolder.get().lockCanvas();
				JDebugUtils.traceTimeEnd("lockCanvas");
				if (format != JBitmapUtils.ARGB_8888){
					throw new IllegalArgumentException("format must be ARGB_8888");
				}
				
				JDebugUtils.traceTimeBegin("createBitmap");
				Bitmap bitmap = Bitmap.createBitmap(data, width, height, Bitmap.Config.ARGB_8888);
				JDebugUtils.traceTimeEnd("createBitmap");
				//JBitmapUtils.saveToFile(bitmap, JDeviceUtil.getExternalPictureFile(this, "tmp" + count++ +".png").getAbsolutePath());
				
				JDebugUtils.traceTimeBegin("drawBitmap");
				c.drawBitmap(bitmap, new Rect(0, 0, width / 2, height / 2), new Rect(0, 0 , 300, 400), null);
				JDebugUtils.traceTimeEnd("drawBitmap");
				
				JDebugUtils.traceTimeBegin("unlockCanvasAndPost");
				mHolder.get().unlockCanvasAndPost(c);
				JDebugUtils.traceTimeEnd("unlockCanvasAndPost");
				JDebugUtils.traceTimeEnd("MSG_POST_BUFFER");
				break;

			default:
				super.handleMessage(msg);
				break;
			}
		}
	}
	
}
