package com.jagle.samples;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.jagle.base.JUiActivity;
import com.jagle.samples.gamepad.EventAdapter;
import com.jagle.samples.gamepad.GamepadController;

/**
 * Created by ljn1781 on 2015/7/9.
 */
public class TouchDrawDemo extends JUiActivity {

    private GamepadController mController = new GamepadController();
    private EventAdapter  mEventAdapter = new EventAdapter(500, 400, 300) {
        @Override
        protected void sendTouchEvent(int action, int x, int y) {
            long time = System.currentTimeMillis();
            MotionEvent event = MotionEvent.obtain(time, time, action, x, y, 0);
            dispatchTouchEvent(event);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventAdapter.setGamepadController(mController);

    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_touch_draw;
    }

    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        mController.handleMotionEvent(event);

        return super.onGenericMotionEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        mController.handleKeyEvent(event);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        mController.handleKeyEvent(event);
        return super.onKeyUp(keyCode, event);
    }
}
