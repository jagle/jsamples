package com.jagle.samples;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jagle.base.JUiActivity;

public class RgbToYuvActivity extends JUiActivity {
	private TextView mTextView;
	private ImageView mImageView;
	private ImageView mOutImageView;

	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();
		
		mTextView = (TextView)findViewById(R.id.text);
		mImageView = (ImageView)findViewById(R.id.image);
		mOutImageView = (ImageView)findViewById(R.id.outImage);
	}
	
	public native void changeBitmap(Bitmap bitmap, Bitmap outBitmap);
	
	public native void changeBitmapDiff(Bitmap bitmap, Bitmap outBitmap);
	
	public void showMessage(String message){
		mTextView.append(message);
	}
	
	public void onRgbToYuv(View view){
		// load bitmap from resources  
        BitmapFactory.Options options = new BitmapFactory.Options();  
        // Make sure it is 24 bit color as our image processing algorithm   
        // expects this format  
        options.inPreferredConfig = Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test_image1, options);
		Bitmap scaleBitmap = Bitmap.createScaledBitmap(bitmap, 320, 480, false);
		Bitmap outBitmap = Bitmap.createBitmap(scaleBitmap.getWidth(), scaleBitmap.getHeight(),  
				Config.RGB_565);  
		mImageView.setImageBitmap(scaleBitmap);
		mTextView.setText("");
		changeBitmap(scaleBitmap, outBitmap);
		mOutImageView.setImageBitmap(outBitmap);
	}
	
	public void onRgbToYuvDiff(View view){
		// load bitmap from resources  
        BitmapFactory.Options options = new BitmapFactory.Options();  
        // Make sure it is 24 bit color as our image processing algorithm   
        // expects this format  
        options.inPreferredConfig = Config.RGB_565;
		Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.test_image1, options);
		Bitmap scaleBitmap = Bitmap.createScaledBitmap(bitmap, 320, 480, false);
		Bitmap outBitmap = Bitmap.createBitmap(scaleBitmap.getWidth(), scaleBitmap.getHeight(),  
				Config.RGB_565);  
		
		mImageView.setImageBitmap(scaleBitmap);
		mTextView.setText("");
		changeBitmapDiff(scaleBitmap, outBitmap);
		mOutImageView.setImageBitmap(outBitmap);
	}

	@Override
	public int getContentViewId() {
		return R.layout.activity_rgb_to_yuv;
	}

}
