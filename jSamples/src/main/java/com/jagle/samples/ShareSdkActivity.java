package com.jagle.samples;

import android.os.Bundle;
import android.view.View;

import com.jagle.base.JUiActivity;
import com.jagle.sdks.JShareSdk;

import cn.sharesdk.framework.ShareSDK;

public class ShareSdkActivity extends JUiActivity implements View.OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ShareSDK.initSDK(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		ShareSDK.stopSDK(this);
	}
	
	@Override
	public int getContentViewId() {
		return R.layout.activity_share_sdk;
		
	}
	
	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();
		setOnClickedListener(this, R.id.ass_share_all_gui);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.ass_share_all_gui:{
			showShare(false, null);
			break;
		}
		}
	}
	
	private void showShare(boolean silent, String platform) {
		JShareSdk.showOneKeyShare(this, silent, platform);
	}
	
}

