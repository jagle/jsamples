package com.jagle.samples;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import com.jagle.common.Developer;
import com.jagle.common.JLog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class SocketClient {
	public static final int MSG_WRITE_DATA = 0;
	
	private final Socket mSocket;
	
	private String mName;
	
	ReadThread mReadThread;
	HandlerThread mHandlerThread;
	H mHandler;
	
	private AtomicBoolean mIsOpen = new AtomicBoolean(true);
	
	public SocketClient(Socket socket) {
		mSocket = socket;
		mName = getClass().getSimpleName();
		
		startThreads();
	}
	
	public SocketClient(Socket socket, String name) {
		mSocket = socket;
		mName = name;
		
		startThreads();
	}
	
	public String getName() {
		return mName;
	}
	
	private void startThreads(){
		mReadThread = new ReadThread();
		mReadThread.start();
		
		mHandlerThread = new HandlerThread(mName + "Handler Thread");
		mHandlerThread.start();
		mHandler = new H(this, mHandlerThread.getLooper());
		
	}
	
	protected void onReceivedData(byte[] data){
		
	}
	
	public void write(byte[] data) {
		Message msg = mHandler.obtainMessage();
		msg.what = MSG_WRITE_DATA;
		msg.obj = data;
		msg.sendToTarget();
	}
	
	private class ReadThread extends Thread {
		
		public ReadThread() {
			super(mName + "read");
		}
		
		@Override
		public void run() {
			InputStream is = null;
			try {
				is = mSocket.getInputStream();
			} catch (IOException e) {
				JLog.e(Developer.Jagle, "failed to get input stream of socket", e);
				return;
			}
			

			ByteArrayOutputStream buffer = new ByteArrayOutputStream(128);
			while (mIsOpen.get() && !mSocket.isClosed()) {
				buffer.reset();
				try {
					int length = 0;
					byte[] tmp = new byte[128];
					while((length = is.read(tmp)) != -1){
						buffer.write(tmp, 0, length);
					}
					JLog.e(Developer.Jagle, tmp.toString());
				} catch (IOException e) {
					JLog.e(Developer.Jagle, "failed to read socket", e);
					continue;
				}
				if (buffer.size() > 0){
					onReceivedData(buffer.toByteArray());
				}
				
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
		}
	}
	
	public void close() {
		mIsOpen.set(false);
		mHandlerThread.quit();
		
		try {
			mSocket.close();
		} catch (IOException e) {
			JLog.e(Developer.Jagle, "failed to close socket", e);
		}
	}
	
	protected void handleMassage(Message msg) {
		switch (msg.what) {
		case MSG_WRITE_DATA:
			if (mSocket.isClosed()){
				JLog.e(Developer.Jagle, "failed to write data to closed socket");
				break;
			}
			
			byte[] data = (byte[])msg.obj;
			OutputStream os = null;
			
			try {
				os = mSocket.getOutputStream();
			} catch (IOException e) {
				JLog.e(Developer.Jagle, "failed to get outputs stream of socket", e);
				return;
			}
			
			try {
				os.write(data);
			} catch (IOException e) {
				JLog.e(Developer.Jagle, "failed to write data to socket", e);
			}
			
			break;

		default:
			break;
		}
	}
	
	private static class H extends Handler {
		private final WeakReference<SocketClient> mClient;
		
		public H(SocketClient client, Looper looper) {
			super(looper);
			mClient = new WeakReference<SocketClient>(client);
		}
		
		@Override
		public void handleMessage(Message msg) {
			mClient.get().handleMassage(msg);
		}
	}
	

}
