package com.jagle.samples;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jagle.base.JApplication;
import com.jagle.base.JUiActivity;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JDebugUtils;
import com.jagle.utils.JTimeUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class ScreenRecordActivity extends JUiActivity implements View.OnClickListener {
    private static final int PERMISSION_CODE = 1;

    private int mScreenDensity;
    private MediaProjectionManager mProjectionManager;

    private int mVideoWidth = 720;
    private int mVideoHeight = 1280;
    private boolean mScreenRecording;

    private long mLastStartTime = 0;

    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private Surface mSurface;

    private VideoEncoderCore mVideoEncoder;
    private File mOutputFile;

    private TextView mTextView;

    private HandlerThread mThread;
    private Handler mHandler;

    private AudioRecord mAudioRecord;
    private int mSampleRateInHz = 44100;
    private int mChannelConfig = AudioFormat.CHANNEL_IN_MONO;
    private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;
    private byte[] mAudioBuffer;

    private long mNextFrameTime;

    private int mBufferLength;

    @Override
    public int getContentViewId() {
        return R.layout.activity_screen_recorder;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenDensity = metrics.densityDpi;
        mProjectionManager = (MediaProjectionManager) JApplication.getApp().getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        mThread = new HandlerThread("Screen Record Thread");
        mThread.start();

        mHandler = new H(mThread.getLooper(), this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();

        setOnClickedListener(this, R.id.asr_start_record, R.id.asr_stop_record, R.id.asr_play_record);
        mTextView = (TextView) findViewById(R.id.asr_record_time);
    }

    private VirtualDisplay createVirtualDisplay() {
        return mMediaProjection.createVirtualDisplay("ScreenSharingDemo",
                mVideoWidth, mVideoHeight, mScreenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mSurface, null /*Callbacks*/, null /*Handler*/);
    }

    private void startEncoder() {
        JLog.d(Developer.Jagle, "starting to record");
        // Record at 1280x720, regardless of the window dimensions.  The encoder may
        // explode if given "strange" dimensions, e.g. a width that is not a multiple
        // of 16.  We can box it as needed to preserve dimensions.
        final int BIT_RATE = 4000000;   // 4Mbps
        try {
            mOutputFile = createOutputFile();
            mVideoEncoder = new VideoEncoderCore(mVideoWidth, mVideoHeight,
                    BIT_RATE, mOutputFile);
            mVideoEncoder.setAudioEncoder(mSampleRateInHz, 1);
            mSurface = mVideoEncoder.getInputSurface();
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private File createOutputFile() {
        File file = new File(JApplication.getApp().getExternalFilesDir(Environment.DIRECTORY_MOVIES), String.valueOf(System.currentTimeMillis()) + ".mp4");
        return file;
    }

    private void startRecord() {
        startEncoder();

        mScreenRecording = true;
        if (mSurface == null) {
            return;
        }
        if (mMediaProjection == null) {
            startActivityForResult(mProjectionManager.createScreenCaptureIntent(),
                    PERMISSION_CODE);
            return;
        }
        mVirtualDisplay = createVirtualDisplay();
        startAudioRecord();
        mNextFrameTime = System.nanoTime();
    }

    private void startAudioRecord(){
        int bufferSize =AudioRecord.getMinBufferSize(mSampleRateInHz,
                mChannelConfig,
                mAudioFormat);

        bufferSize = Math.max(mSampleRateInHz * H.TIME_DELAY * 2 / 500, bufferSize);
        JLog.i(Developer.Jagle, "buffer size:" + bufferSize);

        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                mSampleRateInHz,
                mChannelConfig,
                mAudioFormat,
                bufferSize) ;

        mAudioBuffer = new byte[bufferSize];

        mBufferLength = mSampleRateInHz * H.TIME_DELAY / 500;

        mAudioRecord.startRecording();
    }


    private void stopRecord() {
        mScreenRecording = false;
        if (mVideoEncoder != null) {
            mVideoEncoder.drainVideoEncoder(true);
            sendAudioFrame(true);

            mVideoEncoder.release();
            mVideoEncoder = null;
        }

        if (mAudioRecord != null){
            mAudioRecord.stop();
            mAudioRecord.release();
            mAudioRecord = null;
        }

        if (mVirtualDisplay == null) {
            return;
        }
        mVirtualDisplay.release();
        mVirtualDisplay = null;

        if (mSurface != null){
            mSurface.release();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != PERMISSION_CODE) {
            JLog.e(Developer.Jagle, "Unknown request code: " + requestCode);
            return;
        }
        if (resultCode != RESULT_OK) {
            Toast.makeText(this,
                    "User denied screen sharing permission", Toast.LENGTH_SHORT).show();
            return;
        }
        mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
        mVirtualDisplay = createVirtualDisplay();

        mLastStartTime = System.currentTimeMillis();
        mTimerRunable.run();

        startAudioRecord();

        mNextFrameTime  = System.nanoTime();
    }

    private void updateTime() {
        Long diff = System.currentTimeMillis() - mLastStartTime;
        mTextView.setText(JTimeUtils.getMillisHMSFormat(diff));
    }

    private Runnable mTimerRunable = new Runnable() {
        @Override
        public void run() {
            updateTime();
            mTextView.postDelayed(mTimerRunable, 1000);
        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.asr_start_record:
                if(!mScreenRecording){
                    mHandler.sendEmptyMessage(H.MSG_STRAR_RECORD);
                    mLastStartTime = System.currentTimeMillis();
                    mTimerRunable.run();
                }
                break;

            case R.id.asr_stop_record:
                if (mScreenRecording) {
                    mHandler.sendEmptyMessage(H.MSG_STOP_RECORD);
                    updateTime();
                    mTextView.removeCallbacks(mTimerRunable);
                }
                break;

            case R.id.asr_play_record:
                if(!mScreenRecording){
                    showVideo();
                } else {
                    JDebugUtils.toast("show record first");
                }
                break;
            default:

                break;
        }
    }

    public void showVideo(){
        Uri uri = Uri.fromFile(new File(mOutputFile.getAbsolutePath()));
        String type = "video/mp4";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, type);
        try
        {
            startActivity(intent);
        }
        catch(Exception e)
        {
            JLog.e(Developer.Default, "Activity Not Found..", e);
            Toast.makeText(this,"Activity Not Found..", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendAudioFrame(boolean endOfStream){
        JDebugUtils.traceTimeBegin("Auido Read");
        int length = mAudioRecord.read(mAudioBuffer, 0, mBufferLength);
        JDebugUtils.traceTimeEnd("Auido Read");
        JLog.i(Developer.Jagle, "input audio length:" + length + " time:" + mNextFrameTime);

        JLog.i(Developer.Jagle, "now time" + System.nanoTime());

        if (endOfStream){
            JLog.i(Developer.Jagle, "send Audio Frame endOfStream");
        }
        if (length > 0){
            mVideoEncoder.offerAudioEncoderAsync(mAudioBuffer, mNextFrameTime , endOfStream, length);
            mNextFrameTime += H.TIME_DELAY * 1000 * 1000;
        } else if(endOfStream) {
            mVideoEncoder.offerAudioEncoderAsync(null, mNextFrameTime , endOfStream, 0);
        }

    }


    private static class H extends Handler{

        public static final int MSG_STRAR_RECORD = 0;
        public static final int MSG_STOP_RECORD = 1;
        public static final int MSG_SHOW_VIDEO = 2;
        public static final int MSG_UPDATE_FRAME = 3;

        public static final int TIME_DELAY = 200;

        private final WeakReference<ScreenRecordActivity> mActRef;

        H(Looper looper, ScreenRecordActivity activity){
            super(looper);
            mActRef = new WeakReference<ScreenRecordActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MSG_STRAR_RECORD:{
                    mActRef.get().startRecord();

                    sendEmptyMessageDelayed(MSG_UPDATE_FRAME, TIME_DELAY);
                    mActRef.get().mVideoEncoder.drainVideoEncoder(false);
                    mActRef.get().mVideoEncoder.drainAuidoEncoder(false);
                    break;
                }

                case MSG_STOP_RECORD:{
                    removeMessages(MSG_UPDATE_FRAME);
                    mActRef.get().stopRecord();
                    break;
                }
                case MSG_SHOW_VIDEO:{
                    mActRef.get().showVideo();
                    break;
                }

                case MSG_UPDATE_FRAME:{
                    JDebugUtils.traceTimeBegin("MSG_UPDATE_FRAME");
                    removeMessages(MSG_UPDATE_FRAME);
                    JDebugUtils.traceTimeBegin("drainVideoEncoder");
                    mActRef.get().mVideoEncoder.drainVideoEncoder(false);
                    JDebugUtils.traceTimeEnd("drainVideoEncoder");
//                    try {
//                        Thread.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    JDebugUtils.traceTimeBegin("sendAudioFrame");
                    mActRef.get().sendAudioFrame(false);
                    JDebugUtils.traceTimeEnd("sendAudioFrame");

                    long diff = mActRef.get().mNextFrameTime - System.nanoTime();
                    JLog.d(Developer.Jagle, "delaytime: " + diff);
                    if (diff > 0){
                        sendEmptyMessageDelayed(MSG_UPDATE_FRAME, diff/ 1000);
                    } else {
                        sendEmptyMessage(MSG_UPDATE_FRAME);
                    }
                    JDebugUtils.traceTimeEnd("MSG_UPDATE_FRAME");
                    break;
                }
            }
            super.handleMessage(msg);
        }
    }




}
