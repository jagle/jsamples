package com.jagle.samples;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.jagle.base.JUiActivity;


public class RemoteInputActivity extends JUiActivity {

    @Override
    public int getContentViewId() {
        return R.layout.activity_remote_input;
    }
}
