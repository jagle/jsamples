package com.jagle.samples;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.view.View;

import com.jagle.base.JUiActivity;
import com.jagle.utils.JDebugUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by liujie on 14-10-9.
 */
public class PcmConvertActivty extends JUiActivity {

    private int mSampleRateInHz = 44100;
    private int mChannelConfig = AudioFormat.CHANNEL_IN_MONO;
    private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;

    @Override
    public int getContentViewId() {
        return R.layout.activity_pcm_convert;
    }


    private class PlayRunnable implements Runnable {

        private String mFile;
        private AtomicBoolean mRunning = new AtomicBoolean(false);
        private AudioTrack mTrack;

        PlayRunnable(String file) {
            mFile = file;

        }

        public boolean isRunning() {
            return mRunning.get();
        }

        @Override
        public void run() {
            mRunning.set(true);
            BufferedInputStream bis = null;
            try {
                bis = new BufferedInputStream(new FileInputStream(mFile));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            int bufferSize = AudioTrack.getMinBufferSize(mSampleRateInHz,
                    AudioFormat.CHANNEL_OUT_STEREO,
                    mAudioFormat);
            byte[] buffer = new byte[bufferSize];

            mTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                    mSampleRateInHz,
                    AudioFormat.CHANNEL_OUT_STEREO,
                    AudioFormat.ENCODING_PCM_16BIT,
                    bufferSize,
                    AudioTrack.MODE_STREAM);
            mTrack.play();

            try {
                while (bis.available() > 0) {
                    int length = bis.read(buffer);
                    if (length > 0) {
                        //然后将数据写入到AudioTrack中
                        mTrack.write(buffer, 0, length);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mTrack.stop();
            try {
                bis.close();// 关闭写入流
            } catch (IOException e) {
                e.printStackTrace();
            }
            mRunning.set(false);
        }

    }

    private static final String SourceFile = "/mnt/sdcard/test_pcm";
    private static final String OutFile = "test_o_pcm";
    private static final String OutFile2 = "test_o2_pcm";

    public void onPlayBefore(View view) {
        PlayRunnable runnable = new PlayRunnable(SourceFile);
        Thread thread = new Thread(runnable, "play before thread");
        thread.start();
    }

    public void onPlayOut(View view) {
        PlayRunnable runnable = new PlayRunnable(OutFile);
        Thread thread = new Thread(runnable, "play before thread");
        thread.start();
    }

    public void onPlayOut2(View view) {
        PlayRunnable runnable = new PlayRunnable(OutFile2);
        Thread thread = new Thread(runnable, "play before thread");
        thread.start();
    }

    public void onConvert(View view){
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        byte[] buffer = new byte[128];
        try {
            bis = new BufferedInputStream(getResources().openRawResource(R.raw.test_pcm));
            bos = new BufferedOutputStream(openFileOutput(OutFile, Context.MODE_WORLD_READABLE));
            bos = new BufferedOutputStream(new FileOutputStream(OutFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            while (bis.available() > 0) {
                int length = bis.read(buffer);
                if (length == 128) {
                    PcmConvert.floatToInt16(buffer, length);
                } else {
                    length = length & 0xfffffff0;
                    PcmConvert.floatToInt16(buffer, length);
                    JDebugUtils.toast("length:" + length);
                }
                bos.write(buffer, 0, length / 2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            bis.close();
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void onConvert2(View view){
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        byte[] buffer = new byte[128];
        try {
            bis = new BufferedInputStream(getResources().openRawResource(R.raw.test_pcm));
            bos = new BufferedOutputStream(new FileOutputStream(OutFile2));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            while (bis.available() > 0) {
                int length = bis.read(buffer);
                if (length == 128) {
                    PcmConvert.floatToInt16_2(buffer, length);
                } else {
                    length = length & 0xfffffff0;
                    PcmConvert.floatToInt16_2(buffer, length);
                    JDebugUtils.toast("length:" + length);
                }
                bos.write(buffer, 0, length / 2);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            bis.close();
            bos.flush();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
