package com.jagle.samples.Accessibility;

import android.accessibilityservice.AccessibilityService;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;

import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JDebugUtils;

/**
 * Created by ljn1781 on 2015/6/15.
 */
public class MyAccessibilityService  extends AccessibilityService {
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        // TODO Auto-generated method stub
        JLog.d(Developer.Jagle, event.toString());
    }

    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        // TODO Auto-generated method stub
        return true;

    }

    @Override
    public void onInterrupt() {
        // TODO Auto-generated method stub

    }

}
