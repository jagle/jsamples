/*
 * Copyright 2014 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jagle.samples;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.AsyncTask;
import android.os.Trace;
import android.util.Log;
import android.view.Surface;

import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JDebugUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * This class wraps up the core components used for surface-input video encoding.
 * <p>
 * Once created, frames are fed to the input surface.  Remember to provide the presentation
 * time stamp, and always call drainVideoEncoder() before swapBuffers() to ensure that the
 * producer side doesn't get backed up.
 * <p>
 * This class is not thread-safe, with one exception: it is valid to use the input surface
 * on one thread, and drain the output on a different thread.
 */
public class VideoEncoderCore {
    private static final String TAG = "VideoEncodeCore";

    private static final int TOTAL_NUM_TRACKS = 2;

    // TODO: these ought to be configurable as well
    private static final String MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding
    private static final int FRAME_RATE = 30;               // 30fps
    private static final int IFRAME_INTERVAL = 5;           // 5 seconds between I-frames

    private static final String AUDIO_MIME_TYPE = "audio/mp4a-latm";
    private static final int AUDIO_BIT_RATE = 128000;
    private static final int AUDIO_MAX_SIZE = 44100;

    private Surface mInputSurface;
    private MediaMuxer mMuxer;
    private MediaCodec mVideoEncoder;
    private MediaCodec.BufferInfo mVideoBufferInfo;
    private int mVideoTrackIndex;
    private boolean mMuxerStarted;

    private int mNumTraceAdd = 0;

    private MediaCodec mAudioEncoder;
    private int mAudioTrackIndex;
    private MediaCodec.BufferInfo mAudioBufferInfo;

    private long mAudioStartTime;
    private long mVideoStartTime;

    FileOutputStream mStream;

    /**
     * Configures encoder and muxer state, and prepares the input Surface.
     */
    public VideoEncoderCore(int width, int height, int bitRate, File outputFile)
            throws IOException {
        mVideoBufferInfo = new MediaCodec.BufferInfo();

        mAudioBufferInfo = new MediaCodec.BufferInfo();

        MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, width, height);

        // Set some properties.  Faililongng to specify some of these can cause the MediaCodec
        // configure() call to throw an unhelpful exception.
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
        format.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);
        JLog.d(Developer.Jagle, "format: " + format);

        // Create a MediaCodec encoder, and configure it with our format.  Get a Surface
        // we can use for input and wrap it with a class that handles the EGL work.
        mVideoEncoder = MediaCodec.createEncoderByType(MIME_TYPE);
        mVideoEncoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        mInputSurface = mVideoEncoder.createInputSurface();
        mVideoEncoder.start();

        // Create a MediaMuxer.  We can't add the video track and start() the muxer here,
        // because our MediaFormat doesn't have the Magic Goodies.  These can only be
        // obtained from the encoder after it has started processing data.
        //
        // We're not actually interested in multiplexing audio.  We just want to convert
        // the raw H.264 elementary stream we get from MediaCodec into a .mp4 file.
        mMuxer = new MediaMuxer(outputFile.toString(),
                MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);

        mVideoTrackIndex = -1;
        mMuxerStarted = false;

        mStream = new FileOutputStream(new File("/mnt/sdcard/test.h264"));

    }

    public void setAudioEncoder(int samepleRate, int channelCount) throws IOException {
        MediaFormat audioFormat = new MediaFormat();
        audioFormat.setString(MediaFormat.KEY_MIME, AUDIO_MIME_TYPE);
        audioFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        audioFormat.setInteger(MediaFormat.KEY_BIT_RATE, AUDIO_BIT_RATE);
        audioFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, AUDIO_MAX_SIZE);
        audioFormat.setInteger(MediaFormat.KEY_SAMPLE_RATE, samepleRate);
        audioFormat.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channelCount);

        mAudioEncoder = MediaCodec.createEncoderByType(AUDIO_MIME_TYPE);
        mAudioEncoder.configure(audioFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        mAudioEncoder.start();
    }

    private void offerAudioEncoder(byte[] input, long presentationTimeNs, boolean endOfStream, int length) {
        JLog.e(Developer.Jagle, "offerAudio time:" + presentationTimeNs / 1000);
        try {
            ByteBuffer[] inputBuffers = mAudioEncoder.getInputBuffers();
            int inputBufferIndex = mAudioEncoder.dequeueInputBuffer(-1);
            if (inputBufferIndex >= 0) {
                ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
                inputBuffer.clear();
                long presentationTimeUs = presentationTimeNs / 1000;
                if (length != 0) {
                    inputBuffer.put(input);
                }

                if (endOfStream) {
                    mAudioEncoder.queueInputBuffer(inputBufferIndex, 0, length, presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                } else {
                    mAudioEncoder.queueInputBuffer(inputBufferIndex, 0, length, presentationTimeUs, 0);
                }
            }
        } catch (Throwable t) {
            JLog.e(Developer.Jagle, "_offerAudioEncoder exception", t);
        }
        if (endOfStream){
            drainAuidoEncoder(true);
        }
    }

    public void offerAudioEncoderAsync(final byte[] input, final long presentationTimeNs, final boolean endOfStream, final int length){
        drainAuidoEncoder(false);
        if (!endOfStream) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                offerAudioEncoder(input, presentationTimeNs, endOfStream, length);
                }
            };
            new InputAudioTask().execute(r);
        } else {
            offerAudioEncoder(input, presentationTimeNs, endOfStream, length);
        }
    }

    /**
     * Returns the encoder's input surface.
     */
    public Surface getInputSurface() {
        return mInputSurface;
    }

    /**
     * Releases encoder resources.
     */
    public void release() {
         JLog.d(Developer.Jagle, "releasing encoder objects");
        if (mVideoEncoder != null) {
            mVideoEncoder.stop();
            mVideoEncoder.release();
            mVideoEncoder = null;
        }

        if (mAudioEncoder != null){
            mAudioEncoder.stop();
            mAudioEncoder.release();
            mAudioEncoder = null;
        }

        if (mMuxer != null) {
            // TODO: stop() throws an exception if you haven't fed it any data.  Keep track
            //       of frames submitted, and don't call stop() if we haven't written anything.
            mMuxer.stop();
            mMuxer.release();
            mMuxer = null;
        }

        mNumTraceAdd = 0;
        mMuxerStarted = false;

        if (mStream!= null) {
            try {
                mStream.close();
            } catch (IOException e){
                JLog.e(Developer.Jagle, "close stream error", e);
            }
            mStream = null;
        }
    }

    /**
     * Extracts all pending data from the encoder and forwards it to the muxer.
     * <p/>
     * If endOfStream is not set, this returns when there is no more data to drain.  If it
     * is set, we send EOS to the encoder, and then iterate until we see EOS on the output.
     * Calling this with endOfStream set should be done once, right before stopping the muxer.
     * <p/>
     * We're just using the muxer to get a .mp4 file (instead of a raw H.264 stream).  We're
     * not recording audio.
     */
    private void drainEncoder(MediaCodec encoder, MediaCodec.BufferInfo bufferInfo, boolean endOfStream, boolean audio) {
        final int TIMEOUT_USEC = (audio ? 20 : 10000);
        if (audio) {
            JLog.d(Developer.Jagle, "audio drainEncoder(" + endOfStream + ")");
        } else {
            JLog.d(Developer.Jagle, "video drainEncoder(" + endOfStream + ")");
        }

        ByteBuffer[] encoderOutputBuffers = encoder.getOutputBuffers();
        while (true) {
            int encoderStatus = encoder.dequeueOutputBuffer(bufferInfo, TIMEOUT_USEC);
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // no output available yet
                if (!endOfStream) {
                    break;      // out of while
                } else {
                     JLog.d(Developer.Jagle, "no output available, spinning to await EOS");
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                // not expected for an encoder
                encoderOutputBuffers = encoder.getOutputBuffers();
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                // should happen before receiving buffers, and should only happen once

                if (mMuxerStarted) {
                    throw new RuntimeException("format changed after muxer start");
                }
                MediaFormat newFormat = encoder.getOutputFormat();

                // now that we have the Magic Goodies, start the muxer
                int trackIndex = mMuxer.addTrack(newFormat);
                if (audio) {
                    mAudioTrackIndex = trackIndex;
                } else {
                    mVideoTrackIndex = trackIndex;
                }
                mNumTraceAdd++;
                JLog.d(Developer.Jagle, "encoder output format changed: " + newFormat + ". Added track index: " + trackIndex);
                if (mNumTraceAdd == TOTAL_NUM_TRACKS) {
                    mMuxer.start();
                    mMuxerStarted = true;
                    JLog.i(Developer.Jagle, "All tracks added. Muxer started");
                }

            } else if (encoderStatus < 0) {
                JLog.w(Developer.Jagle, "unexpected result from encoder.dequeueOutputBuffer: " +
                        encoderStatus);
                // let's ignore it
            } else {
                ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                if (encodedData == null) {
                    throw new RuntimeException("encoderOutputBuffer " + encoderStatus +
                            " was null");
                }

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    // The codec config data was pulled out and fed to the muxer when we got
                    // the INFO_OUTPUT_FORMAT_CHANGED status.  Ignore it.
                    JLog.d(Developer.Jagle, "ignoring BUFFER_FLAG_CODEC_CONFIG");
                    bufferInfo.size = 0;
                }


                if (bufferInfo.size != 0) {
                    if (!mMuxerStarted) {
                        throw new RuntimeException("muxer hasn't started");
                    }

                    // adjust the ByteBuffer values to match BufferInfo (not needed?)
                    encodedData.position(bufferInfo.offset);
                    encodedData.limit(bufferInfo.offset + bufferInfo.size);

                    int trackIndex = audio ? mAudioTrackIndex: mVideoTrackIndex;

                    if (audio && endOfStream){
                        JLog.i(Developer.Jagle, "ignore last buffer");
                    } else {
                        mMuxer.writeSampleData(trackIndex, encodedData, bufferInfo);
                    }

                    if (audio){
                        if (mAudioStartTime == 0) {
                            JLog.d(Developer.Jagle, "audio sent " + bufferInfo.size + " bytes to muxer, ts=" +
                                    bufferInfo.presentationTimeUs);
                            mAudioStartTime = System.nanoTime();
                        }
                    } else {
                        if (mVideoStartTime == 0) {
                            JLog.d(Developer.Jagle, "video sent " + bufferInfo.size + " bytes to muxer, ts=" +
                                    bufferInfo.presentationTimeUs);
                            mVideoStartTime = System.nanoTime();
                        }
                    }
                }

                encoder.releaseOutputBuffer(encoderStatus, false);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (!endOfStream) {
                        JLog.w(Developer.Jagle, "reached end of stream unexpectedly");
                    } else {
                         JLog.d(Developer.Jagle, "end of stream reached");
                    }
                    break;      // out of while
                }
            }
        }
    }

    public void drainVideoEncoder(boolean endOfStream) {
        if (endOfStream ) {
             JLog.d(Developer.Jagle, "sending EOS to encoder");
            mVideoEncoder.signalEndOfInputStream();
        }
        drainEncoder(mVideoEncoder, mVideoBufferInfo,  endOfStream, false);
    }

    public void drainAuidoEncoder(boolean endOfStream) {
        drainEncoder(mAudioEncoder, mAudioBufferInfo,  endOfStream, true);
    }

    /**
     * Extracts all pending data from the encoder and forwards it to the muxer.
     * <p>
     * If endOfStream is not set, this returns when there is no more data to drain.  If it
     * is set, we send EOS to the encoder, and then iterate until we see EOS on the output.
     * Calling this with endOfStream set should be done once, right before stopping the muxer.
     * <p>
     * We're just using the muxer to get a .mp4 file (instead of a raw H.264 stream).  We're
     * not recording audio.
     */
    public void drainEncoder(boolean endOfStream) {
        final int TIMEOUT_USEC = 10000;

        if (endOfStream) {
             JLog.d(Developer.Jagle, "sending EOS to encoder");
            mVideoEncoder.signalEndOfInputStream();
        }

        ByteBuffer[] encoderOutputBuffers = mVideoEncoder.getOutputBuffers();
        while (true) {
            int encoderStatus = mVideoEncoder.dequeueOutputBuffer(mVideoBufferInfo, TIMEOUT_USEC);
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // no output available yet
                if (!endOfStream) {
                    break;      // out of while
                } else {
                     JLog.d(Developer.Jagle, "no output available, spinning to await EOS");
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                // not expected for an encoder
                encoderOutputBuffers = mVideoEncoder.getOutputBuffers();
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                // should happen before receiving buffers, and should only happen once
                if (mMuxerStarted) {
                    throw new RuntimeException("format changed twice");
                }
                MediaFormat newFormat = mVideoEncoder.getOutputFormat();
                JLog.d(Developer.Jagle, "encoder output format changed: " + newFormat);

                // now that we have the Magic Goodies, start the muxer
                mVideoTrackIndex = mMuxer.addTrack(newFormat);
                mMuxer.start();
                mMuxerStarted = true;
            } else if (encoderStatus < 0) {
                JLog.w(Developer.Jagle, "unexpected result from encoder.dequeueOutputBuffer: " +
                        encoderStatus);
                // let's ignore it
            } else {
                ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                if (encodedData == null) {
                    throw new RuntimeException("encoderOutputBuffer " + encoderStatus +
                            " was null");
                }

                if ((mVideoBufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    // The codec config data was pulled out and fed to the muxer when we got
                    // the INFO_OUTPUT_FORMAT_CHANGED status.  Ignore it.
                     JLog.d(Developer.Jagle, "ignoring BUFFER_FLAG_CODEC_CONFIG");
                    mVideoBufferInfo.size = 0;
                }

                if (mVideoBufferInfo.size != 0) {
                    if (!mMuxerStarted) {
                        throw new RuntimeException("muxer hasn't started");
                    }

                    // adjust the ByteBuffer values to match BufferInfo (not needed?)
                    encodedData.position(mVideoBufferInfo.offset);
                    encodedData.limit(mVideoBufferInfo.offset + mVideoBufferInfo.size);

                    mMuxer.writeSampleData(mVideoTrackIndex, encodedData, mVideoBufferInfo);
                     {
                        JLog.d(Developer.Jagle, "sent " + mVideoBufferInfo.size + " bytes to muxer, ts=" +
                                mVideoBufferInfo.presentationTimeUs);
                    }
                }

                mVideoEncoder.releaseOutputBuffer(encoderStatus, false);

                if ((mVideoBufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (!endOfStream) {
                        JLog.w(Developer.Jagle, "reached end of stream unexpectedly");
                    } else {
                         JLog.d(Developer.Jagle, "end of stream reached");
                    }
                    break;      // out of while
                }
            }
        }
    }

    class InputAudioTask extends AsyncTask<Runnable, Void, Void> {
        @Override
        protected Void doInBackground(Runnable... runnables) {
            for(Runnable runnable : runnables){
                runnable.run();
            }
            return null;
        }
    }
}
