package com.jagle.samples;

import android.media.AudioRecord;
import android.media.audiofx.Visualizer;
import android.util.Log;

public class AudioRecorder {
	private static final String LOG_TAG = "Video Recorder - Audio Recorder";

	private AudioRecord record;

	private Visualizer visualizer;

	ByteQueue bufferQueue;

	/*
	 * public int startAudioRecord(int sampleRateInHz, int channelConfig, int
	 * audioFormat, int bufferSize){ stopAuidoRecord();
	 * 
	 * try{ record = new AudioRecord(MediaRecorder.AudioSource.MIC,
	 * sampleRateInHz, channelConfig, audioFormat, bufferSize) ;
	 * 
	 * record.startRecording();
	 * 
	 * return 0; } catch (Exception e){ Log.e(LOG_TAG,
	 * "Failed to start audio recorder: " + e.getMessage() + ", Sample Rate: " +
	 * sampleRateInHz + ", Channel Config: " + channelConfig +
	 * ", Audio Format: " + audioFormat + ", Buffer Size: " + bufferSize);
	 * 
	 * if (record != null) { record.release(); record = null; } }
	 * 
	 * return -1; }
	 */

	public AudioRecorder(int sampleRateInHz, int channelConfig,
			int audioFormat, int bufferSize) {
		visualizer = new Visualizer(0);
		visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
		bufferQueue = new ByteQueue(bufferSize);

		Visualizer.OnDataCaptureListener captureListener = new Visualizer.OnDataCaptureListener() {
			@Override
			public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
				Log.e(LOG_TAG, "onWaveFormDataCapture");
				try {
					bufferQueue.write(bytes, 0, bytes.length);
				} catch (InterruptedException e) {
					Log.e(LOG_TAG, "Failed to write data", e);
				}
			}

			@Override
			public void onFftDataCapture(Visualizer visualizer, byte[] bytes,
					int samplingRate) {
			}
		};

		int size = visualizer.getCaptureSize();
		Log.e("test", "onWaveFormDataCapture:size " + size);
		
		int max = Visualizer.getMaxCaptureRate();
		Log.e("test","onWaveFormDataCapture:max " + max + " " + (sampleRateInHz / visualizer.getCaptureSize()));
		
		int[] range = Visualizer.getCaptureSizeRange();
		Log.e("test","onWaveFormDataCapture:range  [" + range[0] +", " + range[1] + "]");
		
		visualizer.setDataCaptureListener(captureListener, sampleRateInHz, true, false);

	}

	public int startAudioRecord(int sampleRateInHz, int channelConfig,
			int audioFormat, int bufferSize) {
		visualizer.setEnabled(true);
		return 0;
	}

	/*
	 * public void stopAuidoRecord(){ if (record != null){ try { record.stop();
	 * }catch (Exception e) { Log.e(LOG_TAG, "Failed to stop audio recorder: " +
	 * e.getMessage()); } finally { record.release(); record = null; } } }
	 */

	public void stopAuidoRecord() {
		visualizer.setEnabled(false);
	}

	/*
	 * public int readData(byte[] audioData, int offsetInBytes, int sizeInBytes)
	 * { if (record == null){ return AudioRecord.ERROR_INVALID_OPERATION; }
	 * return record.read(audioData, offsetInBytes, sizeInBytes); }
	 */

	public int readData(byte[] audioData, int offsetInBytes, int sizeInBytes) {

		try {
			return bufferQueue.read(audioData, offsetInBytes, sizeInBytes);
		} catch (InterruptedException e) {
			Log.e(LOG_TAG, "Failed to read data", e);
		}
		return -1;
	}

	public static byte[] bit8ToBit16(byte[] data, int length) {
		byte[] out = new byte[length * 2];

		StringBuilder sb = new StringBuilder();
		StringBuilder sb2 = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append(data[i]);
			sb.append(" ");
			out[2 * i] = 0;
			out[2 * i + 1] = (byte) (data[i] - 128);
			
			sb2.append(out[2*i]).append(",").append(out[2*i + 1]).append(" ");
		}
		
		Log.e("test", "PCM 8bit" + sb.toString());
		Log.e("test", "PCM 16bit" + sb2.toString());
		return out;
	}
}
