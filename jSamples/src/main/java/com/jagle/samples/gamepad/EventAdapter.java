package com.jagle.samples.gamepad;

import android.app.Notification;
import android.view.KeyEvent;
import android.view.MotionEvent;

import com.j256.ormlite.stmt.query.ManyClause;

/**
 * Created by ljn1781 on 2015/7/15.
 */
public class EventAdapter implements GamepadController.OnKeyEventListener, GamepadController.OnMotionEventListener {

    private int mX;
    private int mY;
    private int mRadius;

    private boolean isMoving = false;

    private final static float MIN_VALUE = 0.3f;

    public EventAdapter(int x, int y, int radius ){
        mX = x;
        mY = y;
        mRadius = radius;
    }

    private GamepadController mController;

    public void setGamepadController(GamepadController controller){
        mController = controller;
        mController.setOnKeyEventListener(this);
        mController.setOnMotionEventListener(this);
    }

    private void handlerMotionEvent(GamepadController controller, MotionEvent event){
        if (!isMoving && controller.isJoystickRelease()){
            return;
        }

        if (isMoving && controller.isJoystickRelease()){
            sendTouchEvent(MotionEvent.ACTION_UP, mX, mY);
            isMoving = false;
            return;
        }

        int toX = (int) (mX + mRadius * controller.getJoystickPosition(0, 0));
        int toY = (int) (mY + mRadius * controller.getJoystickPosition(0, 1));

        if (!isMoving){
            sendTouchEvent(MotionEvent.ACTION_DOWN, mX, mY);
            sendTouchEvent(MotionEvent.ACTION_MOVE, toX, toY);
            isMoving = true;
            return;
        }

        sendTouchEvent(MotionEvent.ACTION_MOVE, toX, toY);
    }

    protected void sendTouchEvent(int action, int x, int y){
    }

    @Override
    public void onKeyEvent(GamepadController controller, KeyEvent event) {

    }

    @Override
    public void onMotionEvent(GamepadController controller, MotionEvent event) {
        handlerMotionEvent(controller, event);
    }
}
