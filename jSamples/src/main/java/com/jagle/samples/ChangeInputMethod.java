package com.jagle.samples;

import android.content.Context;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.jagle.base.JUiActivity;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.common.ViewRef;

/**
 * Created by ljn1781 on 2015/5/26.
 */
public class ChangeInputMethod extends JUiActivity {

    ViewRef<EditText> mEdit = new ViewRef<EditText>(this, R.id.cim_edit);

    @Override
    public int getContentViewId() {
        return R.layout.activity_change_input_method;
    }

    public void onMyIme(View view){
        try {
            String MY_IME = "com.jagle.samples/.Keyboard.SoftKeyboard";
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            final IBinder token1 = this.getWindow().getCurrentFocus().getWindowToken();
            final IBinder token2 = this.getWindow().getAttributes().token;
            final IBinder token3 = mEdit.get().getWindowToken();
            JLog.d(Developer.Jagle, token1.toString() + token2.toString());
            imm.setInputMethod(token3, MY_IME);
        } catch (Throwable t) { // java.lang.NoSuchMethodError if API_level<11
            JLog.e(Developer.Jagle, "cannot set the previous input method:", t);
        }
    }

    public void onNext(View view){
        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            final IBinder token = this.getWindow().getAttributes().token;
            imm.switchToNextInputMethod(token, false);
        } catch (Throwable t) { // java.lang.NoSuchMethodError if API_level<11
            JLog.e(Developer.Jagle, "cannot set the previous input method:");
            t.printStackTrace();
        }
    }

}
