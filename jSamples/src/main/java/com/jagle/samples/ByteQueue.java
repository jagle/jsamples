package com.jagle.samples;

import android.util.Log;

class ByteQueue {
	
	private static long writeLength = 0;
	
	private static long readLength = 0;
	
    public ByteQueue(int size) {
        mBuffer = new byte[size];
    }

    public int getBytesAvailable() {
        synchronized(this) {
            return mStoredBytes;
        }
    }

    public int read(byte[] buffer, int offset, int length)
        throws InterruptedException {
        if (length + offset > buffer.length) {
            throw
                new IllegalArgumentException("length + offset > buffer.length");
        }
        if (length < 0) {
            throw
            new IllegalArgumentException("length < 0");

        }
        if (length == 0) {
            return 0;
        }
        synchronized(this) {
            int totalRead = 0;
            int bufferLength = mBuffer.length;
            while (length > 0 && mStoredBytes > 0) {
                int oneRun = Math.min(bufferLength - mHead, mStoredBytes);
                int bytesToCopy = Math.min(length, oneRun);
                System.arraycopy(mBuffer, mHead, buffer, offset, bytesToCopy);
                mHead += bytesToCopy;
                if (mHead >= bufferLength) {
                    mHead = 0;
                }
                mStoredBytes -= bytesToCopy;
                length -= bytesToCopy;
                offset += bytesToCopy;
                totalRead += bytesToCopy;
            }
            readLength += totalRead;
            Log.e("test", "ByteQueue read " + totalRead + " " + readLength);
            
            return totalRead;
        }
    }

    /**
     * Attempt to write the specified portion of the provided buffer to
     * the queue.  Returns the number of bytes actually written to the queue;
     * it is the caller's responsibility to check whether all of the data
     * was written and repeat the call to write() if necessary.
     */
    public int write(byte[] buffer, int offset, int length)
    throws InterruptedException {
    	writeLength += length;
    	Log.e("test", "ByteQueue write " + length + " " + writeLength);
        if (length + offset > buffer.length) {
            throw
                new IllegalArgumentException("length + offset > buffer.length");
        }
        if (length < 0) {
            throw
            new IllegalArgumentException("length < 0");

        }
        if (length == 0) {
            return 0;
        }
        
        if (length > buffer.length) {
        	offset = offset + (length - buffer.length);
        	length = buffer.length;
        }
        
        synchronized(this) {
            int bufferLength = mBuffer.length;
            
            int more = length - ( bufferLength - mStoredBytes) ;
            if (more > 0) {
            	mHead += more;
            	if (mHead >= bufferLength){
            		mHead -= bufferLength;
            	}
            	mStoredBytes -= more;
            }
            
            int tail = mHead + mStoredBytes;
            
        	if (tail >= bufferLength) {
                tail = tail - bufferLength;
                System.arraycopy(buffer, offset, mBuffer, tail, length);
            } else if (tail + length <= bufferLength) {
            	System.arraycopy(buffer, offset, mBuffer, tail, length);
            } else {
            	int oneRun = bufferLength - tail;
            	System.arraycopy(buffer, offset, mBuffer, tail, oneRun);
            	System.arraycopy(buffer, offset + oneRun, mBuffer, 0, length - oneRun);
            }
            mStoredBytes += length;
            
            return length;
        }
    }

    private byte[] mBuffer;
    private int mHead;
    private int mStoredBytes;
}
