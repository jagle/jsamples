package com.jagle.samples;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jagle.base.JUiActivity;
import com.jagle.utils.JFileUtils;
import com.jagle.utils.JTimeUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class InnerAudioRecorderDemo extends JUiActivity implements View.OnClickListener {
	
	private int mSampleRateInHz = 16000;
	private int mChannelConfig = AudioFormat.CHANNEL_IN_MONO;
	private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;
	
	private TextView mTextView;
	private RecordState mState = new RecordState();
	private RecordRunable mRecordRunable = new RecordRunable();
	private PlayRunnable mPlayRunnable = new PlayRunnable();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public int getContentViewId() {
		return R.layout.activity_audio_recorder;
	}
	
	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();
		
		setOnClickedListener(this, R.id.aar_start_record, R.id.aar_stop_record, R.id.aar_play_record);
		mTextView = (TextView) findViewById(R.id.aar_record_time);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.aar_start_record:
			if (!mState.isRecording()){
				startAudioRecord();
				mState.onStart();
				mTimerRunable.run();
			}
			break;

		case R.id.aar_stop_record:
			if (mState.isRecording()){
				stopAudioRecord();
				updateTime();
				mState.onStop();
				mTextView.removeCallbacks(mTimerRunable);
			}
			break;
			
		case R.id.aar_play_record:
			playRecordedAudio();
			break;
		default:
			break;
		}
	}
	
	private void startAudioRecord(){
		if (!mRecordRunable.isRunning()){
			Thread thread = new Thread(mRecordRunable, "Record Thread");
			thread.start();
		}
	}
	
	private void stopAudioRecord(){
		if (mRecordRunable.isRunning()){
			mRecordRunable.stop();
		}
	}
	
	private void playRecordedAudio(){
		if (!mRecordRunable.isRunning() && !mPlayRunnable.isRunning()){
			Thread thread = new Thread(mPlayRunnable, "Play Thread");
			thread.start();
		}
	}
	
	private class RecordRunable implements Runnable{
		private AtomicBoolean mForceStop = new AtomicBoolean(true);
		private AtomicBoolean mRunning = new AtomicBoolean(false);
		
		private AudioRecorder mRecorder;
		
		int bufferSize = 20000;
		
		public RecordRunable() {
			mRecorder = new AudioRecorder(mSampleRateInHz, mChannelConfig, mAudioFormat, bufferSize);
		}
		
		void stop(){
			mForceStop.set(true);
			mRecorder.stopAuidoRecord();
		}
		
		public boolean isRunning(){
			return mRunning.get();
		}
		
		@Override
		public void run() {
			mRunning = new AtomicBoolean(true);
			mForceStop = new AtomicBoolean(false);
			
			mRecorder.startAudioRecord(0, 0, 0, 0);
			
			JFileUtils.makeParentDirsIfNonExist(TestEnv.sTestPCMData);
			BufferedOutputStream bos = null;
			try {
				bos = new BufferedOutputStream(new FileOutputStream(TestEnv.sTestPCMData));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			 //定义缓冲    
            byte[] buffer = new byte[bufferSize];
			
            while(!mForceStop.get()){    
                 //从bufferSize中读取字节，返回读取的short个数    
                 //这里老是出现buffer overflow，不知道是什么原因，试了好几个值，都没用，TODO：待解决    
                 //int bufferReadResult = mRecorder.readData(buffer, 0, buffer.length);
            	int bufferReadResult = mRecorder.readData(buffer, 0, buffer.length);
                 //循环将buffer中的音频数据写入到OutputStream中    
                 if (AudioRecord.ERROR_INVALID_OPERATION != bufferReadResult) {  
                        try {
							bos.write(buffer, 0, bufferReadResult);
						} catch (IOException e) {
							e.printStackTrace();
						} 
                 }  
				try {
					Thread.sleep(400);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
            }
            try {  
                bos.close();// 关闭写入流  
            } catch (IOException e) {  
                e.printStackTrace();  
            } 
            mRunning.set(false);
		}
	};
	
	private class PlayRunnable implements Runnable {
		
		private AtomicBoolean mRunning = new AtomicBoolean(false);
		
		private AudioTrack mTrack;

		public boolean isRunning(){
			return mRunning.get();
		}
		
		@Override
		public void run() {
			mRunning.set(true);
			BufferedInputStream bis = null;
			try{
				bis = new BufferedInputStream(new FileInputStream(TestEnv.sTestPCMData));
			} catch (FileNotFoundException e){
				e.printStackTrace();
			}
			
			int bufferSize = AudioTrack.getMinBufferSize(mSampleRateInHz, 
					AudioFormat.CHANNEL_OUT_MONO, 
					mAudioFormat);
			byte[] buffer = new byte[bufferSize]; 
			
			mTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 
					mSampleRateInHz,
					AudioFormat.CHANNEL_OUT_MONO, 
					mAudioFormat, 
					bufferSize,
					AudioTrack.MODE_STREAM);  
			mTrack.play();
			
			try{
				while( bis.available()>0){    
	                 int length = bis.read(buffer);  
	                 if (length > 0){
	                	 //然后将数据写入到AudioTrack中  
	                	 byte[] data = AudioRecorder.bit8ToBit16(buffer, length);
	                	 mTrack.write(data, 0, data.length);     
	                 }
	                     
	             }  
			} catch (Exception e){
				e.printStackTrace();
			}
			mTrack.stop();
			try {  
                bis.close();// 关闭写入流  
            } catch (IOException e) {  
                e.printStackTrace();  
            }
			mRunning.set(false);
		}
		
	}
	
	private Runnable mTimerRunable = new Runnable() {
		@Override
		public void run() {
			updateTime();
			mTextView.postDelayed(mTimerRunable, 1000);
		}
	};
	
	private void updateTime(){
		mTextView.setText(JTimeUtils.getMillisHMSFormat(mState.getRecordedMillis()));
	}
	
	static class RecordState{
		private boolean mRecording = false;
		private long mLastStartTime = 0;
		
		public void onStart(){
			mRecording = true;
			mLastStartTime = System.currentTimeMillis();
		}
		
		public long getRecordedMillis(){
			if (mRecording){
				return System.currentTimeMillis() - mLastStartTime;
			} else {
				return 0;
			}
		}
		
		public void onStop(){
			mRecording = false;
		}
		
		public boolean isRecording(){
			return mRecording;
		}
	}

}
