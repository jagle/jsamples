package com.jagle.samples;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JDebugUtils;

import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by liujie on 15-1-19.
 */
public class AudioRecorderUtil {
    private int mSampleRateInHz = 44100;
    private int mChannelConfig = AudioFormat.CHANNEL_IN_STEREO;
    private int mAudioFormat = AudioFormat.ENCODING_PCM_16BIT;

    private long mPacketDuration = 40;
    private int mReadLength = 0;
    private int mBufferLength = 0;
    private byte[] mBuffer;

    private RecorderCallback mCallback;

    private AudioRecord mAudioRecord;

    private long mTimeStampUs;

    public AudioRecorderUtil(){
    }

    public AudioRecorderUtil setSampleRate(int sampleRate){
        mSampleRateInHz = sampleRate;
        return this;
    }

    public AudioRecorderUtil setChannelConfig(int channelConfig){
        mChannelConfig = channelConfig;
        return this;
    }

    public AudioRecorderUtil setAudioFormat(int audioFormat){
        mAudioFormat = audioFormat;
        return this;
    }

    public AudioRecorderUtil setBufferLength(int length){
        mBufferLength = length;
        return this;
    }

    public AudioRecorderUtil setPacketDuration(long duration){
        mPacketDuration = duration;
        return this;
    }

    public int getChannelCount(){
        int channelCount = mChannelConfig == AudioFormat.CHANNEL_IN_MONO ? 1 : 2;
        return channelCount;
    }

    public void setRecorderCallback(RecorderCallback callback) {
        mCallback = callback;
    }

    public int readData(){
        long nowTime = System.nanoTime();
        int length = mAudioRecord.read(mBuffer, mReadLength ,mBufferLength - mReadLength);
        JLog.d(Developer.Jagle, "audio read length:"+length);
        if (length < 0){
            JLog.e(Developer.Jagle, "audio recorder read error, id:"+ length);
        }

        if (length > 0) {
            mReadLength += length;
            if (mReadLength == mBufferLength) {
                if (mCallback != null) {
                    mCallback.onData(mBuffer, mBufferLength, mTimeStampUs);
                }
                mTimeStampUs += mPacketDuration * 1000000;
                mReadLength = 0;
                if (mTimeStampUs < nowTime) {
                    JLog.v(Developer.Jagle, "adjust timer, diff:" + (nowTime - mTimeStampUs));
                    mTimeStampUs = nowTime;
                }
            }
        }
        return length;
    }

    public int getBitLength(){
        int bitLength = mAudioFormat == AudioFormat.ENCODING_PCM_16BIT ? 2 : 1;
        return bitLength;

    }

    private void initBuffer(){
        mBufferLength = (int)(mSampleRateInHz * getChannelCount() * getBitLength() * mPacketDuration / 1000);
        mBuffer = new byte[mBufferLength];
        JLog.i(Developer.Jagle, "init buffer, length:" + mBufferLength);
    }

    public void init() {
        initBuffer();
        int bufferSize =AudioRecord.getMinBufferSize(mSampleRateInHz,
                mChannelConfig,
                mAudioFormat);

        bufferSize = Math.max(mBufferLength / 2 , bufferSize);

        mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                mSampleRateInHz,
                mChannelConfig,
                mAudioFormat,
                bufferSize) ;

        JLog.i(Developer.Jagle, "Audio Recorder buffer length:"+ bufferSize);
    }

    public void startRecording(){
        JLog.i(Developer.Jagle, "Audio Recorder start");
        mAudioRecord.startRecording();
        mTimeStampUs = System.nanoTime();
        if (mCallback != null){
            mCallback.onStart(mTimeStampUs);
        }
    }

    public void stop(){
        mAudioRecord.stop();
        if (mCallback != null){
            mCallback.onStop(mBuffer, 0, mTimeStampUs);
        }
    }

    public void release() {
        mAudioRecord.release();
    }

    public static interface  RecorderCallback{
        public void onStart(long timeStampUs);

        public void onData(byte[] data, int length, long timeStampUs);

        public void onStop(byte[] data, int length, long timeStampUs);
    }

    public static class AudioRecorderThread{
        private HandlerThread mThread;
        private H mHandler;

        private AtomicBoolean mStartState = new AtomicBoolean(false);

        public AudioRecorderThread(AudioRecorderUtil recorder){
            mThread = new HandlerThread("Audio Record Thread");
            mThread.start();
            mHandler = new H(mThread.getLooper(), recorder);
        }

        public void start(){
            if (mStartState.get()){
                JLog.w(Developer.Jagle, "Audio Recorder has been started");
                return;
            }

            mStartState.set(true);
            mHandler.sendEmptyMessage(H.MSG_START);
        }

        public void stop(){
            if (mStartState.get() == false){
                JLog.w(Developer.Jagle, "Audio Recorder has been stopped");
                return;
            }

            mHandler.mStopped.set(true);
            JLog.i(Developer.Jagle, "stop");

            mStartState.set(false);
            mHandler.removeMessages(H.MSG_READ_DATA);
            mHandler.sendEmptyMessage(H.MSG_STOP);
        }

        public void quit(){
            mThread.quitSafely();
            mThread = null;
            mHandler = null;
        }

        private static class H extends Handler {
            public static final int MSG_START = 0;
            public static final int MSG_READ_DATA = 1;
            public static final int MSG_STOP = 2;

            private long mReadRate;

            private final WeakReference<AudioRecorderUtil> mRef;
            private AtomicBoolean mStopped = new AtomicBoolean(false);

            H(Looper looper, AudioRecorderUtil recorder){
                super(looper);
                mReadRate = recorder.mPacketDuration / 4;
                mRef = new WeakReference<AudioRecorderUtil>(recorder);
            }

            @Override
            public void handleMessage(Message msg) {
                if (mRef.get() == null){
                    JLog.e(Developer.Jagle, "Audio Recorder Util is null, why?");
                    return;
                }

                switch (msg.what){
                    case MSG_START:{
                        mRef.get().startRecording();
                        mStopped.set(false);
                        sendEmptyMessage(MSG_READ_DATA);
                        break;
                    }
                    case MSG_READ_DATA:{
                        long begin = System.currentTimeMillis();
                        while(!mStopped.get()){
                            int length = mRef.get().readData();
                            if (length <= 0){
                                break;
                            }
                        }

                        if (!mStopped.get()) {
                            sendEmptyMessageDelayed(MSG_READ_DATA, mReadRate);
                        }
                        break;
                    }
                    case MSG_STOP:{
                        JLog.i(Developer.Jagle, "MSG_STOP");
                        mRef.get().stop();
                        break;
                    }
                }
                super.handleMessage(msg);
            }
        }
    }


}
