package com.jagle.samples;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jagle.base.JUiActivity;
import com.jagle.common.ViewRef;

public class SocketServiceActivity extends JUiActivity {
	
	private static final String KEY_STORE_PORT = "key.store.port";
	
	ViewRef<TextView> mPort = new ViewRef<TextView>(this, R.id.ass_port);
	ViewRef<TextView> mMessage = new ViewRef<TextView>(this, R.id.ass_message);

	@Override
	public int getContentViewId() {
		return R.layout.activity_socket_service;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		IntentFilter filter = new IntentFilter();
		filter.addAction(SocketService.BROADCAST_RECEIVDD_DATA);
		registerReceiver(mReceiver, filter);
	}
	
	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();
		
		String port = getPref().getString(KEY_STORE_PORT, "12345");
		mPort.get().setText(port);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		getPref().edit().putString(KEY_STORE_PORT, mPort.get().getText().toString()).apply();
	}
	
	public void onStart(View view) {
		Intent intent = new Intent(this ,SocketService.class);
		startService(intent);
	}
	
	public void onStop(View view) {
		
	}
	
	BroadcastReceiver mReceiver = new BroadcastReceiver(){
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action == SocketService.BROADCAST_RECEIVDD_DATA) {
				String msg = intent.getStringExtra(SocketService.KEY_BROADCAST_MESSAGE);
				mMessage.get().append(msg);
				mMessage.get().append("\n");
			}
		};
	};

}
