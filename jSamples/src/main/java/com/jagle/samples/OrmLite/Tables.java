package com.jagle.samples.OrmLite;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DatabaseFieldConfig;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by liujie on 14-8-22.
 */
public interface Tables {

    @DatabaseTable(tableName = "User")
    public static class User {
        @DatabaseField(id = true, canBeNull = false, maxForeignAutoRefreshLevel = DatabaseFieldConfig.NO_MAX_FOREIGN_AUTO_REFRESH_LEVEL_SPECIFIED)
        public String username;

        @DatabaseField(canBeNull = false, maxForeignAutoRefreshLevel = DatabaseFieldConfig.NO_MAX_FOREIGN_AUTO_REFRESH_LEVEL_SPECIFIED)
        public String password;

        @DatabaseField(canBeNull = true, maxForeignAutoRefreshLevel = DatabaseFieldConfig.NO_MAX_FOREIGN_AUTO_REFRESH_LEVEL_SPECIFIED)
        public String nickname;

        public User(){}

        public static String[] getColumns() {
            return new String[] {"username", "password", "nickname"};
        }

        public String[] getValues() {
            return new String[] {username, password, nickname};
        }

    }
}
