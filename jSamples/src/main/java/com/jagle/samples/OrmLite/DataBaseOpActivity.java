package com.jagle.samples.OrmLite;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.Where;
import com.jagle.base.JUiActivity;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.common.ViewRef;
import com.jagle.samples.R;
import com.jagle.samples.OrmLite.Tables.User;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liujie on 14-8-22.
 */
public class DataBaseOpActivity extends JUiActivity{

    ViewRef<EditText> mUsernameEdit = new ViewRef<EditText>(this, R.id.aoc_username_edit);
    ViewRef<EditText> mPasswordEdit = new ViewRef<EditText>(this, R.id.aoc_password_edit);
    ViewRef<EditText> mNickNameEdit = new ViewRef<EditText>(this, R.id.aoc_nickname_edit);
    ViewRef<TextView> mResultText = new ViewRef<TextView>(this, R.id.aoc_result_text);

    private JOrmliteHelper mOrmHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_ormlite_sdk;
    }

    private synchronized  JOrmliteHelper getHelper() {
        if (mOrmHelper == null) {
            mOrmHelper = OpenHelperManager.getHelper(this, JOrmliteHelper.class);
        }
        return mOrmHelper;
    }

    public void onInsert(View view) {
        User user =new User();
        user.username = mUsernameEdit.get().getText().toString();
        user.password = mPasswordEdit.get().getText().toString();
        user.nickname = mNickNameEdit.get().getText().toString();

        try {
            Dao<User, String>  userDao = getHelper().getDao(User.class);

            userDao.createIfNotExists(user);

            mResultText.get().setText("insert user:" + Arrays.toString(user.getValues()));
        }catch (SQLException e) {
            JLog.e(Developer.Jagle, "failed to insert user:" + Arrays.toString(user.getValues()), e);

            mResultText.get().setText("failed to insert user:" + Arrays.toString(user.getValues()));
        }
    }

    public void onQuery(View view) {

        User user =new User();
        user.username = mUsernameEdit.get().getText().toString();
        user.password = mPasswordEdit.get().getText().toString();
        user.nickname = mNickNameEdit.get().getText().toString();
        try {
            Dao<User, String>  userDao = getHelper().getDao(User.class);
            Where<User, String> where = userDao.queryBuilder().where();

            if (user.username.length()  > 0) {
                where.eq("username", user.username);
            } else if (user.password.length()  > 0) {
                where.eq("password", user.password);
            } else if (user.nickname.length()  > 0) {
                where.eq("nickname", user.nickname);
            } else {
                where = null;
            }
            List<User> users;
            if (where != null) {
                users = where.query();
            } else {
                users = userDao.queryForAll();
            }
            String result = Arrays.toString(User.getColumns()) + "\n";
            for(User u : users) {
                result += Arrays.toString(u.getValues()) + "\n";
            }

            mResultText.get().setText(result);

        }catch (SQLException e) {
            JLog.e(Developer.Jagle, "failed to query user:" + Arrays.toString(user.getValues()), e);

            mResultText.get().setText("failed to query user:" + Arrays.toString(user.getValues()));
        }

    }

    public void onDelete(View view) {
        User user =new User();
        user.username = mUsernameEdit.get().getText().toString();
        user.password = mPasswordEdit.get().getText().toString();
        user.nickname = mNickNameEdit.get().getText().toString();
        try {
            Dao<User, String>  userDao = getHelper().getDao(User.class);
            Where<User, String> where = userDao.queryBuilder().where();

            if (user.username.length()  > 0) {
                where.eq("username", user.username);
            } else if (user.password.length()  > 0) {
                where.eq("password", user.password);
            } else if (user.nickname.length()  > 0) {
                where.eq("nickname", user.nickname);
            } else {
                where = null;
            }
            List<User> users;
            if (where != null) {
                users = where.query();
            } else {
                users = userDao.queryForAll();
            }

            userDao.delete(users);

            String result = "delete:\n" + Arrays.toString(User.getColumns()) + "\n";
            for(User u : users) {
                result += Arrays.toString(u.getValues()) + "\n";
            }
            mResultText.get().setText(result);
        }catch (SQLException e) {
            JLog.e(Developer.Jagle, "failed to delete user:" + Arrays.toString(user.getValues()), e);

            mResultText.get().setText("failed to delete user:" + Arrays.toString(user.getValues()));
        }
    }


}
