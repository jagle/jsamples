package com.jagle.samples;

import android.annotation.SuppressLint;

@SuppressLint("SdCardPath")
public class TestEnv {
	public static String sTestImageLocal = "/mnt/sdcard/jagle/samples/test/test_image.png";
	
	public static String sTestImageAsset = "file:///android_asset/test_image.png";
	
	public static String sTestImageWeb = "https://avatars1.githubusercontent.com/u/3478082?s=460";
	
	public static String sQiuniuPutPolicy = "{\"deadline\":1396958615,\"scope\":\"jagle\"}";
	
	public static String sQiniuPutPolicyEncoded = "eyJzY29wZSI6ImphZ2xlIiwiZGVhZGxpbmUiOjEzOTY5NTg2MTV9";
	
	public static int sTestImageResoure = R.drawable.test_image1;

	public static String sTestPCMData = "/mnt/sdcard/jagle/samples/test/test_pcm";
}
