package com.jagle.samples;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jagle.base.JUiActivity;
import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.common.ViewRef;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClientActivity extends JUiActivity {

	private static final String KET_STORE_IP = "key.store.ip";
	private static final String KEY_STORE_MESSAGE = "key.store.meesage";
	private static final String KEY_STORE_PORT = "key.store.port";

	private SharedPreferences mPref;

	ViewRef<TextView> mIP = new ViewRef<TextView>(this, R.id.asc_ip);
	ViewRef<TextView> mPort = new ViewRef<TextView>(this, R.id.asc_port);
	ViewRef<TextView> mMessage = new ViewRef<TextView>(this, R.id.asc_message);

	@Override
	public int getContentViewId() {
		return R.layout.activity_socket_client;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mPref = getPreferences(MODE_PRIVATE);
	}

	@Override
	protected void onCreateContentView() {
		super.onCreateContentView();

		String ip = mPref.getString(KET_STORE_IP, "127.0.0.1");
		mIP.get().setText(ip);

		String message = mPref.getString(KEY_STORE_MESSAGE, "android test");
		mMessage.get().setText(message);

		String port = mPref.getString(KEY_STORE_PORT, "12345");
		mPort.get().setText(port);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mPref.edit().putString(KET_STORE_IP, mIP.get().getText().toString())
				.putString(KEY_STORE_PORT, mPort.get().getText().toString())
				.putString(KEY_STORE_MESSAGE,mMessage.get().getText().toString()).apply();

	}

	public void onSend(View view) {
		Socket socket = null;
		String message = mMessage.get().getText().toString() + "/r/n";
		try {
			// 创建Socket
			// socket = new Socket("192.168.1.110",54321);
			socket = new Socket(mIP.get().getText().toString(), Integer.valueOf(mPort.get().getText().toString())); // IP：10.14.114.127，端口54321
			// 向服务器发送消息
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(socket.getOutputStream())), true);
			out.println(message);

			// 接收来自服务器的消息
			BufferedReader br = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			String msg = br.readLine();

			if (msg != null) {
				Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, "error msg", Toast.LENGTH_LONG).show();
			}
			// 关闭流
			out.close();
			br.close();
			// 关闭Socket
			socket.close();
		} catch (Exception e) {
			// TODO: handle exception
			JLog.e(Developer.Jagle, "", e);
		}
	}
}
