package com.jagle.samples;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.Surface;

import com.jagle.common.Developer;
import com.jagle.common.Encoder;
import com.jagle.common.JLog;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by liujie on 15-1-14.
 */
public class MediaEncoderGroup {
    private static final String MIME_TYPE = "video/avc";
    private static final String AUDIO_MIME_TYPE = "audio/mp4a-latm";

    private static final int FRAME_RATE = 30;               // 30fps
    private static final int IFRAME_INTERVAL = 5;           // 5 seconds between I-frames

    private static final int AUDIO_BIT_RATE = 128000;
    private static final int AUDIO_MAX_SIZE = 10000;

    private ArrayList <MediaEncoder> mEncoders;

    public MediaEncoderGroup()
    {
        mEncoders = new ArrayList<MediaEncoder>();
    }

    public static void sendDataToEncoder(MediaEncoder encoder, byte[] input, int offset,  int length, long presentationTimeNs, boolean endOfStream){
        MediaCodec codec = encoder.mCodec;
        try {
            ByteBuffer[] inputBuffers = codec.getInputBuffers();
            int inputBufferIndex = codec.dequeueInputBuffer(-1);
            if (inputBufferIndex >= 0) {
                ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
                inputBuffer.clear();
                long presentationTimeUs = presentationTimeNs / 1000;
                inputBuffer.put(input, offset, length);

                if (endOfStream) {
                    codec.queueInputBuffer(inputBufferIndex, 0, length, presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                } else {
                    codec.queueInputBuffer(inputBufferIndex, 0, length, presentationTimeUs, 0);
                }
            }
        } catch (Throwable t) {
            JLog.e(Developer.Jagle, encoder.mName + " sendDataToEncoder exception", t);
        }
    }

    public MediaEncoder addVideoEncoder(int width, int height, int bitRate, EncoderCallback callback) throws IOException{
        //check mode first
        MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, width, height);

        // Set some properties.  Faililongng to specify some of these can cause the MediaCodec
        // configure() call to throw an unhelpful exception.
        format.setInteger(MediaFormat.KEY_COLOR_FORMAT,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
        format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
        format.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
        format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, IFRAME_INTERVAL);
        JLog.v(Developer.Jagle, "format: " + format);

        // Create a MediaCodec encoder, and configure it with our format.  Get a Surface
        // we can use for input and wrap it with a class that handles the EGL work.
        MediaEncoder encoder = new MediaEncoder();
        encoder.mCodec = MediaCodec.createEncoderByType(MIME_TYPE);
        encoder.mCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);

        encoder.mFormat = format;
        encoder.mEncoderCallback = callback;
        encoder.mName = "Video Encoder";
        mEncoders.add(encoder);
        return encoder;
    }

    public MediaEncoder addAudioEncoder(int samepleRate, int channelCount, EncoderCallback callback) throws IOException {
        //check mode first

        // Set some properties.  Faililongng to specify some of these can cause the MediaCodec
        // configure() call to throw an unhelpful exception.
        MediaFormat format = new MediaFormat();
        format.setString(MediaFormat.KEY_MIME, AUDIO_MIME_TYPE);
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        format.setInteger(MediaFormat.KEY_BIT_RATE, AUDIO_BIT_RATE);
        format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, AUDIO_MAX_SIZE);
        format.setInteger(MediaFormat.KEY_SAMPLE_RATE, samepleRate);
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, channelCount);
        JLog.v(Developer.Jagle, "format: " + format);

        // Create a MediaCodec encoder, and configure it with our format
        MediaEncoder encoder = new MediaEncoder();
        encoder.mCodec = MediaCodec.createEncoderByType(AUDIO_MIME_TYPE);
        encoder.mCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);

        encoder.mFormat = format;
        encoder.mEncoderCallback = callback;
        encoder.mName = "Audio Encoder";
        mEncoders.add(encoder);
        return encoder;
    }

    public MediaEncoder addMediaEncoder(MediaFormat format, String type, EncoderCallback callback) throws IOException{
        JLog.v(Developer.Jagle, "format: " + format);
        MediaEncoder encoder = new MediaEncoder();
        encoder.mCodec = MediaCodec.createEncoderByType(type);
        encoder.mCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);

        encoder.mFormat = format;
        encoder.mEncoderCallback = callback;
        encoder.mName = type;
        mEncoders.add(encoder);
        return encoder;
    }



    public void release(){

        for(MediaEncoder encoder : mEncoders){
            encoder.mCodec.stop();
            encoder.mCodec.release();
        }

        mEncoders.clear();
    }

    public void drainAllEncorder(boolean endOfStream){
        for(MediaEncoder encoder : mEncoders){
            if (endOfStream && encoder.mInputSurface != null){
                encoder.mCodec.signalEndOfInputStream();
                JLog.i(Developer.Jagle, "signalEndOfInputStream");
            }
            drainEncoder(encoder, endOfStream);
        }
    }

    /**
     * Extracts all pending data from the encoder and forwards it to the muxer.
     * <p/>
     * If endOfStream is not set, this returns when there is no more data to drain.  If it
     * is set, we send EOS to the encoder, and then iterate until we see EOS on the output.
     * Calling this with endOfStream set should be done once, right before stopping the muxer.
     * <p/>
     * We're just using the muxer to get a .mp4 file (instead of a raw H.264 stream).  We're
     * not recording isAudio.
     */
    public void drainEncoder( MediaEncoder encoder, boolean endOfStream) {
        MediaCodec codec = encoder.mCodec;
        MediaCodec.BufferInfo bufferInfo = encoder.mBufferInfo;
        String tag = encoder.mName + "::";
        final int TIMEOUT_USEC = 10000;
        EncoderCallback callback = encoder.mEncoderCallback;

        ByteBuffer[] encoderOutputBuffers = codec.getOutputBuffers();
        while (true) {
            int encoderStatus = codec.dequeueOutputBuffer(bufferInfo, TIMEOUT_USEC);
            if (encoderStatus == MediaCodec.INFO_TRY_AGAIN_LATER) {
                // no output available yet
                if (!endOfStream) {
                    break;      // out of while
                } else {
                    JLog.d(Developer.Jagle, tag + " no output available, spinning to await EOS");
                }
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                // not expected for an encoder
                encoderOutputBuffers = codec.getOutputBuffers();
            } else if (encoderStatus == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                // should happen before receiving buffers, and should only happen once
                JLog.d(Developer.Jagle,tag + "format changed");
                MediaFormat newFormat = codec.getOutputFormat();
                if (callback != null){
                    callback.onFormatChanged(newFormat);
                }

            } else if (encoderStatus < 0) {
                JLog.w(Developer.Jagle, tag + " unexpected result from encoder.dequeueOutputBuffer: " +
                        encoderStatus);
                // let's ignore it
            } else {
                ByteBuffer encodedData = encoderOutputBuffers[encoderStatus];
                if (encodedData == null) {
                    throw new RuntimeException(tag + " encoderOutputBuffer " + encoderStatus +
                            " was null");
                }

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
                    // The codec config data was pulled out and fed to the muxer when we got
                    // the INFO_OUTPUT_FORMAT_CHANGED status.  Ignore it.
                    JLog.d(Developer.Jagle, tag + " ignoring BUFFER_FLAG_CODEC_CONFIG");
                    bufferInfo.size = 0;
                }


                if (bufferInfo.size != 0) {
                    // adjust the ByteBuffer values to match BufferInfo (not needed?)
                    encodedData.position(bufferInfo.offset);
                    encodedData.limit(bufferInfo.offset + bufferInfo.size);

                    if (callback != null){
                        callback.onData(encodedData, bufferInfo, endOfStream);
                    }
                }

                codec.releaseOutputBuffer(encoderStatus, false);

                if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
                    if (!endOfStream) {
                        JLog.w(Developer.Jagle, tag + " reached end of stream unexpectedly");
                    } else {
                        JLog.d(Developer.Jagle, tag + " end of stream reached");
                    }

                    encoder.mStopped = true;

                    if (callback != null){
                        callback.onDataEnd();
                    }
                    break;      // out of while
                }
            }
        }
    }


    public void restartAll(){
        for(MediaEncoder encoder : mEncoders){
            encoder.mCodec.release();
            encoder.mCodec.start();
        }
    }

    public static interface EncoderCallback {
        // should happen before receiving buffers, and should only happen once
        public void onFormatChanged(MediaFormat format);

        public void onData(ByteBuffer data, MediaCodec.BufferInfo bufferInfo, boolean endOfStream);

        public void onDataEnd();
    }

    public static class MediaEncoder{
        public String mName;
        public MediaCodec mCodec;
        public MediaFormat mFormat;
        public MediaCodec.BufferInfo mBufferInfo = new MediaCodec.BufferInfo();
        public Surface mInputSurface;
        public boolean mStopped = false;
        public EncoderCallback mEncoderCallback;
    }

    public static class EncoderDequeueThread{
        private HandlerThread mThread;
        private H mHandler;

        private AtomicBoolean mStartState = new AtomicBoolean(false);

        public EncoderDequeueThread(MediaEncoderGroup group){
            mThread = new HandlerThread("Media Encoder Thread");
            mThread.start();
            mHandler = new H(mThread.getLooper(), group);
        }

        public void startDequeue(long rate){
            if (mStartState.get()){
                JLog.w(Developer.Jagle, "Media Encoder has been started");
                return;
            }

            mStartState.set(true);
            mHandler.mReadRate = rate;
            mHandler.sendEmptyMessage(H.MSG_START);

        }

        public void stopDequeue(int delay){
            if (mStartState.get() == false){
                JLog.w(Developer.Jagle, "Media Encoder has been stopped");
                return;
            }

            mStartState.set(false);
            mHandler.removeMessages(H.MSG_READ_DATA);
            if (delay>0){
                mHandler.sendEmptyMessageDelayed(H.MSG_STOP, delay);
            } else {
                mHandler.sendEmptyMessage(H.MSG_STOP);
            }
        }

        public void quit(){
            mThread.quitSafely();
            mThread = null;
            mHandler = null;
        }

        private static class H extends Handler{
            public static final int MSG_START = 0;
            public static final int MSG_READ_DATA = 1;
            public static final int MSG_STOP = 2;

            private long mReadRate;

            private final WeakReference<MediaEncoderGroup> mRef;

            H(Looper looper, MediaEncoderGroup encoder){
                super(looper);
                mRef = new WeakReference<MediaEncoderGroup>(encoder);
            }

            @Override
            public void handleMessage(Message msg) {
                if (mRef.get() == null){
                    JLog.e(Developer.Jagle, "Media Encoder is null, why?");
                }

                switch (msg.what){
                    case MSG_START:{
                        mRef.get().drainAllEncorder(false);
                        removeMessages(MSG_READ_DATA);
                        sendEmptyMessageDelayed(MSG_READ_DATA, mReadRate);
                        break;
                    }
                    case MSG_READ_DATA:{
                        mRef.get().drainAllEncorder(false);
                        sendEmptyMessageDelayed(MSG_READ_DATA, mReadRate);
                        break;
                    }
                    case MSG_STOP:{
                        removeMessages(MSG_READ_DATA);
                        mRef.get().drainAllEncorder(true);
                        break;
                    }
                }
                super.handleMessage(msg);
            }
        }

    }

}
