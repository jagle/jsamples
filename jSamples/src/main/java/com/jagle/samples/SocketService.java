package com.jagle.samples;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.jagle.common.Developer;
import com.jagle.common.JLog;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class SocketService extends Service {
	
	private static final int SERVICE_PORT = 12345;
	public static final String BROADCAST_RECEIVDD_DATA = "broadcast.received.data";
	public static final String KEY_BROADCAST_MESSAGE = "key.broadcast.message";
	
	private AtomicBoolean mRunning = new AtomicBoolean(true);
	
	SocketThread mThread;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		mThread = new SocketThread();
		mThread.start();
	}
	
	public void stop(){
		mRunning.set(false);
	}
	
	public Client getClient(Socket socket){
		return new Client(socket);
	}
	
	private class SocketThread extends Thread{
		ServerSocket mService;
		
		public SocketThread() {
			super("SocketThread");
		}
		
		@Override
		public void run() {
			try {
				mService = new ServerSocket(SERVICE_PORT);
			} catch (IOException e) {
				JLog.e(Developer.Jagle, "start service failed", e);
				return;
			}
			
			Socket client = null; 
			while(mRunning.get()) {
				try {
					client = mService.accept();
					
				} catch (IOException e) {
					JLog.e(Developer.Jagle, "get client failed", e);
					continue;
				} 
				getClient(client);
			}
		}
		
	}
	
	private class Client extends SocketClient {

		public Client(Socket socket) {
			super(socket);
		}
		
		@Override
		protected void onReceivedData(byte[] data) {
			write(("received:" + data.toString()).getBytes());
			Intent intent = new Intent(BROADCAST_RECEIVDD_DATA);
			String msg = getName() + " received:" + new String(data);
			intent.putExtra(KEY_BROADCAST_MESSAGE, msg);
			sendBroadcast(intent);
		}
	}

}
