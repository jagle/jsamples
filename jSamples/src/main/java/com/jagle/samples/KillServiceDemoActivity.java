package com.jagle.samples;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.jagle.base.JLinearLayout;
import com.jagle.base.JListAdapter;
import com.jagle.base.JUiActivity;
import com.jagle.common.ViewRef;
import com.jagle.utils.JUI;
import com.jagle.utils.ShellCmd;

import java.util.ArrayList;
import java.util.List;


public class KillServiceDemoActivity extends JUiActivity {

    ViewRef<ListView> mProcessList = new ViewRef<ListView>(this, R.id.ksd_process_list);

    ProcessListAdapter mAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.kill_service_demo, menu);
        return true;
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();

        mAdapter = new ProcessListAdapter(getContext());
        mProcessList.get().setAdapter(mAdapter);

        updateProcessListPs();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_kill_service_demo;
    }

    private void updateProcessList() {
        ActivityManager actvityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = actvityManager.getRunningAppProcesses();
       // mAdapter.setDatas(procInfos);
    }

    private void updateProcessListPs() {
        ArrayList<String> ret = ShellCmd.docmd("ps mediaserver");
        mAdapter.setDatas(ret);
    }

    class ProcessListAdapter extends JListAdapter<String> {

        public ProcessListAdapter(Context context) {
            super(context, KillServiceListItem.class);
        }

        @Override
        public void updateView(View view, String data) {
            if (data == null) {
                return;
            }

            String[] datas = data.split("\\s+");

            if (datas == null){
                return;
            }
            KillServiceListItem item = (KillServiceListItem) view;
            item.setDatas(datas[1],datas[2], datas[datas.length - 1]);
        }
    }

    public void onKill(View view) {
        List<String> datas = mAdapter.getDatas();
        String[] tmp = datas.get(1).split("\\s+");
        ShellCmd.startSuProcess("kill -9 " + tmp[1]);

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                updateProcessListPs();
            }
        }, 200);


    }


}
