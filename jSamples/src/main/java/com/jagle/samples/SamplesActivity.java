package com.jagle.samples;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.jagle.base.JListAdapter;
import com.jagle.base.JUiActivity;
import com.jagle.samples.Keyboard.ImePreferences;
import com.jagle.samples.OrmLite.DataBaseOpActivity;

import java.util.ArrayList;

public class SamplesActivity extends Activity {
	
	private ListView mListView;
	private ActivityListAdapter mAdapter;
	
	static {  
//		  System.loadLibrary("yuv");
//        System.loadLibrary("RgbToYuv");
    } 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_samples);
		
		mListView = (ListView) findViewById(R.id.as_demo_list);
		mAdapter = new ActivityListAdapter(this);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				ActivitDemoPair pair = mAdapter.getItem(position);
				Intent in = new Intent(SamplesActivity.this, pair.mCls);
				in.putExtra(JUiActivity.DATA_ACTIVITY_TITLE, pair.mName);
				startActivity(in);
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	static class ActivitDemoPair{
		public Class<?> mCls;
		public String mName;
		
		public ActivitDemoPair(Class<?> cls, String name) {
			mCls = cls;
			mName = name;
		}
		
		public static ActivitDemoPair build(Class<?> cls, String name){
			return new ActivitDemoPair(cls, name);
		}
	}
	
	static class ActivityListAdapter extends JListAdapter<ActivitDemoPair>{

		public ActivityListAdapter(Context context) {
			super(context, R.layout.samples_list_item);
			
			mObjects = new ArrayList<ActivitDemoPair>();
			mObjects.add(ActivitDemoPair.build(TouchDrawDemo.class, "Touch Draw Demo"));
			mObjects.add(ActivitDemoPair.build(ChangeInputMethod.class, "Change Input Method Demo"));
			mObjects.add(ActivitDemoPair.build(ShellCmdDemo.class, "Shell Command Demo"));
			mObjects.add(ActivitDemoPair.build(RemoteInputActivity.class, "Remote Input Demo"));
			mObjects.add(ActivitDemoPair.build(ImePreferences.class, "Ime Preferences"));
			mObjects.add(ActivitDemoPair.build(ShareSdkActivity.class, "ShareSDK demo"));
			mObjects.add(ActivitDemoPair.build(RgbToYuvActivity.class, "RGB To YUV demo"));
			mObjects.add(ActivitDemoPair.build(QiniuSdkActivity.class, "Qiniu Sdk demo"));
			mObjects.add(ActivitDemoPair.build(CameraComposeDemo.class, "Camera Compose demo"));
			mObjects.add(ActivitDemoPair.build(AudioRecorderDemo.class, "Audio Recorder demo"));
			mObjects.add(ActivitDemoPair.build(InnerAudioRecorderDemo.class, "Inner Audio Recorder demo"));
			mObjects.add(ActivitDemoPair.build(SocketClientActivity.class, "socket client demo"));
			mObjects.add(ActivitDemoPair.build(SocketServiceActivity.class, "socket service demo"));
            mObjects.add(ActivitDemoPair.build(DataBaseOpActivity.class, "OrmLite Data base demo"));
            mObjects.add(ActivitDemoPair.build(KillServiceDemoActivity.class, "Kill Service Demo"));
            mObjects.add(ActivitDemoPair.build(PcmConvertActivty.class, "PCM Convert Demo"));
            mObjects.add(ActivitDemoPair.build(MediaProjectActivity.class, "Virtual Screen Demo"));
            //mObjects.add(ActivitDemoPair.build(ScreenRecordActivity.class, "Screen Record Demo"));
            mObjects.add(ActivitDemoPair.build(ScreenRecordActivity2.class, "Screen Record Demo2"));
		}
		
		@Override
		public void updateView(View view, ActivitDemoPair data) {
			((TextView)view).setText(data.mName);
		}
		
	}

}
