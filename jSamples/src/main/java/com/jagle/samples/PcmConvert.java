package com.jagle.samples;

/**
 * Created by liujie on 14-10-9.
 */
public class PcmConvert {

    static {
        System.loadLibrary("PcmConvert");
    }

    public native static int floatToInt16(byte[] data, int lenth);

    public native static int floatToInt16_2(byte[] data, int lenth);

}
