package com.jagle.samples;

import android.media.MediaFormat;
import android.media.MediaMuxer;

import com.jagle.common.Developer;
import com.jagle.common.JLog;

import java.io.File;
import java.io.IOException;

/**
 * Created by liujie on 15-1-15.
 */
public class Mp4Writer {

    public static final int K_NONE = 0;
    public static final int K_VIDEO = 1;
    public static final int K_AUDIO = 2;
    public static final int K_VIDEO_AUDIO = K_AUDIO | K_VIDEO;

    private MediaMuxer mMuxer;

    private final int mTraceMode;

    private int mCurrentTraceFlag;

    private int mAudioTrace = -1;
    private int mVideoTrace = -1;

    private boolean mMuxerStarted;


    public Mp4Writer(int mode, File outputFile) throws IOException {
        mTraceMode = mode;
        mCurrentTraceFlag = K_NONE;

        mMuxer = new MediaMuxer(outputFile.toString(),MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
    }

    public void addAudioTrace(MediaFormat format){
        if ((mTraceMode & K_AUDIO) == 0){
            JLog.e(Developer.Jagle, " can't add audio trace with mode:" + mTraceMode);
            return;
        }

        if(mAudioTrace != -1 ){
            JLog.e(Developer.Jagle, "already add audio trace");
            return;
        }
        mAudioTrace = mMuxer.addTrack(format);
        mCurrentTraceFlag |= K_AUDIO;

        if (mCurrentTraceFlag == mTraceMode){
            start();
        }
    }

    public void addVideoTrace(MediaFormat format){
        if ((mTraceMode & K_VIDEO) == 0){
            JLog.e(Developer.Jagle, " can't add video trace with mode:" + mTraceMode);
            return;
        }

        if(mVideoTrace != -1 ){
            JLog.e(Developer.Jagle, "already add video trace");
            return;
        }

        mVideoTrace = mMuxer.addTrack(format);
        mCurrentTraceFlag |= K_VIDEO;

        if (mCurrentTraceFlag == mTraceMode){
            start();
        }
    }

    public void writeAudioData(java.nio.ByteBuffer byteBuf, android.media.MediaCodec.BufferInfo bufferInfo){
        if (!mMuxerStarted) {
            throw new RuntimeException("MediaMuxer hasn't started");
        }
        mMuxer.writeSampleData(mAudioTrace, byteBuf, bufferInfo);
    }

    public void writeVideoData(java.nio.ByteBuffer byteBuf, android.media.MediaCodec.BufferInfo bufferInfo) {
        if (!mMuxerStarted) {
            throw new RuntimeException("MediaMuxer hasn't started");
        }
        mMuxer.writeSampleData(mVideoTrace, byteBuf, bufferInfo);
    }

    public void onAudioStopped(){
        JLog.v(Developer.Jagle, "audio trace end");
        if (mAudioTrace == -1){
            JLog.e(Developer.Jagle, "no audio trace");
            return;
        }

        mAudioTrace = -1;

        mCurrentTraceFlag &= ~K_AUDIO;

        if (mCurrentTraceFlag == K_NONE){
            stop();
        }
    }

    public void onVideoStopped(){
        JLog.v(Developer.Jagle, "video trace end");
        if (mVideoTrace == -1){
            JLog.e(Developer.Jagle, " no video trace");
            return;
        }

        mVideoTrace = -1;

        mCurrentTraceFlag &= ~K_VIDEO;

        if (mCurrentTraceFlag == K_NONE){
            stop();
        }
    }

    private void start(){
        JLog.d(Developer.Jagle, "MP4 writer start");
        mMuxer.start();
        mMuxerStarted = true;
    }

    private void stop(){
        JLog.d(Developer.Jagle, "MP4 writer stop");
        mMuxer.stop();
        mMuxerStarted = false;
    }

}
