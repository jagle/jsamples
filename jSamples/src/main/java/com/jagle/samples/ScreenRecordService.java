package com.jagle.samples;

import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.TextView;

import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.utils.JTimeUtils;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

public class ScreenRecordService extends JService {

    public static final int COMMAND_START_RECORD = 0;
    public static final int COMMAND_STOP_RECORD = 1;
    public static final int COMMAND_SHOW_VIDEO = 2;

    private static final int PERMISSION_CODE = 1;

    private int mScreenDensity;
    private MediaProjectionManager mProjectionManager;

    private int mVideoWidth = 720;
    private int mVideoHeight = 1280;
    private boolean mScreenRecording;

    private long mLastStartTime = 0;

    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private Surface mSurface;

    private VideoEncoderCore mVideoEncoder;
    private File mOutputFile;

    private TextView mTextView;

    private HandlerThread mThread;
    private Handler mHandler;

    private WindowManager mWm;

    @Override
    public void onCreate() {
        super.onCreate();

        mWm = (WindowManager)getSystemService(WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        mWm.getDefaultDisplay().getMetrics(metrics);
        mScreenDensity = metrics.densityDpi;
        mProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        mThread = new HandlerThread("Screen Record Thread");
        mThread.start();

        mHandler = new H(mThread.getLooper(), this);
    }


    private VirtualDisplay createVirtualDisplay() {
        return mMediaProjection.createVirtualDisplay("ScreenSharingDemo",
                mVideoWidth, mVideoHeight, mScreenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC,
                mSurface, null /*Callbacks*/, null /*Handler*/);
    }

    private void startEncoder() {
        JLog.d(Developer.Jagle, "starting to record");
        // Record at 1280x720, regardless of the window dimensions.  The encoder may
        // explode if given "strange" dimensions, e.g. a width that is not a multiple
        // of 16.  We can box it as needed to preserve dimensions.
        final int BIT_RATE = 4000000;   // 4Mbps
        try {
            mOutputFile = createOutputFile();
            mVideoEncoder = new VideoEncoderCore(mVideoWidth, mVideoHeight,
                    BIT_RATE, mOutputFile);
            mSurface = mVideoEncoder.getInputSurface();
        } catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
    }

    private File createOutputFile() {
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_MOVIES), String.valueOf(System.currentTimeMillis()) + ".mp4");
        return file;
    }

    private void startRecord() {

        startEncoder();
        mVideoEncoder.drainVideoEncoder(false);

        mScreenRecording = true;
        if (mSurface == null) {
            return;
        }
        if (mMediaProjection == null) {
            startActivity(mProjectionManager.createScreenCaptureIntent());
            return;
        }
        mVirtualDisplay = createVirtualDisplay();


    }

    private void stopRecord() {
        mScreenRecording = false;
        if (mVideoEncoder != null) {
            mVideoEncoder.drainVideoEncoder(true);
            mVideoEncoder.release();
            mVideoEncoder = null;
        }

        if (mVirtualDisplay == null) {
            return;
        }
        mVirtualDisplay.release();
        mVirtualDisplay = null;

    }

    private void updateTime() {
        Long diff = System.currentTimeMillis() - mLastStartTime;
        mTextView.setText(JTimeUtils.getMillisHMSFormat(diff));
    }

    private Runnable mTimerRunable = new Runnable() {
        @Override
        public void run() {
            updateTime();
            mTextView.postDelayed(mTimerRunable, 1000);
        }
    };

    public void showVideo(){
        Uri uri = Uri.parse("file://" + mOutputFile.getAbsolutePath());
        String type = "video/mp4";
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setDataAndType(uri, type);
        startActivity(intent);
    }


    private static class H extends Handler{

        public static final int MSG_STRAR_RECORD = 0;
        public static final int MSG_STOP_RECORD = 1;
        public static final int MSG_SHOW_VIDEO = 2;

        private final WeakReference<ScreenRecordService> mServiceRef;


        H(Looper looper, ScreenRecordService service){
            super(looper);
            mServiceRef = new WeakReference<ScreenRecordService>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case MSG_STRAR_RECORD:{
                    mServiceRef.get().startRecord();
                    break;
                }

                case MSG_STOP_RECORD:{
                    mServiceRef.get().stopRecord();
                    break;
                }
                case MSG_SHOW_VIDEO:{
                    mServiceRef.get().showVideo();
                    break;
                }
            }
            super.handleMessage(msg);
        }
    }

    public ScreenRecordService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }

    @Override
    protected void onCommand(int cmd) {

        switch (cmd){
            case COMMAND_START_RECORD:{
                if(!mScreenRecording){
                    mHandler.sendEmptyMessage(H.MSG_STRAR_RECORD);
                }
            }


        }
        super.onCommand(cmd);
    }
}
