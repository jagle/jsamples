package com.jagle.common;


import android.content.Context;
import android.content.SharedPreferences;

import com.jagle.base.JApplication;

import java.util.ArrayList;
import java.util.Arrays;

public class AppPreferences {

	// app prefile
	public static final String KEY_UUID = "key.uuid";

	// / name
	private static final String CONFIG_PREFERENCE_NAME = "com.jagle.app.preference";
	private static SharedPreferences sAppPrefs;

	static {
		sAppPrefs = JApplication.getApp().getSharedPreferences(
				CONFIG_PREFERENCE_NAME, Context.MODE_PRIVATE);
	}

	// / remove interface
	public static void remove(String key) {
		sAppPrefs.edit().remove(key).commit();
	}
	
	public static String getString( String key){
		return getString( key ,null);
	}
	
	public static String getString( String key, String def){
		String s = sAppPrefs.getString(key, def);
		return s;
	}
	
	public static int getInt( String key){
		return getInt( key, -1);
	}
	
	public static int getInt( String key, int def){
		int value = sAppPrefs.getInt(key, def);
		return value;
	}
	
	public static boolean getBoolean( String key){
		return getBoolean( key, false);
	}
	
	public static boolean getBoolean( String key, boolean def){
		boolean value = sAppPrefs.getBoolean(key, def);
		return value;
	}
	
	public static long getLong( String key){
		return getLong( key, -1);
	}
	
	public static long getLong(String key, long def){
		long value = sAppPrefs.getLong(key, def);
		return value;
	}
	
	public static boolean setValue(String key, boolean value){
		return sAppPrefs.edit().putBoolean(key, value).commit();
	}
	
	public static boolean setValue( String key, int value){
		return sAppPrefs.edit().putInt(key, value).commit();
	}
	
	public static boolean setValue(String key, String value){
		return sAppPrefs.edit().putString(key, value).commit();
	}
	
	public static boolean setValue(String key, Long value){
		return sAppPrefs.edit().putLong(key, value).commit();
	}
	
	public static SharedPreferences.Editor geEditor(){
		return sAppPrefs.edit();
	}
	
	public static void setValueAsync(String key, boolean value){
		sAppPrefs.edit().putBoolean(key, value).apply();
	}
	
	public static void setValueAsync( String key, int value){
		sAppPrefs.edit().putInt(key, value).apply();
	}
	
	public static void setValueAsync(String key, String value){
		sAppPrefs.edit().putString(key, value).apply();
	}
	
	public static void setValueAsync(String key, Long value){
		sAppPrefs.edit().putLong(key, value).apply();
	}
	
	public static void setValueAsync(String key, ArrayList<String> values, String divider){
		StringBuilder sb = new StringBuilder();
		for (String value : values){
			sb.append(value);
			sb.append(divider);
		}
		setValueAsync(key, sb.substring(0, sb.length() - divider.length()));
	}
	
	public static void setValueAsync(String key, String[] values, String divider){
		StringBuilder sb = new StringBuilder();
		for (String value : values){
			sb.append(value);
			sb.append(divider);
		}
		setValueAsync(key, sb.substring(0, sb.length() - divider.length()));
	}
	
	public static String[] getStringArray(String key, String divider){
		String value = getString(key);
		if (value == null){
			return null;
		}
		
		String[] values = value.split(divider);
		return values;
	}
	
	public static ArrayList<String> getStringArrayList(String key, String divider){
		String[] array = getStringArray(key, divider);
		ArrayList<String> list = new ArrayList<String>(Arrays.asList(array));
		return list;
	}
}
