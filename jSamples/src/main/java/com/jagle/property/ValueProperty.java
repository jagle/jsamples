package com.jagle.property;

import android.os.Looper;

import com.jagle.utils.JUI;

import java.util.HashSet;
import java.util.Set;

/**
 * @author qingfeng
 */
public class ValueProperty<T> implements IValueProperty<T> {
    private final Set<OnChangedHandler> mHandlers = new HashSet<OnChangedHandler>();
    private T mValue;
    private T mDefaultValue;

    public ValueProperty(T defaultValue) {
        mDefaultValue = defaultValue;
        reset();
    }

    public ValueProperty() {
        mDefaultValue = null;
        reset();
    }

    public synchronized T get() {
        return mValue;
    }

    public synchronized void set(final T value) {
        if (null != mValue && mValue.equals(value))
            return;
        mValue = value;
        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(value);
                }
            });
        } else {
            onPropChange(value);
        }
    }

    private void onPropChange(T value) {
        OnChangedHandler[] copyHandlers;
        synchronized (mHandlers) {
            copyHandlers = new OnChangedHandler[mHandlers.size()];
            mHandlers.toArray(copyHandlers);
        }
        for (OnChangedHandler handler : copyHandlers) {
            handler.onPropChange(value);
        }
    }


    public void reset() {
        set(mDefaultValue);
    }

    public void addPropChangeHandler(OnChangedHandler<T> handler) {
        synchronized (mHandlers) {
            mHandlers.add(handler);
        }
    }

    public void removePropChangeHandler(OnChangedHandler<T> handler) {
        synchronized (mHandlers) {
            mHandlers.remove(handler);
        }
    }
}
