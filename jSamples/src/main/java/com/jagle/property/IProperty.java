package com.jagle.property;

/**
 * @author qingfeng
 */
public interface IProperty {
	public static interface IPropChangeHandler {
		public void onPropChange(Object value);
	}
	public Object getObject();
	public void setObject(Object v);
	public void reset();

	public void addPropChangeHandler(IPropChangeHandler handler);
	public void removePropChangeHandler(IPropChangeHandler handler);
}
