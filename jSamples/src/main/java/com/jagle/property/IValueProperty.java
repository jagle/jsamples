package com.jagle.property;

/**
 * @author qingfeng
 */
public interface IValueProperty<T> {
	public static interface OnChangedHandler<T> {
		public void onPropChange(T value);
	}
	public T get();
	public void set(T v);
	public void reset();

	public void addPropChangeHandler(OnChangedHandler<T> handler);
	public void removePropChangeHandler(OnChangedHandler<T> handler);
}
