package com.jagle.property;

import java.util.List;

/**
 * @author qingfeng
 */
public interface IListProperty<T> {
	public static interface IPropChangeHandler<T> {
		public void onPropChange(List<T> value);
	}
	public List<T> get();
	public void set(List<T> value);
    public void add(T obj);
    public void add(int i, T obj);
    public void remove(T obj);
    public void remove(int i);
	public void reset();

	public void addPropChangeHandler(IPropChangeHandler<T> handler);
	public void removePropChangeHandler(IPropChangeHandler<T> handler);
}
