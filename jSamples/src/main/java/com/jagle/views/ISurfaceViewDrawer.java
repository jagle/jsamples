package com.jagle.views;

import android.graphics.Canvas;
import android.graphics.Rect;

public interface ISurfaceViewDrawer {
	public void draw(Canvas canvas);
	
	public void dirtyRect(Rect rect);
	
	public void refresh();
	
	public void clear();
}
