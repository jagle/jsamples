package com.jagle.views;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.jagle.base.JListAdapter;
import com.jagle.samples.R;
import com.jagle.sdks.views.AsyncImageView;

public class ImageListAdapter extends JListAdapter<String> {

    private int mItemHeight = 0;
    private int mMaxImageNumber = -1;
    private boolean mAutoAddLastItem = false;

    private ImageSelecorListener mListener;
    private ImageLongClickedListener mLongClickListener;

    private View.OnClickListener mLastItemClickLitener;
    private View.OnLongClickListener mLastItemLongClickLitener;

    public ImageListAdapter(Context context) {
        this(context, R.layout.image_grid_item);
    }

    public ImageListAdapter(Context context, int resource) {
        super(context, resource);
        mLastItemClickLitener = new LastItemClickListener();
        mLastItemLongClickLitener = new LastItemLongClickedListener();
    }

    public void setAutoAddLastItem(boolean add) {
        mAutoAddLastItem = add;
    }

    public boolean isAutoAddLastItem() {
        return mAutoAddLastItem;
    }

    public void setMaxImageNumber(int number) {
        mMaxImageNumber = number;
    }

    public int getMaxImageNumber() {
        return mMaxImageNumber;
    }

    public boolean addImage(String image) {
        mObjects.add(image);
        notifyDataSetChanged();
        return true;
    }

    public void setImageSelecorListener(ImageSelecorListener listener) {
        mListener = listener;
    }

    public void setImageLongClickedListener(ImageLongClickedListener listener) {
        mLongClickListener = listener;
    }

    public String[] getImageArray() {
        return mObjects.toArray(new String[mObjects.size()]);
    }

    @Override
    public int getCount() {
        if (mAutoAddLastItem) {
            return super.getCount() + 1;
        } else {
            return super.getCount();
        }
    }

    @Override
    public String getItem(int position) {
        if (mAutoAddLastItem && position == getCount() - 1) {
            return null;
        }
        return super.getItem(position);
    }

    @Override
    public void updateView(View view, int position) {
        if (mAutoAddLastItem && position == getCount() - 1) {
            if (view instanceof AsyncImageView) {
                ((AsyncImageView) view).clear();
            }
            ((ImageView) view).setImageResource(getLastDrawable());
            view.setOnClickListener(mLastItemClickLitener);
            view.setLongClickable(false);
            view.setOnLongClickListener(mLastItemLongClickLitener);
        } else {
            updateView(view, getItem(position));
            view.setOnClickListener(new ImageItemClickListener(position));
            view.setOnLongClickListener(new ImageItemLongClickedListener(position));
        }

    }

    @Override
    public void updateView(View view, String data) {
        AsyncImageView v = (AsyncImageView) view;
        v.setImageURI(data);
    }

    @Override
    public View createView(int position) {
        View v = super.createView(position);
        v.setClickable(true);
        //v.setLongClickable(true);
        return v;
    }

    @Override
    public void notifyDataSetChanged() {
        if (mMaxImageNumber != -1) {
            mAutoAddLastItem = mObjects.size() < mMaxImageNumber;
        }
        super.notifyDataSetChanged();
    }

    public void setItemHeight(int height) {
        if (mItemHeight == height) {
            return;
        }

        mItemHeight = height;
        notifyDataSetChanged();
    }

    public int getItemHeight() {
        return mItemHeight;
    }

    public int getLastDrawable() {
        return R.drawable.icon_topic_image_add;
    }

    class LastItemClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onLastItemClieked(v);
            }
        }
    }

    class ImageItemClickListener implements View.OnClickListener {
        private final int mPosition;

        public ImageItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View v) {
            if (mListener != null) {
                mListener.onImageSelected(mPosition, getItem(mPosition));
            }
        }
    }

    class LastItemLongClickedListener implements View.OnLongClickListener {

        @Override
        public boolean onLongClick(View v) {
            if (mLongClickListener != null) {
                return mLongClickListener.onLastItemLongClieked(v);
            } else {
                return false;
            }
        }

    }

    class ImageItemLongClickedListener implements View.OnLongClickListener {

        private final int mPosition;

        public ImageItemLongClickedListener(int position) {
            mPosition = position;
        }

        @Override
        public boolean onLongClick(View v) {
            if (mLongClickListener != null) {
                return mLongClickListener.onImageLongClieked(mPosition,
                        getItem(mPosition));
            } else {
                return false;
            }
        }
    }

    public static interface ImageSelecorListener {
        void onLastItemClieked(View v);

        void onImageSelected(int position, String data);
    }

    public static interface ImageLongClickedListener {
        boolean onLastItemLongClieked(View v);

        boolean onImageLongClieked(int position, String data);
    }

}
