package com.jagle.views;



import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager.LayoutParams;

import com.jagle.samples.R;

public class JCenterDialog extends JDialog {

	public JCenterDialog(Context context) {
		super(context, R.style.JCenterDialog);
		
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		setCanceledOnTouchOutside(true);
	}

}
