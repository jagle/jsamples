package com.jagle.views;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

public class TitleTabPagerIndicator extends BaseTabPagerIndicator {

	public TitleTabPagerIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public TitleTabPagerIndicator(Context context) {
		super(context);
	}

	@Override
	public TextView getIndicatorView(int index, ViewPager pager) {
		TextView v = getTabView(index);
		PagerAdapter adapter = pager.getAdapter();
		CharSequence title = adapter.getPageTitle(index);
		if (title == null){
			throw new IllegalStateException("PagerAdapter not setting title");
		}
		v.setText(title);
		return v;
	}
	
	public TextView getTabView(int index){
		TextView v = new TextView(getContext());
		v.setGravity(Gravity.CENTER);
		v.setTextSize(16);
		return v;
	}

}
