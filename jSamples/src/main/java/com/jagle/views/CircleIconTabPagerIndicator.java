package com.jagle.views;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

import com.jagle.samples.R;

public class CircleIconTabPagerIndicator extends BaseTabPagerIndicator {

	public CircleIconTabPagerIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CircleIconTabPagerIndicator(Context context) {
		super(context);
	}

	@Override
	public View getIndicatorView(int index, ViewPager pager) {
		return getTabView(index, R.layout.cirle_indicator_view, 1);
	}

}
