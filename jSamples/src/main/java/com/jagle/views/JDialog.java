package com.jagle.views;



import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.jagle.common.Developer;
import com.jagle.common.JLog;
import com.jagle.samples.R;

public class JDialog extends Dialog {
	private View mContentView;
	private OnButtonClickListener mOnClickLisener;
	
	public JDialog(Context context, boolean cancelable,
			OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
		initDiloag(context);
	}

	public JDialog(Context context, int theme) {
		super(context, theme);
		initDiloag(context);
	}

	public JDialog(Context context) {
		super(context, R.style.JDialog);
		initDiloag(context);
	}
	
	public JDialog(Context context, OnButtonClickListener listener){
		this(context);
		mOnClickLisener = listener;
	}
	
	public void setOnButtonClickListener(OnButtonClickListener listener){
		mOnClickLisener = listener;
	}
	
	public void initButtons(Button... buttons){
		for (Button button : buttons){
			setButtonClickListener(button);
		}
	}
	
	public void initButtons(int... buttonIds){
		if (mContentView == null){
            JLog.e(Developer.Jagle, "content view not create");
			return;
		}
		
		for (int id : buttonIds){
			Button button = (Button)mContentView.findViewById(id);
			setButtonClickListener(button);
		}
	}
	
	private void setButtonClickListener(Button button){
		if (button == null){
			JLog.e(Developer.Jagle, "button can't be null");
			return;
		}
		
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onButtonClick(v.getId());
			}
		});
	}

	private void initDiloag(Context context) {
		
		if (getContentViewResource() == 0 ){
			return ;
		}
		
		LayoutInflater li = LayoutInflater.from(getContext());
		View v = li.inflate(getContentViewResource(), null);
		setContentView(v);
		mContentView = v;
		onCreateContentView(v);
		
		(new Handler(Looper.getMainLooper())).post(new Runnable() {
			
			@Override
			public void run() {
				asyncInit();
			}
		});
		
	}

	protected void asyncInit() {
	}

	protected void onCreateContentView(View v) {
	}

	public int getContentViewResource(){
		return 0;
	}
	
	protected void onButtonClick(int id){
		if (mOnClickLisener != null){
			mOnClickLisener.onDismiss(id);
		}
	}
	
	public interface OnButtonClickListener {
		void onDismiss(int id);
	}

}
