package com.jagle.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceView;

public class JSurfaceView extends SurfaceView {
	

	public JSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public JSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public JSurfaceView(Context context) {
		super(context);
	}
	
}
