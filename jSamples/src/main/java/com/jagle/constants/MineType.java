package com.jagle.constants;

public enum MineType {
	ALL("*", "*"),
	
	AUDIO_ALL("audio", "*"),
	AUDIO_MP4("audio", "mp4", ".mp4"),
	
	IMAGE_ALL("image", "*"),
	IMAGE_PNG("image", "png", ".png"),
	IMAGE_JPGE("image", "jpeg", ".jpg", ".jpeg"),
	IMAGE_WEBP("image", "webp", ".webp"),
	IMAGE_GIF("image", "gif", ".gif"),
	
	VIDEO_ALL("video", "*"),
	VIDEO_AVI("video", "avi", ".avi"),
	VIDEO_MP4("video", "mp4", ".mp4"),
	
	TEXT_ALL("text", "*"),
	TEXT_HTML("text", "html", ".htm", ".html"),
	
	;
	
	public final String rootType;
	public final String subType;
	public final String[] extentions;
	
	private MineType(String rootType, String subType, String... extentions){
		this.rootType = rootType;
		this.subType = subType;
		this.extentions = extentions;
	}
	
	private MineType(String rootType, String subType){
		this(rootType, subType, ".*");
	}
	
	public String getContentType(){
    	return rootType + "/" + subType;
    }
	
	public boolean checkFile(String filePath){
		if (subType.equals("*")){
			throw new IllegalStateException("not support check this type, subtype can't be *");
		}
		
		for (String extention : extentions){
			if (filePath.length() <= extention.length()){
				continue;
			}
			
			String sub = filePath.substring(filePath.length() - extention.length());
			if (sub.equalsIgnoreCase(extention)){
				return true;
			}
		}
		return false;
	}
	
	public String getExtention(){
		return extentions[0].substring(1);
	}

}
