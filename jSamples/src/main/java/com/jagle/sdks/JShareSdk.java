package com.jagle.sdks;

import android.content.Context;

import com.jagle.samples.R;
import com.jagle.samples.TestEnv;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.Platform.ShareParams;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.onekeyshare.ShareContentCustomizeCallback;

public class JShareSdk {

	// 使用快捷分享完成分享（请务必仔细阅读位于SDK解压目录下Docs文件夹中OnekeyShare类的JavaDoc）
		/**ShareSDK集成方法有两种</br>
		 * 1、第一种是引用方式，例如引用onekeyshare项目，onekeyshare项目再引用mainlibs库</br>
		 * 2、第二种是把onekeyshare和mainlibs集成到项目中，本例子就是用第二种方式</br>
		 * 请看“ShareSDK 使用说明文档”，SDK下载目录中 </br>
		 * 或者看网络集成文档 http://wiki.sharesdk.cn/Android_%E5%BF%AB%E9%80%9F%E9%9B%86%E6%88%90%E6%8C%87%E5%8D%97
		 * 3、混淆时，把sample或者本例子的混淆代码copy过去，在proguard-project.txt文件中
		 *
		 *
		 * 平台配置信息有三种方式：
		 * 1、在我们后台配置各个微博平台的key
		 * 2、在代码中配置各个微博平台的key，http://sharesdk.cn/androidDoc/cn/sharesdk/framework/ShareSDK.html
		 * 3、在配置文件中配置，本例子里面的assets/ShareSDK.conf,
		 */
	public static void showOneKeyShare(Context context, boolean silent, String platform){
		final OnekeyShare oks = new OnekeyShare();
		oks.setNotification(R.drawable.icon_app, context.getString(R.string.app_name));
		oks.setAddress("12345678901");
		oks.setTitle(context.getString(R.string.test_title));
		oks.setTitleUrl("http://sharesdk.cn");
		oks.setText(context.getString(R.string.test_content));
		oks.setImagePath(TestEnv.sTestImageLocal);
		oks.setImageUrl(TestEnv.sTestImageWeb);
		oks.setUrl("http://www.sharesdk.cn");
		//oks.setFilePath(MainActivity.TEST_IMAGE);
		oks.setComment(context.getString(R.string.share));
		oks.setSite(context.getString(R.string.app_name));
		oks.setSiteUrl("http://sharesdk.cn");
		oks.setVenueName("ShareSDK");
		oks.setVenueDescription("This is a beautiful place!");
		oks.setLatitude(23.056081f);
		oks.setLongitude(113.385708f);
		oks.setSilent(silent);
		if (platform != null) {
			oks.setPlatform(platform);
		}

		// 取消注释，可以实现对具体的View进行截屏分享
//		oks.setViewToShare(getPage());

		// 去除注释，可令编辑页面显示为Dialog模式
//		oks.setDialogMode();

		// 去除注释，在自动授权时可以禁用SSO方式
//		oks.disableSSOWhenAuthorize();

		// 去除注释，则快捷分享的操作结果将通过OneKeyShareCallback回调
//		oks.setCallback(new OneKeyShareCallback());
		oks.setShareContentCustomizeCallback(new ShareContentCustomizeDemo());

		// 去除注释，演示在九宫格设置自定义的图标
//		Bitmap logo = BitmapFactory.decodeResource(menu.getResources(), R.drawable.ic_launcher);
//		String label = menu.getResources().getString(R.string.app_name);
//		OnClickListener listener = new OnClickListener() {
//			public void onClick(View v) {
//				String text = "Customer Logo -- ShareSDK " + ShareSDK.getSDKVersionName();
//				Toast.makeText(menu.this, text, Toast.LENGTH_SHORT).show();
//				oks.finish();
//			}
//		};
//		oks.setCustomerLogo(logo, label, listener);

		// 去除注释，则快捷分享九宫格中将隐藏新浪微博和腾讯微博
//		oks.addHiddenPlatform(SinaWeibo.NAME);
//		oks.addHiddenPlatform(TencentWeibo.NAME);

		oks.show(context);
	}
	
	public static class ShareContentCustomizeDemo implements ShareContentCustomizeCallback {

		public void onShare(Platform platform, ShareParams paramsToShare) {
			// 改写twitter分享内容中的text字段，否则会超长，
			// 因为twitter会将图片地址当作文本的一部分去计算长度
			if ("Twitter".equals(platform.getName())) {
				String text = platform.getContext().getString(R.string.test_content);
				paramsToShare.setText(text);
			}
		}

	};
	
	public static class OneKeyShareBuilder {
		private int mNotificationIconId = 0;
		private String mNotificationTitle;
		private String mAddress;
		private String mTitle;
		private String mTitleUrl;
		private String mContent;
		private String mImagePath;
		private String mImageUrl;
		private String mUrl;
		private String mFilePath;
		private String mComment;
		private String mSite;
		private String mSiteUrl;
		private String mVenueName;
		private String mVenueDescription;
		private String mPlatform;
		private float mLatitude;
		private float mLongitude;
		private boolean mSilent = false;
		
		public OnekeyShare build(){
			final OnekeyShare oks = new OnekeyShare();
			
			oks.setNotification(mNotificationIconId, mNotificationTitle);
			oks.setAddress(mAddress);
			oks.setTitle(mTitle);
			oks.setTitleUrl(mTitleUrl);
			oks.setText(mContent);
			oks.setImagePath(mImagePath);
			oks.setImageUrl(mImageUrl);
			oks.setUrl(mUrl);
			oks.setFilePath(mFilePath);
			oks.setComment(mComment);
			oks.setSite(mSite);
			oks.setSiteUrl(mSiteUrl);
			oks.setVenueName(mVenueName);
			oks.setVenueDescription(mVenueDescription);
			oks.setLatitude(mLatitude);
			oks.setLongitude(mLongitude);
			oks.setSilent(mSilent);
			if (mPlatform != null) {
				oks.setPlatform(mPlatform);
			}

			return oks;
		}
		
	}
}
