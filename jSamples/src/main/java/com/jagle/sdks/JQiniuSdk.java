package com.jagle.sdks;

import android.content.Context;
import android.net.Uri;
import android.widget.Toast;

import com.jagle.common.Encoder;
import com.qiniu.auth.JSONObjectRet;
import com.qiniu.io.IO;
import com.qiniu.io.PutExtra;
import com.qiniu.resumableio.ResumableIO;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;


public class JQiniuSdk {
	private static String AK = "bxsBHvxEoAgoZh3PlW-5uGZ6b6Or-WtSD2rqIGdY";
	
	private static String SK = "83agtIpXam8dXKWsl_y1UVfiN0njK1a6Q_hWyEDK";
	
	private static String mUploadToken = null;
	
	private static final int TAKEN_DURATION = 1;
	
	public static void uploadToken(){
		mUploadToken = getUploadToken((new PutPolicyBuilder("jagle")).getPutPolicy(), AK, SK);
	}
	
	public static boolean checkToken(){
		return true;
	}
	
	public static void uploadFile(final Context context, String key, File file){
		//if (mUploadToken == null){
			uploadToken();
		//}
		IO.putFile(mUploadToken, key, file, new PutExtra(), new JSONObjectRet() {
			
			@Override
			public void onFailure(Exception ex) {
				Toast.makeText(context, "on failure", Toast.LENGTH_LONG).show();
			}
			
			@Override
			public void onSuccess(JSONObject obj) {
				Toast.makeText(context, "on Success", Toast.LENGTH_LONG).show();
				
			}
		});
	}
	
	public static void uploadFile(final Context context, String key, Uri fileUri){
		//if (mUploadToken == null){
			uploadToken();
		//}
		IO.putFile(context, mUploadToken, key, fileUri, new PutExtra(), new JSONObjectRet() {
			
			@Override
			public void onFailure(Exception ex) {
				Toast.makeText(context, "on failure", Toast.LENGTH_LONG).show();
			}
			
			@Override
			public void onSuccess(JSONObject obj) {
				Toast.makeText(context, "on Success", Toast.LENGTH_LONG).show();
				
			}
		});
	}
	
	public static int resumeUploadFile(final Context context, String key, Uri fileUri, JSONObjectRet ret){
		uploadToken();
		
		return ResumableIO.putFile(context, mUploadToken, key, fileUri, new com.qiniu.resumableio.PutExtra(), ret);
	}
	
	public static String getEncodedSign(String encodedPutPolicy, String SecretKey){
		byte[] sign = Encoder.hmacSha1(encodedPutPolicy, SecretKey);
		
		return Encoder.base64(sign);
	}
	
	public static String getUploadToken(String putPolicy, String AccessKey, String SecretKey){
		String encodedPutPolicy = Encoder.base64(putPolicy);
		String encodedSign = getEncodedSign(encodedPutPolicy, SecretKey);
		return AccessKey + ":" + encodedSign + ":" + encodedPutPolicy;
		
	}
	
	public static class PutPolicyBuilder {
		public static final String K_SCOPE = "scope";
		public static final String K_DEADLINE = "deadline";
		public static final String K_INSERT_ONLY = "insertOnly";
		public static final String K_END_USER = "endUser";
		public static final String K_RETURN_URL = "returnUrl";
		public static final String K_RETURN_BODY = "returnBody";
		public static final String K_CALLBACK_URL = "callbackUrl";
		public static final String K_PERSISTENT_OPS = "persistentOps";
		public static final String K_PERSISTENT_NOTIFY_URL = "persistentNotifyUrl";
		public static final String K_FSIZE_LIMIT = "fsizeLimit";
		public static final String K_DETECT_MIME = "detectMime";
		public static final String K_MIME_LIMIT = "mimeLimit";
		public static final String K_TRANSFORM = "transform";
		public static final String K_FOP_TIMEOUT = "fopTimeout";
		
		private JSONObject mJsonObject;
		
		public PutPolicyBuilder(String bucket, Long deadLine){
			mJsonObject = new JSONObject();
			try {
				mJsonObject.put(K_DEADLINE, deadLine);
				mJsonObject.put(K_SCOPE, bucket);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		public PutPolicyBuilder(String bucket){
			this(bucket, System.currentTimeMillis() / 1000 + TimeUnit.HOURS.toSeconds(TAKEN_DURATION));
		}
		
		public PutPolicyBuilder set(String key, Object value){
			try {
				mJsonObject.put(key, value);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return this;
		}
		
		public String getPutPolicy(){
			return mJsonObject.toString();
		}
	}
}
