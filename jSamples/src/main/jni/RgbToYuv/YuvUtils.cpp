#include "YuvUtils.h"

#include "libyuv.h"

int JConvertToI420(const uint8* src_frame, size_t src_size, uint8* dst_y,
		int dst_stride_y, uint8* dst_u, int dst_stride_u, uint8* dst_v,
		int dst_stride_v, int crop_x, int crop_y, int src_width, int src_height,
		int dst_width, int dst_height, enum libyuv::RotationMode rotation,
		uint32 format) {

	return libyuv::ConvertToI420(src_frame, src_size, dst_y, dst_stride_y,
			dst_u, dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width,
			src_height, dst_width, dst_height, rotation, format);

}

int ConvertNV21ToI420(const uint8* src_frame, uint8* dst_frame, int src_width, int src_height,
		int crop_x, int crop_y, int crop_width,int crop_height, enum libyuv::RotationMode rotation) {

	int dst_width = crop_width;
	int dst_height = crop_height;
	if (rotation == libyuv::kRotate90 || rotation == libyuv::kRotate270){
		dst_width = crop_height;
		dst_height = crop_width;
	}

	uint8* dst_y = dst_frame;
	uint8* dst_u = dst_frame + dst_width * dst_height;
	uint8* dst_v = dst_u + (dst_width / 2) * (dst_height /2);

	int dst_stride_y = dst_width;
	int dst_stride_u = dst_width / 2;
	int dst_stride_v = dst_width / 2;

	return libyuv::ConvertToI420(src_frame, 0, dst_y, dst_stride_y, dst_u,
			dst_stride_u, dst_v, dst_stride_v, crop_x, crop_y, src_width,
			src_height, crop_width, crop_height, rotation, libyuv::FOURCC_NV21);
}

int JI420Scale(const uint8* src_frame, uint8* dst_frame,
		int src_width, int src_height,
		int dst_width, int dst_height,
		enum libyuv::FilterMode filtering){
	const uint8* src_y = src_frame;
	const uint8* src_u = src_frame + src_width * src_height;
	const uint8* src_v = src_u + (src_width / 2) * (src_height /2);

	uint8* dst_y = dst_frame;
	uint8* dst_u = dst_frame + dst_width * dst_height;
	uint8* dst_v = dst_u + (dst_width / 2) * (dst_height /2);

	int src_stride_y = src_width;
	int src_stride_u = src_width / 2;
	int src_stride_v = src_width / 2;

	int dst_stride_y = dst_width;
	int dst_stride_u = dst_width / 2;
	int dst_stride_v = dst_width / 2;

	return libyuv::I420Scale(src_y, src_stride_y,
			src_u, src_stride_u,
			src_v, src_stride_v,
			src_width, src_height,
			dst_y, dst_stride_y,
			dst_u, dst_stride_u,
			dst_v, dst_stride_v,
			dst_width ,dst_height,
			filtering);
}

int I420ToNV21(const uint8* src_frame, uint8* dst_frame, int width ,int height){

	const uint8* src_y = src_frame;
	const uint8* src_u = src_frame + width * height;
	const uint8* src_v = src_u + (width / 2) * (height /2);\

	uint8* dst_y = dst_frame;
	uint8* dst_uv = dst_frame + width * height;

	int src_stride_y = width;
	int src_stride_u = width / 2;
	int src_stride_v = width / 2;

	int dst_stride_y = width;
	int dst_stride_uv = width;

	libyuv::I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v,
			dst_y, dst_stride_y, dst_uv, dst_stride_uv, width, height);

}

int NV21ToI420(const uint8* src_frame, uint8* dst_frame, int width, int height){

	const uint8* src_y = src_frame;
	const uint8* src_uv = src_frame + width * height;

	uint8* dst_y = dst_frame;
	uint8* dst_u = dst_frame + width * height;
	uint8* dst_v = dst_u + (width / 2) * (height /2);

	int src_stride_y = width;
	int src_stride_uv = width;

	int dst_stride_y = width;
	int dst_stride_u = width / 2;
	int dst_stride_v = width / 2;

	libyuv::NV12ToI420(src_y, src_stride_y, src_uv, src_stride_uv,
			dst_y, dst_stride_y, dst_v, dst_stride_v, dst_u, dst_stride_u, width, height);


}
