#include "../JniCommon.h"

extern int register_yuv_util(JNIEnv *env);

using namespace jagle;

namespace jagle{
	extern int register_video_compose(JNIEnv *evn);
}

RegJNIRec gRegJNI[] = {
		REG_JNI(register_video_compose),
		REG_JNI(register_yuv_util),
};

int gRegJniLen = NELEM(gRegJNI);


