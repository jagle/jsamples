/*
 * YuvUtils.h
 *
 *  Created on: 2014年5月21日
 *      Author: liujie
 */

#ifndef YUVUTILS_H_
#define YUVUTILS_H_

#include "libyuv.h"

int JConvertToI420(const uint8* src_frame, size_t src_size, uint8* dst_y,
		int dst_stride_y, uint8* dst_u, int dst_stride_u, uint8* dst_v,
		int dst_stride_v, int crop_x, int crop_y, int src_width, int src_height,
		int dst_width, int dst_height, enum libyuv::RotationMode rotation,
		uint32 format);

int I420Rotate(const uint8* src_y, int src_stride_y,
               const uint8* src_u, int src_stride_u,
               const uint8* src_v, int src_stride_v,
               uint8* dst_y, int dst_stride_y,
               uint8* dst_u, int dst_stride_u,
               uint8* dst_v, int dst_stride_v,
               int src_width, int src_height, enum libyuv::RotationMode mode);

int ConvertNV21ToI420(const uint8* src_frame, uint8* dst_frame,
		int crop_x, int crop_y, int src_width, int src_height,
		int dst_width, int dst_height,
		enum libyuv::RotationMode rotation);

int JI420Scale(const uint8* src_frame, uint8* dst_frame,
		int src_width, int src_height,
		int dst_width, int dst_height,
		enum libyuv::FilterMode filtering);

int I420ToNV21(const uint8* src_frame, uint8* dst_frame, int width ,int height);

int NV21ToI420(const uint8* src_frame, uint8* dst_frame, int width, int height);


#endif /* YUVUTILS_H_ */
