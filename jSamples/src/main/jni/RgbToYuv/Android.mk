LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)

LOCAL_ARM_NEON  := true

LOCAL_LDFLAGS += -ljnigraphics \
-L$(SYSROOT)/usr/lib -llog \

LOCAL_MODULE    := RgbToYuv
LOCAL_SRC_FILES := RgbToYuvDemo.cpp 
LOCAL_SRC_FILES += RgbToYuv.cpp
LOCAL_SRC_FILES += RegisterJni.cpp
LOCAL_SRC_FILES += ../JniCommon.cpp
LOCAL_SRC_FILES += VideoComposeDemo.cpp
LOCAL_SRC_FILES += YuvUtils.cpp
LOCAL_SRC_FILES += YuvUtilsJni.cpp


LOCAL_SHARED_LIBRARIES := yuv

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../libyuv/include

include $(BUILD_SHARED_LIBRARY)
