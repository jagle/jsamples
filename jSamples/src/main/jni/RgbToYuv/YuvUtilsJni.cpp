#include "../JniCommon.h"
#include "YuvUtils.h"

#define CLASSNAME "com/jagle/utils/YuvUtil"

struct YuvUtilField{
	jfieldID memoryBufferId;
	jfieldID memoryBufferSizeId;
};

static YuvUtilField sFields;

static int calcBufferSize(int crop_width, int crop_height, int dst_width, int dst_height, int rotation){
	int tmp_width = crop_width;
	int tmp_height = crop_height;
	if (rotation == 90 || rotation == 270){
		tmp_width = crop_height;
		tmp_height = crop_width;
	}

	int size = crop_width * crop_height * 3 / 2;

	if (dst_width != tmp_width || dst_height != tmp_height){
		size += dst_width * dst_height * 3 / 2;
	}

	return size;
}

static void JNICALL getMemoryBuffer(JNIEnv *env, jobject object,
		int crop_width, int crop_height,
		int dst_width, int dst_height,
		int rotation){
	jint bufferSize = env->GetIntField(object, sFields.memoryBufferSizeId);
	int needSize = calcBufferSize(crop_width, crop_height, dst_width, dst_height, rotation);

	if (bufferSize != needSize){
		uint8* memoryBuffer = (uint8*) env->GetIntField(object, sFields.memoryBufferId);
		if (memoryBuffer != 0){
			delete memoryBuffer;
		}
		uint8* buffer = new uint8[needSize];
		env->SetIntField(object, sFields.memoryBufferId, (jint)buffer);
		env->SetIntField(object, sFields.memoryBufferSizeId, needSize);
	}
}

static void JNICALL  releaseMemoryBuffer(JNIEnv *env, jobject object){
	uint8* memoryBuffer = (uint8*) env->GetIntField(object, sFields.memoryBufferId);
	if (memoryBuffer != 0){
		delete memoryBuffer;
	}

	env->SetIntField(object, sFields.memoryBufferId, 0);
	env->SetIntField(object, sFields.memoryBufferSizeId, 0);
}

static int JNICALL NV21Convert(JNIEnv *env, jobject object,
		jbyteArray src_frame, jbyteArray dst_frame, int width ,int height,
		int crop_x, int crop_y, int crop_width, int crop_height,
		int dst_width, int dst_height, int rotateMode){
	uint8* memoryBuffer = (uint8*) env->GetIntField(object, sFields.memoryBufferId);
	uint8* I420Data = memoryBuffer;
	int I420DataLenght = crop_width * crop_height * 3 / 2;

	libyuv::RotationMode mode ;
	switch (rotateMode){
	case 0:
		mode = libyuv::kRotateNone;
		break;
	case 90:
		mode = libyuv::kRotate90;
		break;
	case 180:
		mode = libyuv::kRotate180;
		break;
	case 270:
		mode = libyuv::kRotate270;
		break;
	}
	jbyte* src_data = env->GetByteArrayElements(src_frame, JNI_FALSE);
	ConvertNV21ToI420((uint8*)src_data, (uint8*)I420Data, width, height, crop_x, crop_y,  crop_width, crop_height, mode);
	env->ReleaseByteArrayElements(src_frame, src_data, JNI_ABORT);

	int tmp_width = crop_width;
	int tmp_height = crop_height;
	if (mode == libyuv::kRotate90 || mode == libyuv::kRotate270){
		tmp_width = crop_height;
		tmp_height = crop_width;
	}

	if (dst_width != tmp_width || dst_height != tmp_height){
		uint8* tmp_data = I420Data;
		I420Data = memoryBuffer + I420DataLenght;
		JI420Scale(tmp_data, I420Data, tmp_width, tmp_height, dst_width, dst_height, libyuv::kFilterBilinear);
	}

	jbyte* dst_data = env->GetByteArrayElements(dst_frame, JNI_FALSE);
	I420ToNV21(I420Data, (uint8*)dst_data, dst_width, dst_height);
	env->ReleaseByteArrayElements(dst_frame, dst_data, JNI_COMMIT);

	return 0;
}

int register_yuv_util(JNIEnv *env){

	JNINativeMethod jniMethods[] = {
		JNI_METHOD("convertNV21", "([B[BIIIIIIIII)I", (void *)NV21Convert),
		JNI_METHOD("getMemoryBuffer", "(IIIII)V", (void *)getMemoryBuffer),
		JNI_METHOD("releaseMemoryBuffer", "()V", (void *)releaseMemoryBuffer),
	};

	jagle::JavaField javaFields[] = {
		JAVA_MEM("memoryBuffer", "I", &sFields.memoryBufferId),
		JAVA_MEM("memoryBufferSize", "I", &sFields.memoryBufferSizeId),
	};

	if(jagle::registerJavaFields(env, CLASSNAME, javaFields, NELEM(javaFields)) < 0){
		return -1;
	}

	return jagle::registerNativeMethods(env, CLASSNAME, jniMethods, NELEM(jniMethods));
}
