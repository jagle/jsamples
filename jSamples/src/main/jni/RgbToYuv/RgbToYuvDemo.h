/*
 * RgbToYuv.h
 *
 *  Created on: 2014-3-11
 *      Author: jagle
 */
#include <../JniCommon.h>

#ifndef RGBTOYUVDEMO_H_
#define RGBTOYUVDEMO_H_

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_com_jagle_samples_RgbToYuvActivity_changeBitmap(JNIEnv *env, jobject object, jobject bitmap, jobject outbitmap);

JNIEXPORT void JNICALL Java_com_jagle_samples_RgbToYuvActivity_changeBitmapDiff(JNIEnv *env, jobject object, jobject bitmap, jobject outbitmap);

#ifdef __cplusplus
}
#endif

#endif /* RGBTOYUVDEMO_H_ */
