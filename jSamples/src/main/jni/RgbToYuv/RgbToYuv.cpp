#include <stdlib.h>
#include "RgbToYuv.h"

#define RGBTOUV(QB, QG, QR) \
    "vmul.s16   q8, " #QB ", q10               \n"  /* B                    */ \
    "vmls.s16   q8, " #QG ", q11               \n"  /* G                    */ \
    "vmls.s16   q8, " #QR ", q12               \n"  /* R                    */ \
    "vadd.u16   q8, q8, q15                    \n"  /* +128 -> unsigned     */ \
    "vmul.s16   q9, " #QR ", q10               \n"  /* R                    */ \
    "vmls.s16   q9, " #QG ", q14               \n"  /* G                    */ \
    "vmls.s16   q9, " #QB ", q13               \n"  /* B                    */ \
    "vadd.u16   q9, q9, q15                    \n"  /* +128 -> unsigned     */ \
    "vqshrn.u16  d0, q8, #8                    \n"  /* 16 bit to 8 bit U    */ \
    "vqshrn.u16  d1, q9, #8                    \n"  /* 16 bit to 8 bit V    */

#define RGB565TOARGB                                                           \
    "vshrn.u16  d6, q0, #5                     \n"  /* G xxGGGGGG           */ \
    "vuzp.u8    d0, d1                         \n"  /* d0 xxxBBBBB RRRRRxxx */ \
    "vshl.u8    d6, d6, #2                     \n"  /* G GGGGGG00 upper 6   */ \
    "vshr.u8    d1, d1, #3                     \n"  /* R 000RRRRR lower 5   */ \
    "vshl.u8    q0, q0, #3                     \n"  /* B,R BBBBB000 upper 5 */ \
    "vshr.u8    q2, q0, #5                     \n"  /* B,R 00000BBB lower 3 */ \
    "vorr.u8    d0, d0, d4                     \n"  /* B                    */ \
    "vshr.u8    d4, d6, #6                     \n"  /* G 000000GG lower 2   */ \
    "vorr.u8    d2, d1, d5                     \n"  /* R                    */ \
    "vorr.u8    d1, d4, d6                     \n"  /* G                    */

// C reference code that mimics the YUV assembly.

#define YG 74 /* static_cast<int8>(1.164 * 64 + 0.5) */

#define UB 127 /* min(63,static_cast<int8>(2.018 * 64)) */
#define UG -25 /* static_cast<int8>(-0.391 * 64 - 0.5) */
#define UR 0

#define VB 0
#define VG -52 /* static_cast<int8>(-0.813 * 64 - 0.5) */
#define VR 102 /* static_cast<int8>(1.596 * 64 + 0.5) */

// Bias
#define BB UB * 128 + VB * 128
#define BG UG * 128 + VG * 128
#define BR UR * 128 + VR * 128

// Read 8 Y, 4 U and 4 V from 422
#define READYUV422                                                             \
    "vld1.8     {d0}, [%0]!                    \n"                             \
    "vld1.32    {d2[0]}, [%1]!                 \n"                             \
    "vld1.32    {d2[1]}, [%2]!                 \n"

#define YUV422TORGB                                                            \
    "veor.u8    d2, d26                        \n"/*subtract 128 from u and v*/\
    "vmull.s8   q8, d2, d24                    \n"/*  u/v B/R component      */\
    "vmull.s8   q9, d2, d25                    \n"/*  u/v G component        */\
    "vmov.u8    d1, #0                         \n"/*  split odd/even y apart */\
    "vtrn.u8    d0, d1                         \n"                             \
    "vsub.s16   q0, q0, q15                    \n"/*  offset y               */\
    "vmul.s16   q0, q0, q14                    \n"                             \
    "vadd.s16   d18, d19                       \n"                             \
    "vqadd.s16  d20, d0, d16                   \n" /* B */                     \
    "vqadd.s16  d21, d1, d16                   \n"                             \
    "vqadd.s16  d22, d0, d17                   \n" /* R */                     \
    "vqadd.s16  d23, d1, d17                   \n"                             \
    "vqadd.s16  d16, d0, d18                   \n" /* G */                     \
    "vqadd.s16  d17, d1, d18                   \n"                             \
    "vqshrun.s16 d0, q10, #6                   \n" /* B */                     \
    "vqshrun.s16 d1, q11, #6                   \n" /* G */                     \
    "vqshrun.s16 d2, q8, #6                    \n" /* R */                     \
    "vmovl.u8   q10, d0                        \n"/*  set up for reinterleave*/\
    "vmovl.u8   q11, d1                        \n"                             \
    "vmovl.u8   q8, d2                         \n"                             \
    "vtrn.u8    d20, d21                       \n"                             \
    "vtrn.u8    d22, d23                       \n"                             \
    "vtrn.u8    d16, d17                       \n"                             \
    "vmov.u8    d21, d16                       \n"

typedef int8 __attribute__((vector_size(16))) vec8;
static vec8 kUVToRB  = { 127, 127, 127, 127, 102, 102, 102, 102,
                         0, 0, 0, 0, 0, 0, 0, 0 };
static vec8 kUVToG = { -25, -25, -25, -25, -52, -52, -52, -52,
                       0, 0, 0, 0, 0, 0, 0, 0 };

void ARGBToYRow_NEON(const uint8_t* src_argb, uint8_t* dst_y, int pix) {
  asm volatile (
    "vmov.u8    d24, #13                       \n"  // B * 0.1016 coefficient
    "vmov.u8    d25, #65                       \n"  // G * 0.5078 coefficient
    "vmov.u8    d26, #33                       \n"  // R * 0.2578 coefficient
    "vmov.u8    d27, #16                       \n"  // Add 16 constant
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld4.8     {d0, d1, d2, d3}, [%0]!        \n"  // load 8 ARGB pixels.
    "subs       %2, %2, #8                     \n"  // 8 processed per loop.
    "vmull.u8   q2, d0, d24                    \n"  // B
    "vmlal.u8   q2, d1, d25                    \n"  // G
    "vmlal.u8   q2, d2, d26                    \n"  // R
    "vqrshrun.s16 d0, q2, #7                   \n"  // 16 bit to 8 bit Y
    "vqadd.u8   d0, d27                        \n"
    "vst1.8     {d0}, [%1]!                    \n"  // store 8 pixels Y.
    "bgt        1b                             \n"
  : "+r"(src_argb),  // %0
    "+r"(dst_y),     // %1
    "+r"(pix)        // %2
  :
  : "cc", "memory", "q0", "q1", "q2", "q12", "q13"
  );
}

void ABGRToYRow_NEON(const uint8_t* src_argb, uint8_t* dst_y, int pix) {
  asm volatile (
    "vmov.u8    d24, #13                       \n"  // B * 0.1016 coefficient
    "vmov.u8    d25, #65                       \n"  // G * 0.5078 coefficient
    "vmov.u8    d26, #33                       \n"  // R * 0.2578 coefficient
    "vmov.u8    d27, #16                       \n"  // Add 16 constant
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld4.8     {d0, d1, d2, d3}, [%0]!        \n"  // load 8 ARGB pixels.
    "subs       %2, %2, #8                     \n"  // 8 processed per loop.
    "vmull.u8   q2, d2, d24                    \n"  // B
    "vmlal.u8   q2, d1, d25                    \n"  // G
    "vmlal.u8   q2, d0, d26                    \n"  // R
    "vqrshrun.s16 d0, q2, #7                   \n"  // 16 bit to 8 bit Y
    "vqadd.u8   d0, d27                        \n"
    "vst1.8     {d0}, [%1]!                    \n"  // store 8 pixels Y.
    "bgt        1b                             \n"
  : "+r"(src_argb),  // %0
    "+r"(dst_y),     // %1
    "+r"(pix)        // %2
  :
  : "cc", "memory", "q0", "q1", "q2", "q12", "q13"
  );
}

void ARGBToUVRow_NEON(const uint8_t* src_argb, int src_stride_argb,
		uint8_t* dst_u, uint8_t* dst_v, int pix) {
  asm volatile (
    "add        %1, %0, %1                     \n"  // src_stride + src_argb
    "vmov.s16   q10, #112 / 4                  \n"  // UB / VR 0.875 coefficient
    "vmov.s16   q11, #74 / 4                   \n"  // UG -0.5781 coefficient
    "vmov.s16   q12, #38 / 4                   \n"  // UR -0.2969 coefficient
    "vmov.s16   q13, #18 / 4                   \n"  // VB -0.1406 coefficient
    "vmov.s16   q14, #94 / 4                   \n"  // VG -0.7344 coefficient
    "vmov.u16   q15, #0x8080                   \n"  // 128.5
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld4.8     {d0, d2, d4, d6}, [%0]!        \n"  // load 8 ARGB pixels.
    "vld4.8     {d1, d3, d5, d7}, [%0]!        \n"  // load next 8 ARGB pixels.
    "vpaddl.u8  q0, q0                         \n"  // B 16 bytes -> 8 shorts.
    "vpaddl.u8  q1, q1                         \n"  // G 16 bytes -> 8 shorts.
    "vpaddl.u8  q2, q2                        \n"  // R 16 bytes -> 8 shorts.
    "vld4.8     {d8, d10, d12, d14}, [%1]!     \n"  // load 8 more ARGB pixels.
    "vld4.8     {d9, d11, d13, d15}, [%1]!     \n"  // load last 8 ARGB pixels.
    "vpadal.u8  q0, q4                         \n"  // B 16 bytes -> 8 shorts.
    "vpadal.u8  q1, q5                         \n"  // G 16 bytes -> 8 shorts.
    "vpadal.u8  q2, q6                         \n"  // R 16 bytes -> 8 shorts.
    "subs       %4, %4, #16                    \n"  // 32 processed per loop.
    RGBTOUV(q0, q1, q2)
    "vst1.8     {d0}, [%2]!                    \n"  // store 8 pixels U.
    "vst1.8     {d1}, [%3]!                    \n"  // store 8 pixels V.
    "bgt        1b                             \n"
  : "+r"(src_argb),  // %0
    "+r"(src_stride_argb),  // %1
    "+r"(dst_u),     // %2
    "+r"(dst_v),     // %3
    "+r"(pix)        // %4
  :
  : "cc", "memory", "q0", "q1", "q2", "q3", "q4", "q5", "q6", "q7",
    "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15"
  );
}

void ABGRToUVRow_NEON(const uint8_t* src_argb, int src_stride_argb,
		uint8_t* dst_u, uint8_t* dst_v, int pix) {
  asm volatile (
    "add        %1, %0, %1                     \n"  // src_stride + src_argb
    "vmov.s16   q10, #112 / 4                  \n"  // UB / VR 0.875 coefficient
    "vmov.s16   q11, #74 / 4                   \n"  // UG -0.5781 coefficient
    "vmov.s16   q12, #38 / 4                   \n"  // UR -0.2969 coefficient
    "vmov.s16   q13, #18 / 4                   \n"  // VB -0.1406 coefficient
    "vmov.s16   q14, #94 / 4                   \n"  // VG -0.7344 coefficient
    "vmov.u16   q15, #0x8080                   \n"  // 128.5
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld4.8     {d0, d2, d4, d6}, [%0]!        \n"  // load 8 ARGB pixels.
    "vld4.8     {d1, d3, d5, d7}, [%0]!        \n"  // load next 8 ARGB pixels.
    "vpaddl.u8  q0, q0                         \n"  // R 16 bytes -> 8 shorts.
    "vpaddl.u8  q1, q1                         \n"  // G 16 bytes -> 8 shorts.
    "vpaddl.u8  q2, q2                        \n"  // B 16 bytes -> 8 shorts.
    "vld4.8     {d8, d10, d12, d14}, [%1]!     \n"  // load 8 more ARGB pixels.
    "vld4.8     {d9, d11, d13, d15}, [%1]!     \n"  // load last 8 ARGB pixels.
    "vpadal.u8  q0, q4                         \n"  // R 16 bytes -> 8 shorts.
    "vpadal.u8  q1, q5                         \n"  // G 16 bytes -> 8 shorts.
    "vpadal.u8  q2, q6                         \n"  // B 16 bytes -> 8 shorts.
    "subs       %4, %4, #16                    \n"  // 32 processed per loop.
    RGBTOUV(q2, q1, q0)
    "vst1.8     {d0}, [%2]!                    \n"  // store 8 pixels U.
    "vst1.8     {d1}, [%3]!                    \n"  // store 8 pixels V.
    "bgt        1b                             \n"
  : "+r"(src_argb),  // %0
    "+r"(src_stride_argb),  // %1
    "+r"(dst_u),     // %2
    "+r"(dst_v),     // %3
    "+r"(pix)        // %4
  :
  : "cc", "memory", "q0", "q1", "q2", "q3", "q4", "q5", "q6", "q7",
    "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15"
  );
}

void CopyRow_NEON(const uint8* src, uint8* dst, int count) {
  asm volatile (
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld1.8     {d0, d1, d2, d3}, [%0]!        \n"  // load 32
    "subs       %2, %2, #32                    \n"  // 32 processed per loop
    "vst1.8     {d0, d1, d2, d3}, [%1]!        \n"  // store 32
    "bgt        1b                             \n"
  : "+r"(src),   // %0
    "+r"(dst),   // %1
    "+r"(count)  // %2  // Output registers
  :                     // Input registers
  : "cc", "memory", "q0", "q1"  // Clobber List
  );
}

void RGB565ToYRow_NEON(const uint8_t* src_rgb565, uint8_t* dst_y, int pix) {
  asm volatile (
    "vmov.u8    d24, #13                       \n"  // B * 0.1016 coefficient
    "vmov.u8    d25, #65                       \n"  // G * 0.5078 coefficient
    "vmov.u8    d26, #33                       \n"  // R * 0.2578 coefficient
    "vmov.u8    d27, #16                       \n"  // Add 16 constant
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld1.8     {q0}, [%0]!                    \n"  // load 8 RGB565 pixels.
    "subs       %2, %2, #8                     \n"  // 8 processed per loop.
    RGB565TOARGB
    "vmull.u8   q2, d0, d24                    \n"  // B
    "vmlal.u8   q2, d1, d25                    \n"  // G
    "vmlal.u8   q2, d2, d26                    \n"  // R
    "vqrshrun.s16 d0, q2, #7                   \n"  // 16 bit to 8 bit Y
    "vqadd.u8   d0, d27                        \n"
    "vst1.8     {d0}, [%1]!                    \n"  // store 8 pixels Y.
    "bgt        1b                             \n"
  : "+r"(src_rgb565),  // %0
    "+r"(dst_y),       // %1
    "+r"(pix)          // %2
  :
  : "cc", "memory", "q0", "q1", "q2", "q3", "q12", "q13"
  );
}

// 16x2 pixels -> 8x1.  pix is number of argb pixels. e.g. 16.
void RGB565ToUVRow_NEON(const uint8_t* src_rgb565, int src_stride_rgb565,
                        uint8_t* dst_u, uint8_t* dst_v, int pix) {
  asm volatile (
    "add        %1, %0, %1                     \n"  // src_stride + src_argb
    "vmov.s16   q10, #112 / 4                  \n"  // UB / VR 0.875 coefficient
    "vmov.s16   q11, #74 / 4                   \n"  // UG -0.5781 coefficient
    "vmov.s16   q12, #38 / 4                   \n"  // UR -0.2969 coefficient
    "vmov.s16   q13, #18 / 4                   \n"  // VB -0.1406 coefficient
    "vmov.s16   q14, #94 / 4                   \n"  // VG -0.7344 coefficient
    "vmov.u16   q15, #0x8080                   \n"  // 128.5
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld1.8     {q0}, [%0]!                    \n"  // load 8 RGB565 pixels.
    RGB565TOARGB
    "vpaddl.u8  d8, d0                         \n"  // B 8 bytes -> 4 shorts.
    "vpaddl.u8  d10, d1                        \n"  // G 8 bytes -> 4 shorts.
    "vpaddl.u8  d12, d2                        \n"  // R 8 bytes -> 4 shorts.
    "vld1.8     {q0}, [%0]!                    \n"  // next 8 RGB565 pixels.
    RGB565TOARGB
    "vpaddl.u8  d9, d0                         \n"  // B 8 bytes -> 4 shorts.
    "vpaddl.u8  d11, d1                        \n"  // G 8 bytes -> 4 shorts.
    "vpaddl.u8  d13, d2                        \n"  // R 8 bytes -> 4 shorts.

    "vld1.8     {q0}, [%1]!                    \n"  // load 8 RGB565 pixels.
    RGB565TOARGB
    "vpadal.u8  d8, d0                         \n"  // B 8 bytes -> 4 shorts.
    "vpadal.u8  d10, d1                        \n"  // G 8 bytes -> 4 shorts.
    "vpadal.u8  d12, d2                        \n"  // R 8 bytes -> 4 shorts.
    "vld1.8     {q0}, [%1]!                    \n"  // next 8 RGB565 pixels.
    RGB565TOARGB
    "vpadal.u8  d9, d0                         \n"  // B 8 bytes -> 4 shorts.
    "vpadal.u8  d11, d1                        \n"  // G 8 bytes -> 4 shorts.
    "vpadal.u8  d13, d2                        \n"  // R 8 bytes -> 4 shorts.

    "subs       %4, %4, #16                    \n"  // 16 processed per loop.
    "vmul.s16   q8, q4, q10                    \n"  // B
    "vmls.s16   q8, q5, q11                    \n"  // G
    "vmls.s16   q8, q6, q12                    \n"  // R
    "vadd.u16   q8, q8, q15                    \n"  // +128 -> unsigned
    "vmul.s16   q9, q6, q10                    \n"  // R
    "vmls.s16   q9, q5, q14                    \n"  // G
    "vmls.s16   q9, q4, q13                    \n"  // B
    "vadd.u16   q9, q9, q15                    \n"  // +128 -> unsigned
    "vqshrn.u16  d0, q8, #8                    \n"  // 16 bit to 8 bit U
    "vqshrn.u16  d1, q9, #8                    \n"  // 16 bit to 8 bit V
    "vst1.8     {d0}, [%2]!                    \n"  // store 8 pixels U.
    "vst1.8     {d1}, [%3]!                    \n"  // store 8 pixels V.
    "bgt        1b                             \n"
  : "+r"(src_rgb565),  // %0
    "+r"(src_stride_rgb565),  // %1
    "+r"(dst_u),     // %2
    "+r"(dst_v),     // %3
    "+r"(pix)        // %4
  :
  : "cc", "memory", "q0", "q1", "q2", "q3", "q4", "q5", "q6", "q7",
    "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15"
  );
}


void I422ToARGBRow_NEON(const uint8* src_y,
                        const uint8* src_u,
                        const uint8* src_v,
                        uint8* dst_argb,
                        int width) {
  asm volatile (
    "vld1.8     {d24}, [%5]                    \n"
    "vld1.8     {d25}, [%6]                    \n"
    "vmov.u8    d26, #128                      \n"
    "vmov.u16   q14, #74                       \n"
    "vmov.u16   q15, #16                       \n"
    ".p2align  2                               \n"
  "1:                                          \n"
    READYUV422
    YUV422TORGB
    "subs       %4, %4, #8                     \n"
    "vmov.u8    d23, #255                      \n"
    "vst4.8     {d20, d21, d22, d23}, [%3]!    \n"
    "bgt        1b                             \n"
    : "+r"(src_y),     // %0
      "+r"(src_u),     // %1
      "+r"(src_v),     // %2
      "+r"(dst_argb),  // %3
      "+r"(width)      // %4
    : "r"(&kUVToRB),   // %5
      "r"(&kUVToG)     // %6
    : "cc", "memory", "q0", "q1", "q2", "q3",
      "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15"
  );
}

void I422ToABGRRow_NEON(const uint8* src_y,
                        const uint8* src_u,
                        const uint8* src_v,
                        uint8* dst_abgr,
                        int width) {
  asm volatile (
    "vld1.8     {d24}, [%5]                    \n"
    "vld1.8     {d25}, [%6]                    \n"
    "vmov.u8    d26, #128                      \n"
    "vmov.u16   q14, #74                       \n"
    "vmov.u16   q15, #16                       \n"
    ".p2align  2                               \n"
  "1:                                          \n"
    READYUV422
    YUV422TORGB
    "subs       %4, %4, #8                     \n"
    "vswp.u8    d20, d22                       \n"
    "vmov.u8    d23, #255                      \n"
    "vst4.8     {d20, d21, d22, d23}, [%3]!    \n"
    "bgt        1b                             \n"
    : "+r"(src_y),     // %0
      "+r"(src_u),     // %1
      "+r"(src_v),     // %2
      "+r"(dst_abgr),  // %3
      "+r"(width)      // %4
    : "r"(&kUVToRB),   // %5
      "r"(&kUVToG)     // %6
    : "cc", "memory", "q0", "q1", "q2", "q3",
      "q8", "q9", "q10", "q11", "q12", "q13", "q14", "q15"
  );
}

void SplitUVRow_NEON(const uint8* src_uv, uint8* dst_u, uint8* dst_v,
                     int width) {
  asm volatile (
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld2.8     {q0, q1}, [%0]!                \n"  // load 16 pairs of UV
    "subs       %3, %3, #16                    \n"  // 16 processed per loop
    "vst1.8     {q0}, [%1]!                    \n"  // store U
    "vst1.8     {q1}, [%2]!                    \n"  // store V
    "bgt        1b                             \n"
    : "+r"(src_uv),  // %0
      "+r"(dst_u),   // %1
      "+r"(dst_v),   // %2
      "+r"(width)    // %3  // Output registers
    :                       // Input registers
    : "cc", "memory", "q0", "q1"  // Clobber List
  );
}


void MergeUVRow_NEON(const uint8* src_u, const uint8* src_v, uint8* dst_uv,
                     int width) {
  asm volatile (
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld1.8     {q0}, [%0]!                    \n"  // load U
    "vld1.8     {q1}, [%1]!                    \n"  // load V
    "subs       %3, %3, #16                    \n"  // 16 processed per loop
    "vst2.u8    {q0, q1}, [%2]!                \n"  // store 16 pairs of UV
    "bgt        1b                             \n"
    :
      "+r"(src_u),   // %0
      "+r"(src_v),   // %1
      "+r"(dst_uv),  // %2
      "+r"(width)    // %3  // Output registers
    :                       // Input registers
    : "cc", "memory", "q0", "q1"  // Clobber List
  );
}

void NV21ToNV12Row_NEON(const uint8* src, uint8* dst, int count) {
  asm volatile (
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld1.8     {d0, d1}, [%0]!        \n"  // load 32
    "subs       %2, %2, #16                   \n"  // 32 processed per loop
	"vrev16.8   q0, q0                         \n"
    "vst1.8     {d0, d1}, [%1]!        \n"  // store 32
    "bgt        1b                             \n"
  : "+r"(src),   // %0
    "+r"(dst),   // %1
    "+r"(count)  // %2  // Output registers
  :                     // Input registers
  : "cc", "memory", "q0"  // Clobber List
  );
}
int ARGBToI420(const uint8_t* src_rgba, int src_stride_rgba, uint8_t* dst_y,
		int dst_stride_y, uint8_t* dst_u, int dst_stride_u, uint8_t* dst_v,
		int dst_stride_v, int width, int height) {
	if (!src_rgba || !dst_y || !dst_u || !dst_v || width <= 0 || height == 0) {
		return -1;
	}

	for (int y = 0; y < height - 1; y += 2) {
		ARGBToUVRow_NEON(src_rgba, src_stride_rgba, dst_u, dst_v, width);
		ARGBToYRow_NEON(src_rgba, dst_y, width);
		ARGBToYRow_NEON(src_rgba + src_stride_rgba, dst_y + dst_stride_y, width);
		src_rgba += src_stride_rgba * 2;
		dst_y += dst_stride_y * 2;
		dst_u += dst_stride_u;
		dst_v += dst_stride_v;
	}

	return 0;
}

int ABGRToNV21(const uint8* src_argb, int src_stride_argb,
               uint8* dst_y, int dst_stride_y,
               uint8* dst_uv, int dst_stride_uv,
               int width, int height) {

	int halfwidth = width >> 1;
	uint8_t row_u[halfwidth];
	uint8_t row_v[halfwidth];
	for (int y = 0; y < height - 1; y += 2) {
	    ABGRToUVRow_NEON(src_argb, src_stride_argb, row_u, row_v, width);
	    MergeUVRow_NEON(row_v, row_u, dst_uv, halfwidth);
	    ABGRToYRow_NEON(src_argb, dst_y, width);
	    ABGRToYRow_NEON(src_argb + src_stride_argb, dst_y + dst_stride_y, width);
	    src_argb += src_stride_argb * 2;
	    dst_y += dst_stride_y * 2;
	    dst_uv += dst_stride_uv;
	}
	return 0;
}

int ARGBToNV21(const uint8* src_argb, int src_stride_argb,
               uint8* dst_y, int dst_stride_y,
               uint8* dst_uv, int dst_stride_uv,
               int width, int height) {

	int halfwidth = width >> 1;
	uint8_t row_u[halfwidth];
	uint8_t row_v[halfwidth];
	for (int y = 0; y < height - 1; y += 2) {
	    ARGBToUVRow_NEON(src_argb, src_stride_argb, row_u, row_v, width);
	    MergeUVRow_NEON(row_v, row_u, dst_uv, halfwidth);
	    ARGBToYRow_NEON(src_argb, dst_y, width);
	    ARGBToYRow_NEON(src_argb + src_stride_argb, dst_y + dst_stride_y, width);
	    src_argb += src_stride_argb * 2;
	    dst_y += dst_stride_y * 2;
	    dst_uv += dst_stride_uv;
	}
	return 0;
}

int ABGRToI420(const uint8_t* src_rgba, int src_stride_rgba, uint8_t* dst_y,
		int dst_stride_y, uint8_t* dst_u, int dst_stride_u, uint8_t* dst_v,
		int dst_stride_v, int width, int height) {
	if (!src_rgba || !dst_y || !dst_u || !dst_v || width <= 0 || height == 0) {
		return -1;
	}

	for (int y = 0; y < height - 1; y += 2) {
		ABGRToUVRow_NEON(src_rgba, src_stride_rgba, dst_u, dst_v, width);
		ABGRToYRow_NEON(src_rgba, dst_y, width);
		ABGRToYRow_NEON(src_rgba + src_stride_rgba, dst_y + dst_stride_y, width);
		src_rgba += src_stride_rgba * 2;
		dst_y += dst_stride_y * 2;
		dst_u += dst_stride_u;
		dst_v += dst_stride_v;
	}

	return 0;
}

int RGB565ToI420(const uint8_t* src_rgba, int src_stride_rgba, uint8_t* dst_y,
		int dst_stride_y, uint8_t* dst_u, int dst_stride_u, uint8_t* dst_v,
		int dst_stride_v, int width, int height) {
	if (!src_rgba || !dst_y || !dst_u || !dst_v || width <= 0 || height == 0) {
		return -1;
	}

	for (int y = 0; y < height - 1; y += 2) {
		RGB565ToUVRow_NEON(src_rgba, src_stride_rgba, dst_u, dst_v, width);
		RGB565ToYRow_NEON(src_rgba, dst_y, width);
		RGB565ToYRow_NEON(src_rgba + src_stride_rgba, dst_y + dst_stride_y, width);
		src_rgba += src_stride_rgba * 2;
		dst_y += dst_stride_y * 2;
		dst_u += dst_stride_u;
		dst_v += dst_stride_v;
	}

	return 0;
}

void RGB565ToYUV420(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);
	RGB565ToI420(rgb, srcWidth * 2, yplane, srcWidth, uplane, (srcWidth + 1) >> 1, vplane, (srcWidth + 1) >> 1, srcWidth, srcHeight);
}

void ARGB8888ToYUV420(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);
	ARGBToI420(rgb, srcWidth * 4, yplane, srcWidth, uplane, (srcWidth + 1) >> 1, vplane, (srcWidth + 1) >> 1, srcWidth, srcHeight);
}

void ABGR8888ToNV21(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;
	ABGRToNV21(rgb, srcWidth * 4, yplane, srcWidth, uvplane, srcWidth, srcWidth, srcHeight);
}

void ARGB8888ToNV21(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;
	ARGBToNV21(rgb, srcWidth * 4, yplane, srcWidth, uvplane, srcWidth, srcWidth, srcHeight);
}

void RGB565ToNV21(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;
	ARGBToNV21(rgb, srcWidth * 4, yplane, srcWidth, uvplane, srcWidth, srcWidth, srcHeight);
}

void ABGR8888ToYUV420(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);
	ABGRToI420(rgb, srcWidth * 4, yplane, srcWidth, uplane, (srcWidth + 1) >> 1, vplane, (srcWidth + 1) >> 1, srcWidth, srcHeight);
}

void ABGR8888ToYUV_NEON(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);
	ABGRToI420(rgb, srcWidth * 4, yplane, srcWidth, uplane, (srcWidth + 1) >> 1, vplane, (srcWidth + 1) >> 1, srcWidth, srcHeight);
}

static int32_t clamp0(int32_t v) {
  return ((-(v) >> 31) & (v));
}

static int32_t clamp255(int32_t v) {
  return (((255 - (v)) >> 31) | (v)) & 255;
}

static __uint32_t Clamp(int32_t val) {
  int v = clamp0(val);
  return static_cast<__uint32_t>(clamp255(v));
}

static void YuvPixel(uint8_t y, uint8_t u, uint8_t v,
                              uint8_t* b, uint8_t* g, uint8_t* r) {
  int32_t y1 = (static_cast<int32_t>(y) - 16) * YG;
  *b = Clamp(static_cast<int32_t>((u * UB + v * VB) - (BB) + y1) >> 6);
  *g = Clamp(static_cast<int32_t>((u * UG + v * VG) - (BG) + y1) >> 6);
  *r = Clamp(static_cast<int32_t>((u * UR + v * VR) - (BR) + y1) >> 6);
}

void I422ToARGBRow_C(const uint8_t* src_y,
                     const uint8_t* src_u,
                     const uint8_t* src_v,
                     uint8_t* rgb_buf,
                     int width) {
	for (int x = 0; x < width - 1; x += 2) {
		YuvPixel(src_y[0], src_u[0], src_v[0], rgb_buf + 0, rgb_buf + 1,
				rgb_buf + 2);
		rgb_buf[3] = 255;
		YuvPixel(src_y[1], src_u[0], src_v[0], rgb_buf + 4, rgb_buf + 5,
				rgb_buf + 6);
		rgb_buf[7] = 255;
		src_y += 2;
		src_u += 1;
		src_v += 1;
		rgb_buf += 8; // Advance 2 pixels.
	}
	if (width & 1) {
		YuvPixel(src_y[0], src_u[0], src_v[0], rgb_buf + 0, rgb_buf + 1,
				rgb_buf + 2);
		rgb_buf[3] = 255;
	}
}

void I422ToABGRRow_C(const uint8_t* src_y,
                     const uint8_t* src_u,
                     const uint8_t* src_v,
                     uint8_t* rgb_buf,
                     int width) {
	for (int x = 0; x < width - 1; x += 2) {

		YuvPixel(src_y[0], src_u[0], src_v[0], rgb_buf + 2, rgb_buf + 1,
				rgb_buf + 0);
		rgb_buf[3] = 255;
		YuvPixel(src_y[1], src_u[0], src_v[0], rgb_buf + 6, rgb_buf + 5,
				rgb_buf + 4);
		rgb_buf[7] = 255;
		src_y += 2;
		src_u += 1;
		src_v += 1;
		rgb_buf += 8; // Advance 2 pixels.
	}
	if (width & 1) {
		YuvPixel(src_y[0], src_u[0], src_v[0], rgb_buf + 2, rgb_buf + 1,
				rgb_buf + 0);
		rgb_buf[3] = 255;
	}
}

void I422ToRGB565Row_C(const uint8* src_y,
                       const uint8* src_u,
                       const uint8* src_v,
                       uint8* dst_rgb565,
                       int width) {
	uint8 b0;
	uint8 g0;
	uint8 r0;
	uint8 b1;
	uint8 g1;
	uint8 r1;
	for (int x = 0; x < width - 1; x += 2) {
		YuvPixel(src_y[0], src_u[0], src_v[0], &b0, &g0, &r0);
		YuvPixel(src_y[1], src_u[0], src_v[0], &b1, &g1, &r1);
		b0 = b0 >> 3;
		g0 = g0 >> 2;
		r0 = r0 >> 3;
		b1 = b1 >> 3;
		g1 = g1 >> 2;
		r1 = r1 >> 3;
		*reinterpret_cast<uint32*>(dst_rgb565) = b0 | (g0 << 5) | (r0 << 11)
				| (b1 << 16) | (g1 << 21) | (r1 << 27);
		src_y += 2;
		src_u += 1;
		src_v += 1;
		dst_rgb565 += 4;  // Advance 2 pixels.
	}
	if (width & 1) {
		YuvPixel(src_y[0], src_u[0], src_v[0], &b0, &g0, &r0);
		b0 = b0 >> 3;
		g0 = g0 >> 2;
		r0 = r0 >> 3;
		*reinterpret_cast<uint16*>(dst_rgb565) = b0 | (g0 << 5) | (r0 << 11);
	}
}

void YUVToRGB565(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* src_y = yuv;
	uint8_t* src_u = yuv + planeSize;
	uint8_t* src_v = yuv + planeSize + (planeSize >> 2);

	for (int y = 0; y < srcHeight; ++y) {
		I422ToRGB565Row_C(src_y, src_u, src_v, rgb, srcWidth);
		rgb += srcWidth << 1;
	    src_y += srcWidth;
	    if (y & 1) {
	      src_u += (srcWidth + 1) >> 1;
	      src_v += (srcWidth + 1) >> 1;
	    }
	}
}

void YUVToARGB8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* src_y = yuv;
	uint8_t* src_u = yuv + planeSize;
	uint8_t* src_v = yuv + planeSize + (planeSize >> 2);

	for (int y = 0; y < srcHeight; ++y) {
		I422ToARGBRow_C(src_y, src_u, src_v, rgb, srcWidth);
		rgb += srcWidth << 2;
	    src_y += srcWidth;
	    if (y & 1) {
	      src_u += (srcWidth + 1) >> 1;
	      src_v += (srcWidth + 1) >> 1;
	    }
	}
}

void CopyPlane(const uint8* src_y, int src_stride_y,
               uint8* dst_y, int dst_stride_y,
               int width, int height) {
	int w1 = width & ~0x01f;
	int w2 = width & 0x01f;
	for (int y = 0; y < height; ++y) {
		CopyRow_NEON(src_y, dst_y, w1);
		memcpy(dst_y + w1, src_y + w1, w2);
	    src_y += src_stride_y;
	    dst_y += dst_stride_y;
	}
}

void NV21ToNV12(const uint8* src_uv, int src_stride_uv,
               uint8* dst_uv, int dst_stride_uv,
               int width, int height) {
	int w1 = width & ~0x0f;
	int w2 = width & 0x0f;
	for (int y = 0; y < height; ++y) {
		NV21ToNV12Row_NEON(src_uv, dst_uv, w1);
		if (w2 > 0){
			uint8 data[16];
			NV21ToNV12Row_NEON(src_uv + w1, data, 16);
			memcpy(dst_uv + w1, data, w2);
		}
	    src_uv += src_stride_uv;
	    dst_uv += dst_stride_uv;
	}
}

void NV21ToI420(const uint8* src_uv, int src_stride_uv,
               uint8* dst_u, int dst_stride_u,
               uint8* dst_v, int dst_stride_v,
               int width, int height) {
	int w1 = width & ~0x0f;
	int w2 = width & 0x0f;
	for (int y = 0; y < height; ++y) {
		SplitUVRow_NEON(src_uv, dst_v, dst_u, w1);
		if (w2 > 0){
			uint8 data[32];
			SplitUVRow_NEON(src_uv + (w1 << 1), data, data + 16, 16);
			memcpy(dst_v + w1, data, w2);
			memcpy(dst_u + w1, data + 16, w2);
		}
	    src_uv += src_stride_uv;
	    dst_u += dst_stride_u;
	    dst_v += dst_stride_v;
	}
}

int I420ToNV12(const uint8* src_y, int src_stride_y,
               const uint8* src_u, int src_stride_u,
               const uint8* src_v, int src_stride_v,
               uint8* dst_y, int dst_stride_y,
               uint8* dst_uv, int dst_stride_uv,
               int width, int height) {

	CopyPlane(src_y, src_stride_y, dst_y, dst_stride_y, width * height, 1);
	MergeUVRow_NEON(src_u, src_v, dst_uv, ((width + 1) >> 1) * ((height + 1) >> 1));
	return 0;
}
int NV12ToI420(const uint8* src_y, int src_stride_y,
		const uint8* src_uv, int src_stride_uv,
		uint8* dst_y, int dst_stride_y,
		uint8* dst_u, int dst_stride_u,
		uint8* dst_v, int dst_stride_v,
		int width, int height) {

	CopyPlane(src_y, src_stride_y, dst_y, dst_stride_y, width * height, 1);
	SplitUVRow_NEON(src_uv, dst_u, dst_v, ((width + 1) >> 1) * ((height + 1) >> 1));
	return 0;
}

void ARGB8888ToNV12(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	static uint32_t size = srcWidth * srcHeight;
	static uint8_t* I420 = (uint8_t *)malloc(size * sizeof(uint8_t) * 2);

	if (size < srcWidth * srcHeight){
		size = srcWidth * srcHeight;
		I420 = (uint8_t *)realloc(I420, size * sizeof(uint8_t) * 2);
	}

	uint8* src_y = I420;
	uint8* src_u = I420 + planeSize;
	uint8* src_v = I420 + planeSize + (planeSize >> 2);

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	ARGBToI420(rgb, srcWidth * 4, src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, srcWidth, srcHeight);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;
	I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, yplane, srcWidth, uvplane, srcWidth*2, srcWidth, srcHeight);
}

void ABGR8888ToNV12(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	static uint32_t size = srcWidth * srcHeight;
	static uint8_t* I420 = (uint8_t *)malloc(size * sizeof(uint8_t) * 2);

	if (size < srcWidth * srcHeight){
		size = srcWidth * srcHeight;
		I420 = (uint8_t *)realloc(I420, size * sizeof(uint8_t) * 2);
	}

	uint8* src_y = I420;
	uint8* src_u = I420 + planeSize;
	uint8* src_v = I420 + planeSize + (planeSize >> 2);

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	ABGRToI420(rgb, srcWidth * 4, src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, srcWidth, srcHeight);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;
	I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, yplane, srcWidth, uvplane, srcWidth*2, srcWidth, srcHeight);
}

void NV12ToARGB8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	static uint32_t size = srcWidth * srcHeight;
	static uint8_t* I420 = (uint8_t *) malloc(size * sizeof(uint8_t)  * 2);

	if (size < srcWidth * srcHeight) {
		size = srcWidth * srcHeight;
		I420 = (uint8_t *) realloc(I420, size * sizeof(uint8_t) * 2);
	}

	uint8* src_y = I420;
	uint8* src_u = I420 + planeSize;
	uint8* src_v = I420 + planeSize + (planeSize >> 2);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;

	NV12ToI420(yplane, srcWidth, uvplane, srcWidth*2, src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, srcWidth, srcHeight);

	for (int y = 0; y < srcHeight; ++y) {
		I422ToARGBRow_NEON(src_y, src_u, src_v, rgb, srcWidth);
		rgb += srcWidth << 2;
	    src_y += srcWidth;
	    if (y & 1) {
	      src_u += (srcWidth + 1) >> 1;
	      src_v += (srcWidth + 1) >> 1;
	    }
	}
}

void YUV420ToARGB8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	uint8* src_y = yuv;
	uint8* src_u = yuv + planeSize;
	uint8* src_v = yuv + planeSize + (planeSize >> 2);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;

	for (int y = 0; y < srcHeight; ++y) {
		I422ToARGBRow_NEON(src_y, src_u, src_v, rgb, srcWidth);
		rgb += srcWidth << 2;
	    src_y += srcWidth;
	    if (y & 1) {
	      src_u += (srcWidth + 1) >> 1;
	      src_v += (srcWidth + 1) >> 1;
	    }
	}
}

void NV12ToABGR8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	static uint32_t size = srcWidth * srcHeight;
	static uint8_t* I420 = (uint8_t *) malloc(size * sizeof(uint8_t)  * 2);

	if (size < srcWidth * srcHeight) {
		size = srcWidth * srcHeight;
		I420 = (uint8_t *) realloc(I420, size * sizeof(uint8_t) * 2);
	}

	uint8* src_y = I420;
	uint8* src_u = I420 + planeSize;
	uint8* src_v = I420 + planeSize + (planeSize >> 2);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;

	NV12ToI420(yplane, srcWidth, uvplane, srcWidth*2, src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, srcWidth, srcHeight);

	for (int y = 0; y < srcHeight; ++y) {
		I422ToABGRRow_NEON(src_y, src_u, src_v, rgb, srcWidth);
		rgb += srcWidth << 2;
	    src_y += srcWidth;
	    if (y & 1) {
	      src_u += (srcWidth + 1) >> 1;
	      src_v += (srcWidth + 1) >> 1;
	    }
	}
}

void RGB565ToNV12(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	static uint32_t size = srcWidth * srcHeight;
	static uint8_t* I420 = (uint8_t *)malloc(size * sizeof(uint8_t) * 2);

	if (size < srcWidth * srcHeight){
		size = srcWidth * srcHeight;
		I420 = (uint8_t *)realloc(I420, size * sizeof(uint8_t) * 2);
	}

	uint8* src_y = I420;
	uint8* src_u = I420 + planeSize;
	uint8* src_v = I420 + planeSize + (planeSize >> 2);

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	RGB565ToI420(rgb, srcWidth * 2, src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, srcWidth, srcHeight);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;
	I420ToNV12(src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, yplane, srcWidth, uvplane, srcWidth*2, srcWidth, srcHeight);
}

void NV12ToRGB565(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight){
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	int src_stride_y = srcWidth;
	int src_stride_u = (srcWidth + 1) >> 1;
	int src_stride_v = (srcWidth + 1) >> 1;

	static uint32_t size = srcWidth * srcHeight;
	static uint8_t* I420 = (uint8_t *) malloc(size * sizeof(uint8_t)  * 2);

	if (size < srcWidth * srcHeight) {
		size = srcWidth * srcHeight;
		I420 = (uint8_t *) realloc(I420, size * sizeof(uint8_t) * 2);
	}

	uint8* src_y = I420;
	uint8* src_u = I420 + planeSize;
	uint8* src_v = I420 + planeSize + (planeSize >> 2);

	uint8_t* yplane = yuv;
	uint8_t* uvplane = yuv + planeSize;

	NV12ToI420(yplane, srcWidth, uvplane, srcWidth*2, src_y, src_stride_y, src_u, src_stride_u, src_v, src_stride_v, srcWidth, srcHeight);

	for (int y = 0; y < srcHeight; ++y) {
		I422ToRGB565Row_C(src_y, src_u, src_v, rgb, srcWidth);
		rgb += srcWidth << 1;
	    src_y += srcWidth;
	    if (y & 1) {
	      src_u += (srcWidth + 1) >> 1;
	      src_v += (srcWidth + 1) >> 1;
	    }
	}
}

void composeImage(const BaseImage &baseImage, const AttachImage &attachImage ){
	int w = attachImage.width;
	int h = attachImage.height;

	if (w + baseImage.blackLeft + baseImage.blackRight + attachImage.x > baseImage.width){
		w = baseImage.width - baseImage.blackLeft - baseImage.blackRight - attachImage.x;
	}

	if (h + baseImage.blackTop + baseImage.blackBottom + attachImage.y > baseImage.height){
		h = baseImage.height - baseImage.blackTop - baseImage.blackBottom - attachImage.y;
	}

	int x = attachImage.x + baseImage.blackLeft;
	int y = attachImage.y + baseImage.blackTop;

	uint8_t* dsty = (uint8_t*)baseImage.data + y * baseImage.width + x;

	uint8_t* srcuv = (uint8_t*)attachImage.data + attachImage.width * attachImage.height;

	if (attachImage.format == FORMAT_NV21 && baseImage.format == FORMAT_NV12){
		CopyPlane((uint8*)attachImage.data, attachImage.width, dsty, baseImage.width, w, h );

		uint8_t* dstuv = (uint8_t*)baseImage.data + baseImage.width * baseImage.height + ((y+1) >> 1) * baseImage.width + x;
		NV21ToNV12(srcuv, attachImage.width,
					dstuv,
					baseImage.width, w, h >> 1);
	} else if (attachImage.format == FORMAT_NV21 && baseImage.format == FORMAT_P420){
		CopyPlane((uint8*)attachImage.data, attachImage.width, dsty, baseImage.width, w, h );

		uint8_t* dstu = (uint8_t*)baseImage.data + baseImage.width * baseImage.height + ((y+1) >> 2) * baseImage.width + (x >> 1);
		uint8_t* dstv = dstu + baseImage.width * baseImage.height / 4;

		NV21ToI420(srcuv, attachImage.width, dstu, baseImage.width >>1, dstv, baseImage.width >>1, w >> 1, h >> 1);
	}
}


