/*
 * RgbToYuv.h
 *
 *  Created on: 2014-3-11
 *      Author: jagle
 */

#ifndef RGBTOYUV_H_
#define RGBTOYUV_H_

#include <stdio.h>

#define	FORMAT_NV12 0x01
#define	FORMAT_P420 0x32315659
#define	FORMAT_NV21 0x11

typedef unsigned int uint32;
typedef int int32;
typedef unsigned short uint16;  // NOLINT
typedef short int16;  // NOLINT
typedef unsigned char uint8;
typedef signed char int8;

struct AttachImage{
	uint8_t *data;
	int format;
	int x;
	int y;
	int width;
	int height;
};

struct BaseImage{
	uint8_t* data;
	int format;
	int width;
	int height;
	int blackLeft;
	int blackTop;
	int blackRight;
	int blackBottom;

	BaseImage():data(NULL),
			blackTop(0),
			blackLeft(0),
			blackRight(0),
			blackBottom(0)
	{}
};


void CopyPlane(const uint8* src_y, int src_stride_y,uint8* dst_y, int dst_stride_y, int width, int height);

void RGB565ToYUV420(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void RGB565ToNV12(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void ARGB8888ToNV12(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void ARGB8888ToYUV420(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void ABGR8888ToNV12(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void ARGB8888ToNV21(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void ABGR8888ToNV21(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void ABGR8888ToYUV420(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void NV12ToARGB8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void NV12ToABGR8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void NV12ToRGB565(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void NV21ToNV12(const uint8* src_y, int src_stride_y, uint8* dst_y, int dst_stride_y, int width, int height);

void YUV420ToARGB8888(uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight);

void composeImage(const BaseImage &baseImage, const AttachImage &attachImage );


#endif /* RGBTOYUV_H_ */
