#include <android/bitmap.h>
#include <stdlib.h>
#include "RgbToYuv.h"

#include "../JniCommon.h"

#define CLASSNAME "com/jagle/samples/CameraComposeDemo"

namespace jagle{

struct fields_t{
	jobject weakthis;
	jmethodID postBuffer;
};

static fields_t fields;

struct NV12Bitmap{
	uint8_t* data;
	int width;
	int height;
};

static NV12Bitmap sNV12;
static uint8_t* sRgb;

static void JNICALL setBaseBitmap(JNIEnv *env, jobject object, jobject bitmap){
	LOGE("setBaseBitmap");
	//TODO
	AndroidBitmapInfo info;
	int ret;
	void *rgb;
	if ((ret = AndroidBitmap_getInfo(env, bitmap, &info)) < 0){
		LOGE("can't get bitmap info");
		assert(false);
		return ;
	}

	if ((ret = AndroidBitmap_lockPixels(env, bitmap, &rgb)) < 0){
		LOGE("can't lock pixels");
		assert(false);
		return;
	}

	sNV12.data = (uint8_t*)malloc(info.width * info.height * 2);
	sNV12.width = info.width;
	sNV12.height = info.height;
	ABGR8888ToYUV420((uint8_t*)rgb, (uint8_t*)sNV12.data, info.width, info.height, info.width, info.height);

	sRgb = (uint8_t*)malloc(sNV12.width * sNV12.height * 4);

	fields.weakthis = (jobject) env->NewGlobalRef(object);
}

static void JNICALL composeFrame(JNIEnv *env, jobject object, jbyteArray jdata, jint format, jint x, jint y, jint width, jint height){
	int w = width;
	int h = height;

	jbyte * data;
	data = env->GetByteArrayElements(jdata, false);

 	if (x + width > sNV12.width) {
		w = sNV12.width - x;
	}

	if (y + height > sNV12.height){
		h = sNV12.height - y;
	}

	uint8_t* dsty = (uint8_t*)sNV12.data + y * sNV12.width + x;
	uint8_t* dstuv = (uint8_t*)sNV12.data + sNV12.width * sNV12.height + y * sNV12.width + x;
	/*CopyPlane((uint8*)data, width, dsty, sNV12.width, w, h );

	NV21ToNV12((uint8*)(data + width * height), width,
			dstuv,
			sNV12.width, w, h / 2 );*/

	AttachImage attachImage;
	attachImage.data = (uint8_t *)data;
	attachImage.format = format;
	attachImage.x = x;
	attachImage.y = y;
	attachImage.width = width;
	attachImage.height = height;

	BaseImage baseImage;
	baseImage.data = (uint8_t *)sNV12.data;
	baseImage.format = FORMAT_P420;
	baseImage.width = sNV12.width;
	baseImage.height = sNV12.height;
	//baseImage.blackLeft = 10;
	//baseImage.blackTop = 10;

	composeImage(baseImage, attachImage);

	env->ReleaseByteArrayElements(jdata, data, JNI_ABORT);

	YUV420ToARGB8888((uint8_t *)sRgb, (uint8_t *)sNV12.data, sNV12.width, sNV12.height, 0, 0);

	int length = sNV12.height * sNV12.width;
	jintArray jrgb = env->NewIntArray(length);
	env->SetIntArrayRegion(jrgb, 0, length, (jint*)sRgb);

	env->CallVoidMethod( fields.weakthis, fields.postBuffer, jrgb, 1, sNV12.width, sNV12.height );
	env->DeleteLocalRef(jrgb);
}

static void JNICALL release(JNIEnv *env, jobject object){
	delete[] ((uint8_t *)sNV12.data);
	sNV12.data = NULL;

	free(sRgb);
	sRgb = NULL;

	env->DeleteGlobalRef(fields.weakthis);
}

int register_video_compose(JNIEnv *env){

	JNINativeMethod jniMethods[] = {
		JNI_METHOD("setBaseBitmap", "(Landroid/graphics/Bitmap;)V", (void *)setBaseBitmap),
		JNI_METHOD("composeFrame","([BIIIII)V", (void*)composeFrame),
		JNI_METHOD("native_release", "()V", (void*)release),
	};

	JavaMethod javaMetheds[] = {
		JAVA_MEM("postComposedFrame", "([IIII)V", &fields.postBuffer),
	};

	if (registerJavaMethods(env, CLASSNAME, javaMetheds, NELEM(javaMetheds)) <= 0){
		return -1;
	}

	return registerNativeMethods(env, CLASSNAME, jniMethods, NELEM(jniMethods));
}

}

