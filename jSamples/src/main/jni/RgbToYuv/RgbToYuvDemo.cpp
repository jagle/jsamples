
#include <android/bitmap.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include "RgbToYuv.h"
#include "RgbToYuvDemo.h"

#define ARGB8888(c, r, g, b) \
    b = (uint8_t)((c) & 0xff); \
    g = (uint8_t)(((c) >> 8) & 0xff); \
    r = (uint8_t)(((c) >> 16) & 0xff)

#define RGB2Y(r, g, b, y) \
    y = (uint8_t)(((int)153 * (r) + (int)59 * (g) + (int)11 * (b)) / 100)

#define RGB2YUV(r, g, b, y, u, v) \
    RGB2Y(r, g, b, y); \
    u = (uint8_t)(((int)-17 * (r) - (int)33 * (g) + (int)50 * (b) + 12800) / 100); \
    v = (uint8_t)(((int)50 * (r) - (int)42 * (g) - (int)8 * (b) + 12800) / 100)

#define RGB2Y3(r, g, b, y) \
    y = (uint8_t)(((int)38 * (r) + (int)75 * (g) + (int)15 * (b)) >> 6)

#define RGB2YUV3(r, g, b, y, u, v) \
    RGB2Y(r, g, b, y); \
    u = (uint8_t)(((int)-22 * (r) -  (int)42 * (g) + (int)64 * (b) + 16384) >> 6); \
    v = (uint8_t)(((int)64 * (r) - (int)132 * (g) - (int)10 * (b) + 16384) >> 6)

#define RGB2YWithTable(r, g, b, y) \
    y = (uint8_t)(Y_R[r] + Y_G[g] + Y_B[b])

#define RGB2YUVWithTable(r, g, b, y, u, v) \
    RGB2Y(r, g, b, y); \
    u = (uint8_t)(U_R[r] + U_G[g] + U_B[b] + 128); \
    v = (uint8_t)(V_R[r] + V_G[g] + V_B[b] + 128)

#define YUV2RGB(y, u, v, r, g, b) \
	r = (uint8_t)(y + 1.13983 * (v - 128)); \
	g = (uint8_t)(y - 0.39465 * (u - 128) - 0.58060*(v - 128)); \
	b = (uint8_t)(y + 2.03211 * (u - 128));

#define BLACK_Y 0
#define BLACK_U 128
#define BLACK_V 128
#define SIZE 256

// C reference code that mimics the YUV assembly.
using namespace jagle;

jobject jthis;
jmethodID sendMsgMethodID = 0;
unsigned short Y_R[SIZE], Y_G[SIZE], Y_B[SIZE], U_R[SIZE], U_G[SIZE], U_B[SIZE],
		V_R[SIZE], V_G[SIZE], V_B[SIZE];

void sentMessageString(JNIEnv * env, jstring message) {
	env->CallVoidMethod(jthis, sendMsgMethodID, message);
}

void sendMessageCharArray(JNIEnv * env, char *message) {
	// Convert the C++ char array to Java String
	jstring msgString = env->NewStringUTF(message);
	sentMessageString(env, msgString);
}

void ConvertARGB8888ToYUV420PWithDiffSize(const uint8_t* rgb, uint8_t* yuv,
		uint32_t srcWidth, uint32_t srcHeight, uint32_t dstWidth,
		uint32_t dstHeight) {
	uint32_t planeSize = dstWidth * dstHeight;
	uint32_t halfWidth = dstWidth >> 1;

	uint32_t minWidth = (dstWidth < srcWidth ? dstWidth : srcWidth);
	uint32_t minHeight = (dstHeight < srcHeight ? dstHeight : srcHeight);

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);

	uint32_t* rgbIndex = (uint32_t*) rgb;

	uint8_t r, g, b;
	uint8_t y, u, v;

	for (uint32_t y = 0; y < minHeight; ++y) {
		uint8_t* yline = yplane + (y * dstWidth);
		uint8_t* uline = uplane + ((y >> 1) * halfWidth);
		uint8_t* vline = vplane + ((y >> 1) * halfWidth);

		for (uint32_t x = 0; x < minWidth; x += 2) {
			ARGB8888(*rgbIndex, r, g, b);

			RGB2Y(r, g, b, *yline);
			rgbIndex += 1;
			yline++;

			ARGB8888(*rgbIndex, r, g, b);

			RGB2YUV(r, g, b, *yline, *uline, *vline);
			rgbIndex += 1;
			yline++;
			uline++;
			vline++;
		}

		if (srcWidth > dstWidth) {
			rgbIndex += (srcWidth - dstWidth);
		}

		if (dstWidth > srcWidth) {
			memset(yline, BLACK_Y, dstWidth - srcWidth);
			memset(uline, BLACK_U, (dstWidth - srcWidth) >> 1);
			memset(vline, BLACK_V, (dstWidth - srcWidth) >> 1);
		}
	}

	if (dstHeight > srcHeight) {
		uint8_t* yline = yplane + (srcHeight * dstWidth);
		uint8_t* uline = uplane + ((srcHeight >> 1) * halfWidth);
		uint8_t* vline = vplane + ((srcHeight >> 1) * halfWidth);

		uint32_t fill = (dstHeight - srcHeight) * dstWidth;

		memset(yline, BLACK_Y, fill);
		memset(uline, BLACK_U, fill >> 2);
		memset(vline, BLACK_V, fill >> 2);
	}
}

void YUVToRGB(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth,
		uint32_t srcHeight) {
	uint32_t planeSize = srcWidth * srcHeight;
	uint32_t halfWidth = srcWidth >> 1;

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);

	for (uint32_t y = 0; y < srcHeight; ++y) {
		uint8_t* yline = yplane + (y * srcWidth);
		uint8_t* uline = uplane + ((y >> 1) * halfWidth);
		uint8_t* vline = vplane + ((y >> 1) * halfWidth);

		for (uint32_t x = 0; x < srcWidth; x += 2) {

		}
	}
}

void table_init() {
	static bool initialled = false;
	if (initialled) {
		return;
	}

	for (int i = 0; i < SIZE; i++) {
		Y_R[i] = (i * 1224) >> 12; //Y对于应的查表数组
		Y_G[i] = (i * 2404) >> 12;
		Y_B[i] = (i * 467) >> 12;
		U_R[i] = (i * 684) >> 12; //U对于应的查表数组
		U_G[i] = (i * 1340) >> 12;
		U_B[i] = (i * 2024) >> 12;
		V_R[i] = (i * 2024) >> 12; //V对于应的查表数组
		V_G[i] = (i * 1696) >> 12;
		V_B[i] = (i * 328) >> 12;
	}
	initialled = true;
}

void RGBToYUV2(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth,
		uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight) {

	uint32_t planeSize = dstWidth * dstHeight;
	uint32_t halfWidth = dstWidth >> 1;

	uint32_t minWidth = (dstWidth < srcWidth ? dstWidth : srcWidth);
	uint32_t minHeight = (dstHeight < srcHeight ? dstHeight : srcHeight);

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);

	uint32_t* rgbIndex = (uint32_t*) rgb;

	uint8_t r, g, b;
	uint8_t y, u, v;

	for (uint32_t y = 0; y < minHeight; ++y) {
		uint8_t* yline = yplane + (y * dstWidth);
		uint8_t* uline = uplane + ((y >> 1) * halfWidth);
		uint8_t* vline = vplane + ((y >> 1) * halfWidth);

		for (uint32_t x = 0; x < minWidth; x += 2) {
			ARGB8888(*rgbIndex, r, g, b);

			RGB2YWithTable(r, g, b, *yline);

			rgbIndex += 1;
			yline++;

			ARGB8888(*rgbIndex, r, g, b);

			RGB2YUVWithTable(r, g, b, *yline, *uline, *vline);

			rgbIndex += 1;
			yline++;
			uline++;
			vline++;
		}

		if (srcWidth > dstWidth) {
			rgbIndex += (srcWidth - dstWidth);
		}

		if (dstWidth > srcWidth) {
			memset(yline, BLACK_Y, dstWidth - srcWidth);
			memset(uline, BLACK_U, (dstWidth - srcWidth) >> 1);
			memset(vline, BLACK_V, (dstWidth - srcWidth) >> 1);
		}
	}

	if (dstHeight > srcHeight) {
		uint8_t* yline = yplane + (srcHeight * dstWidth);
		uint8_t* uline = uplane + ((srcHeight >> 1) * halfWidth);
		uint8_t* vline = vplane + ((srcHeight >> 1) * halfWidth);

		uint32_t fill = (dstHeight - srcHeight) * dstWidth;

		memset(yline, BLACK_Y, fill);
		memset(uline, BLACK_U, fill >> 2);
		memset(vline, BLACK_V, fill >> 2);
	}
}

void RGBToYUV3(const uint8_t* rgb, uint8_t* yuv, uint32_t srcWidth,
		uint32_t srcHeight, uint32_t dstWidth, uint32_t dstHeight) {

	uint32_t planeSize = dstWidth * dstHeight;
	uint32_t halfWidth = dstWidth >> 1;

	uint32_t minWidth = (dstWidth < srcWidth ? dstWidth : srcWidth);
	uint32_t minHeight = (dstHeight < srcHeight ? dstHeight : srcHeight);

	uint8_t* yplane = yuv;
	uint8_t* uplane = yuv + planeSize;
	uint8_t* vplane = yuv + planeSize + (planeSize >> 2);

	uint32_t* rgbIndex = (uint32_t*) rgb;

	uint8_t r, g, b;
	uint8_t y, u, v;

	for (uint32_t y = 0; y < minHeight; ++y) {
		uint8_t* yline = yplane + (y * dstWidth);
		uint8_t* uline = uplane + ((y >> 1) * halfWidth);
		uint8_t* vline = vplane + ((y >> 1) * halfWidth);

		for (uint32_t x = 0; x < minWidth; x += 2) {
			ARGB8888(*rgbIndex, r, g, b);

			RGB2Y3(r, g, b, *yline);

			rgbIndex += 1;
			yline++;

			ARGB8888(*rgbIndex, r, g, b);

			RGB2YUVWithTable(r, g, b, *yline, *uline, *vline);

			rgbIndex += 1;
			yline++;
			uline++;
			vline++;
		}

		if (srcWidth > dstWidth) {
			rgbIndex += (srcWidth - dstWidth);
		}

		if (dstWidth > srcWidth) {
			memset(yline, BLACK_Y, dstWidth - srcWidth);
			memset(uline, BLACK_U, (dstWidth - srcWidth) >> 1);
			memset(vline, BLACK_V, (dstWidth - srcWidth) >> 1);
		}
	}

	if (dstHeight > srcHeight) {
		uint8_t* yline = yplane + (srcHeight * dstWidth);
		uint8_t* uline = uplane + ((srcHeight >> 1) * halfWidth);
		uint8_t* vline = vplane + ((srcHeight >> 1) * halfWidth);

		uint32_t fill = (dstHeight - srcHeight) * dstWidth;

		memset(yline, BLACK_Y, fill);
		memset(uline, BLACK_U, fill >> 2);
		memset(vline, BLACK_V, fill >> 2);
	}
}

void Java_com_jagle_samples_RgbToYuvActivity_changeBitmap(JNIEnv *env,
		jobject object, jobject bitmap, jobject outbitmap) {
	jthis = object;
	jclass cls = env->GetObjectClass(object);
	sendMsgMethodID = env->GetMethodID(cls, "showMessage",
			"(Ljava/lang/String;)V");

	AndroidBitmapInfo inforgb; //AndroidBitmapInfo图片信息
	char tmp[100] = "";
	int ret = 0;
	void* rgb;
	void* outRgb;
	uint8_t yuv[2000000];

	if ((ret = AndroidBitmap_getInfo(env, bitmap, &inforgb)) < 0) { //【AndroidBitmap_getInfo】没有图片信息（宽高等等）
		sprintf(tmp, "AndroidBitmap_getInfo() failed ! error=%d", ret);
		LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
		return;
	}

	sprintf(tmp,
			"image info :: width is %d; height is %d; stride is %d; format is %d; flags is%d. \n",
			inforgb.width, inforgb.height, inforgb.stride, inforgb.format,
			inforgb.flags); //【获得图片的宽和高、格式】

	sendMessageCharArray(env, tmp);

	if ((ret = AndroidBitmap_lockPixels(env, bitmap, &rgb)) < 0) {
		LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
		return;
	}

	if ((ret = AndroidBitmap_lockPixels(env, outbitmap, &outRgb)) < 0) {
		LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
		return;
	}

	long before, after;

	for (int i = 0; i < 10; i++) {

		before = getCurrentTime();
		ABGR8888ToNV21((uint8_t*) rgb, yuv, inforgb.width, inforgb.height,
				inforgb.width, inforgb.height);
		after = getCurrentTime();

		sprintf(tmp, "Method neon Use time: %d\n", after - before);
		sendMessageCharArray(env, tmp);

		NV12ToRGB565((uint8_t*) outRgb, yuv, inforgb.width, inforgb.height,
				inforgb.width, inforgb.height);
	}
}

void Java_com_jagle_samples_RgbToYuvActivity_changeBitmapDiff(JNIEnv *env,
		jobject object, jobject bitmap, jobject outbitmap) {
	jthis = object;
	jclass cls = env->GetObjectClass(object);
	sendMsgMethodID = env->GetMethodID(cls, "showMessage",
			"(Ljava/lang/String;)V");

	AndroidBitmapInfo inforgb; //AndroidBitmapInfo图片信息
	char tmp[100] = "";
	int ret = 0;
	void* rgb;
	void* outRgb;
	uint8_t yuv[2000000];

	if ((ret = AndroidBitmap_getInfo(env, bitmap, &inforgb)) < 0) { //【AndroidBitmap_getInfo】没有图片信息（宽高等等）
		sprintf(tmp, "AndroidBitmap_getInfo() failed ! error=%d", ret);
		LOGE("AndroidBitmap_getInfo() failed ! error=%d", ret);
		return;
	}

	sprintf(tmp,
			"image info :: width is %d; height is %d; stride is %d; format is %d; flags is%d. \n",
			inforgb.width, inforgb.height, inforgb.stride, inforgb.format,
			inforgb.flags); //【获得图片的宽和高、格式】

	sendMessageCharArray(env, tmp);

	if ((ret = AndroidBitmap_lockPixels(env, bitmap, &rgb)) < 0) {
		LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
		return;
	}

	if ((ret = AndroidBitmap_lockPixels(env, outbitmap, &outRgb)) < 0) {
		LOGE("AndroidBitmap_lockPixels() failed ! error=%d", ret);
		return;
	}

	long before, after;
	for (int i = 0; i < 3; i++) {
		before = getCurrentTime();
		ConvertARGB8888ToYUV420PWithDiffSize((uint8_t*) rgb, yuv, inforgb.width,
				inforgb.height, inforgb.width, inforgb.height);
		after = getCurrentTime();

		sprintf(tmp, "Method 1 Use time: %d\n", after - before);
		sendMessageCharArray(env, tmp);

		table_init();

		before = getCurrentTime();
		RGBToYUV2((uint8_t*) rgb, yuv, inforgb.width, inforgb.height,
				inforgb.width, inforgb.height);
		after = getCurrentTime();

		sprintf(tmp, "Method 2 Use time: %d\n", after - before);
		sendMessageCharArray(env, tmp);

		before = getCurrentTime();
		RGBToYUV3((uint8_t*) rgb, yuv, inforgb.width, inforgb.height,
				inforgb.width, inforgb.height);
		after = getCurrentTime();

		sprintf(tmp, "Method 3 Use time: %d\n", after - before);
		sendMessageCharArray(env, tmp);

		before = getCurrentTime();
		RGB565ToNV12((uint8_t*) rgb, yuv, inforgb.width, inforgb.height,
				inforgb.width, inforgb.height);
		after = getCurrentTime();

		sprintf(tmp, "Method neon Use time: %d\n", after - before);
		sendMessageCharArray(env, tmp);

		NV12ToRGB565((uint8_t*) outRgb, yuv, inforgb.width, inforgb.height,
				inforgb.width, inforgb.height);
	}
}

