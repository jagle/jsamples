STLPORT_FORCE_REBUILD := false
APP_STL := stlport_static
APP_CPPFLAGS += -fno-exceptions
APP_CPPFLAGS += -fno-rtti
GLOBAL_CFLAGS =   -fvisibility=hidden
APP_ABI := armeabi

APP_PLATFORM := android-8

TARGET_ARCH_ABI := armeabi

APP_MODULES      := RgbToYuv
APP_MODULES      += yuv
APP_MODULES 	  += t9search
APP_MODULES      += PcmConvert
