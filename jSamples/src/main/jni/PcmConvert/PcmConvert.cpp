
void FixToFloat_NEON(const char* src, char* dst, int count) {
  asm volatile (
    ".p2align  2                               \n"
  "1:                                          \n"
    "vld1.16     {q0}, [%0]!        \n"  // load 16
    "VCVT.F32.S32 q1, q0 ,#31          \n"
    "subs       %2, %2, #16                    \n"  // 16 processed per loop
    "vst1.16     {q1}, [%1]!        \n"  // store 16
    "bgt        1b                             \n"
  : "+r"(src),   // %0
    "+r"(dst),   // %1
    "+r"(count)  // %2  // Output registers
  :                     // Input registers
  : "cc", "memory", "q0", "q1" // Clobber List
  );
}

int ConvertFloatTo16Bit(char* pData, int nDataSize)
{
	if (nDataSize <= 0)
	{
		return -1;
	}
	float* pSrc = (float*)pData;
	short* pDst = (short*)pData;
	union {float f; unsigned int i;} u;
	int processCount = nDataSize / 4;
	for (int i = 0; i < processCount; i++)
	{
		u.f = float(*pSrc + 384.0);
		if (u.i > 0x43c07fff)
		{
			*pDst = 32767;
		}
		else if (u.i < 0x43bf8000)
		{
			*pDst = -32768;
		}
		else
		{
			*pDst = short(u.i - 0x43c00000);
		}
		pSrc++;
		pDst++;
	}
	return nDataSize / 2;
}



int ConvertFixFloatTo16Bit(char* pData, int nDataSize)
{
    FixToFloat_NEON(pData, pData, nDataSize);
    return ConvertFloatTo16Bit(pData, nDataSize);
}

int ConvertFixFloatTo16Bit2(char* pData, int nDataSize)
{
    if (nDataSize <= 0)
    {
        return -1;
    }

    int * pSrc = (int *)pData;
    short* pDst = (short*)pData;
    int processCount = nDataSize / 4;
    for (int i = 0; i < processCount; i++)
    {
        *pDst = *pSrc >> 16;
        pSrc++;
        pDst++;
    }
    return nDataSize / 2;
}