#ifndef PCM_CONVERT_H_
#define PCM_CONVERT_H_
#include <stdio.h>
int ConvertFixFloatTo16Bit(char* pData, int nDataSize);

int ConvertFixFloatTo16Bit2(char* pData, int nDataSize);

int ConvertFloatTo16Bit(char* pData, int nDataSize);

void FixToFloat_NEON(const char* src, char* dst, int count) ;

#endif