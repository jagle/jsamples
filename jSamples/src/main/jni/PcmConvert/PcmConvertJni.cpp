#include "../JniCommon.h"
#include "PcmConvert.h"

#define CLASSNAME "com/jagle/samples/PcmConvert"


static int floatToInt16(JNIEnv *env, jobject object, jbyteArray jData, int nDataSize){
    jbyte* pData = env->GetByteArrayElements(jData, JNI_FALSE);
    int ret = ConvertFixFloatTo16Bit((char *)pData, nDataSize);
    env->ReleaseByteArrayElements(jData, pData, JNI_ABORT);
    return ret;
}

static int floatToInt16_2(JNIEnv *env, jobject object, jbyteArray jData, int nDataSize){
    jbyte* pData = env->GetByteArrayElements(jData, JNI_FALSE);
    int ret = ConvertFixFloatTo16Bit2((char *)pData, nDataSize);
    env->ReleaseByteArrayElements(jData, pData, JNI_ABORT);
    return ret;
}

int register_pcm_convert(JNIEnv *env){

	JNINativeMethod jniMethods[] = {
		JNI_METHOD("floatToInt16", "([BI)I", (void *)floatToInt16),
		JNI_METHOD("floatToInt16_2", "([BI)I", (void *)floatToInt16_2),
	};

	return jagle::registerNativeMethods(env, CLASSNAME, jniMethods, NELEM(jniMethods));
}
