#include "../JniCommon.h"

extern int register_pcm_convert(JNIEnv *env);

using namespace jagle;

RegJNIRec gRegJNI[] = {
		REG_JNI(register_pcm_convert),
};

int gRegJniLen = NELEM(gRegJNI);


