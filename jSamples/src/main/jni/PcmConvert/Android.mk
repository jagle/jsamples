LOCAL_PATH := $(call my-dir)


include $(CLEAR_VARS)

LOCAL_ARM_NEON  := true

LOCAL_LDFLAGS += -ljnigraphics \
-L$(SYSROOT)/usr/lib -llog \

LOCAL_MODULE    := PcmConvert
LOCAL_SRC_FILES := RegisterJni.cpp
LOCAL_SRC_FILES += PcmConvert.cpp
LOCAL_SRC_FILES += ../JniCommon.cpp
LOCAL_SRC_FILES += PcmConvertJni.cpp


include $(BUILD_SHARED_LIBRARY)
