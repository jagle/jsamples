#include "JniCommon.h"

// enable debug

extern RegJNIRec gRegJNI[] ;
extern int gRegJniLen;

using namespace jagle;

namespace jagle{

int registerNativeMethods(JNIEnv* env, const char* className, JNINativeMethod* methods, int nMethods)
{
	assert(env);
	jclass clazz;
	clazz = env->FindClass(className);
	if (clazz == NULL) {
		LOGE("can't find class %s", className);
		return -1;
	}

	if (env->RegisterNatives(clazz, methods, nMethods) < 0) {
		LOGE("register natives failed: %s", className);
		return -1;
	}

	return 1;
}

int registerJavaFields(JNIEnv *env, const char* className, JavaField *fields, int nFillds)
{
	jclass clazz = env->FindClass(className);
	if (clazz == NULL) {
		LOGE("Can't find class %s", className);
		return -1;
	}

    for (int i = 0; i < nFillds; i++) {
        JavaField *f = &fields[i];


        jfieldID field = env->GetFieldID(clazz, f->fieldName, f->fieldType);
        if (field == NULL) {
            LOGE("Can't find java field %s %s", className, f->fieldName);
            assert(false);
            return -1;
        }

        *(f->fieldID) = field;
    }

    return 1;
}

int registerJavaMethods(JNIEnv *env, const char* className, JavaMethod *methods, int nMethods)
{
	jclass clazz = env->FindClass(className);

	if (clazz == NULL) {
		LOGE("Can't find class %s", className);
		assert(false);
		return -1;
	}

    for (int i = 0; i < nMethods; i++) {
        JavaMethod *m = &methods[i];

        jmethodID method = env->GetMethodID(clazz, m->methodName, m->methodType);
        if (method == NULL) {
            LOGE("Can't find java method %s %s %s", className, m->methodName, m->methodType);
            assert(false);
            return -1;
        }

        *(m->methodID) = method;
    }

    return 1;
}

int registerJavaStaticMethods(JNIEnv *env, const char* className, JavaMethod *methods, int nMethods)
{
	jclass clazz = env->FindClass(className);

	if (clazz == NULL) {
		LOGE("Can't find class %s", className);
		assert(false);
		return -1;
	}

    for (int i = 0; i < nMethods; i++) {
        JavaMethod *m = &methods[i];

        jmethodID method = env->GetMethodID(clazz, m->methodName, m->methodType);
        if (method == NULL) {
            LOGE("Can't find java static method %s %s %s", className, m->methodName, m->methodType);
            assert(false);
            return -1;
        }

        *(m->methodID) = method;
    }

    return 1;
}

}// namespace jagle

/*
static const RegJNIRec gRegJNI[] = {
		REG_JNI(register_video_compose),
		REG_JNI(register_yuv_util),
		REG_JNI(register_T9search_engine),
};*/

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env = NULL;

    int version = -1;
    if (vm->GetEnv((void**) &env, JNI_VERSION_1_6) == JNI_OK ) {
        version = JNI_VERSION_1_6;
    }

    if (version == -1 && vm->GetEnv((void**) &env, JNI_VERSION_1_4) == JNI_OK){
    	version = JNI_VERSION_1_4;
    }

    if (version == -1 ){
    	vm->GetEnv((void**) &env, JNI_VERSION_1_1);
    	version = JNI_VERSION_1_1;
    	return version;
    }

    assert(env != NULL);

    for (size_t i = 0; i < gRegJniLen; i++) {
		if (gRegJNI[i].mProc(env) < 0) {
#ifndef NDEBUG
			LOGE("----------!!! %s failed to load\n", gRegJNI[i].mName);
#endif
			return -1;
		}
    }
    LOGI("JNI VERSION %d", version);
    return version;
}
