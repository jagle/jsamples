#ifndef JNICOMMON_H_
#define JNICOMMON_H_

#include <jni.h>
#include <android/log.h>
#include <stdlib.h>
#include <android/log.h>
#include <assert.h>
#include <sys/time.h>

// log标签
#define  TAG    "Samples NDK"
//定义verbose信息
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__)
// 定义info信息
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG,__VA_ARGS__)
// 定义debug信息
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, TAG, __VA_ARGS__)
// 定义waring信息
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
// 定义error信息
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,TAG,__VA_ARGS__)

#ifndef NELEM
#define NELEM(x) ((int) (sizeof(x) / sizeof((x)[0])))
#endif

#ifndef JAVA_MEM
#define JAVA_MEM(name, type, idPtr) {name, type, idPtr}
#endif

#ifndef JNI_METHOD
#define JNI_METHOD(name, type, fnPtr) {name, type, fnPtr}
#endif

#define TIME_TRACE_BEGIN(key) long timeTraceBegin##key = getCurrentTime();
#define TIME_TRACE_END(key) \
		long timeTraceEnd##key = getCurrentTime(); \
		LOGE(#key" exc time:%d", timeTraceEnd##key - timeTraceBegin##key);

#ifdef NDEBUG
    #define REG_JNI(name)      { name }
    struct RegJNIRec {
        int (*mProc)(JNIEnv*);
    };
#else
    #define REG_JNI(name)      { name, #name }
    struct RegJNIRec {
        int (*mProc)(JNIEnv*);
        const char* mName;
    };
#endif


namespace jagle{

struct JavaField {
    const char *fieldName;
    const char *fieldType;
    jfieldID   *fieldID;
};

struct JavaMethod {
    const char *methodName;
    const char *methodType;
    jmethodID  *methodID;
};

int registerNativeMethods(JNIEnv* env, const char* className, JNINativeMethod* methods, int nMethods);
int registerJavaFields(JNIEnv *env, const char* className, JavaField *fields, int nFields);
int registerJavaMethods(JNIEnv *env, const char* className, JavaMethod *methods, int nMethods);
int registerJavaStaticMethods(JNIEnv *env, const char* className, JavaMethod *methods, int nMethods);

inline long getCurrentTime() {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

} //namespace jagle

#endif //JNICOMMON_H_
