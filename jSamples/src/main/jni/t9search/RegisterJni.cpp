#include "../JniCommon.h"

extern int register_T9search_engine(JNIEnv *env);

RegJNIRec gRegJNI[] = {
		REG_JNI(register_T9search_engine),
};

int gRegJniLen = NELEM(gRegJNI);

